/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;

    export class LintDirectoryTest extends UnitTestRunner {
        private config : any = {
            extensions  : [
                "cpp",
                "hpp"
            ],
            fix         : [],
            ignore      : [],
            ignoredFiles: [
                "interfacesMap.hpp"
            ],
            lineEnding  : "<LF>",
            maxLineLen  : 140,
            spaces      : 4
        };

        public testBasic() : void {
            const baseDir : string = "test/resource/data/Com/Wui/Framework/XCppLint/AutoFormat/" +
                "DirectoryLint/";

            const files : string[] = this.Expand([
                baseDir + "/**/*.hpp",
                baseDir + "/**/*.cpp"
            ]);

            const linter : Linter = new Linter();

            files.forEach(($filePath : string) : void => {
                const fileData : string = this.readFileAbsolute($filePath);

                linter.Run(fileData, $filePath, this.config);

                if (linter.getErrors().length !== 0) {
                    // console.log($filePath);
                    linter.getErrors().forEach(($error : LintError) : void => {
                        // console.log("->[" + $error.getLine() + "] " + $error.getMessage());
                    });
                }

                linter.CleanErrors();
            });

            this.initSendBox();
        }
    }
}
