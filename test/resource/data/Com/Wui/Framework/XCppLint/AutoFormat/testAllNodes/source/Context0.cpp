/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "BaseContext.hpp"
#include "Context0.hpp"
#include "Graph0.hpp"

namespace NewVisualProject {    Context0::Context0(VisualGraph *parent)            : BaseContext(parent) {}    vx_status Context0::create() {        vx_status status = vxGetStatus((vx_reference)this->getVxContext());        if (status == VX_SUCCESS) {            this->graphsMap["graph0"] = new Graph0(this);            status = this->graphsMap["graph0"]->create();            if (status != VX_SUCCESS) {                this->removeGraph("graph0");            }        }        return status;    }    vx_status Context0::validate() {        vx_status status = vxGetStatus((vx_reference)this->getVxContext());        if (status == VX_SUCCESS){            status = this->graphsMap["graph0"]->validate();        }        return status;    }    vx_status Context0::process() const {        vx_status status = vxGetStatus((vx_reference)this->getVxContext());        auto graph0 = this->getGraph("graph0");        if (status == VX_SUCCESS && graph0 != nullptr) {            status = graph0->process();            if (status != VX_SUCCESS) {                const_cast<Context0 *>(this)->removeGraph("graph0");            }        } else {            status = VX_FAILURE;        }        return status;    }}