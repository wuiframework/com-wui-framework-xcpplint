/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ReplacementPair = Com.Wui.Framework.XCppLint.Structures.ReplacementPair;

    export class TokenMapLinterTest extends UnitTestRunner {
        private data : string;
        private errors : ArrayList<LintError>;
        private map : IToken;
        private hasError : boolean;
        private readonly codeParser : CodeBlocksParser;
        private readonly tokenMapLinter : TokenMapLinter;

        constructor() {
            super();
            this.codeParser = new CodeBlocksParser();
            this.tokenMapLinter = new TokenMapLinter(new Config(new ErrorReporter()));
        }

        public testErrorsBuild() : void {
            this.data = "int main() {\n" +
                "    std::make_pair<int, int>(1, 1);\n" +
                "    std::make_pair(1, 1);\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                new LintError(2, ERRORS.BUILD.EXPLICIT_MAKE_PAIR),
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "class Foo::Bar;\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                new LintError(1, ERRORS.BUILD.INNER_FWD_DECL),
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    a = aaa >?= bbo;\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                new LintError(2, ERRORS.BUILD.MAX_MIN_DEPRECATED),
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    const static int foo;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                new LintError(2, ERRORS.BUILD.STORAGE_CLASS),
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    printf(\"\\[\", bar);\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                new LintError(2, ERRORS.BUILD.UNDEFINED_ESCAPES),
                this.errors);
            // assert.equal(this.hasError,true); TODO(nxa33118)

            this.data = "using namespace std;\n" +
                "\n" +
                "int main() {\n" +
                "    using namespace foo;\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                new LintError(1, ERRORS.BUILD.NAMESPACES.USING_NAMESPACE),
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.BUILD.NAMESPACES.USING_NAMESPACE},
                this.errors);
            assert.equal(this.hasError, true);

            const testData = "#ifndef FOO\n" +
                "#define FOO 1\n" +
                "#endif // FOO\n" +
                "\n" +
                "#ifndef BAZ\n" +
                "#define BAZ 1\n" +
                "#endif BAZ\n" +
                "\n" +
                "#ifndef BAR\n" +
                "#define BAR 1\n" +
                "#endif\n" +
                "\n" +
                "void func() {\n" +
                "    bar();\n" +
                "}\n";
            const expectedError : any = {line: 5, message: ERRORS.BUILD.ENDIF_COMMENT};
            this.checkHasLintError(testData, expectedError);
            this.checkHasMultipleLintErrors(testData, [
                {line: 1, message: ERRORS.BUILD.ENDIF_COMMENT},
                {line: 9, message: ERRORS.BUILD.ENDIF_COMMENT}
            ], false);

            this.initSendBox();
        }

        public testErrorsBuildNamespaces() : void {
            const tempLinter : TokenMapLinter = new TokenMapLinter(new Config(new ErrorReporter()), "foo.hpp");
            const tempParser : CodeBlocksParser = new CodeBlocksParser();

            const testData = "/**\n" +
                " * Copyright (c) 2018 NXP\n" +
                " */\n" +
                "\n" +
                "namespace {\n" +
                "    void foo() {\n" +
                "        bar();\n" +
                "    }\n" +
                "}\n";

            const tempMap : any = tempParser.Parse(testData, new ArrayList<ReplacementPair>());
            tempLinter.Process(tempMap);

            const expectedError : any = {line: 5, message: ERRORS.BUILD.NAMESPACES.UNNAMED_NAMESPACE};

            assert.equal(this.isErrorInArray(expectedError, tempLinter.getAllErrors()), true);

            this.initSendBox();
        }

        public testErrorsLegalCopyright() : void {
            let testData = "/**\n" +
                " * Copyright (c) 2018 NXP\n" +
                " */\n" +
                "\n" +
                "void foo() {\n" +
                "    bar();\n" +
                "}\n";
            let expectedError : any = {line: 0, message: ERRORS.LEGAL_COPYRIGHT};
            this.checkHasLintError(testData, expectedError, false);

            testData = "void foo() {\n" +
                "    bar();\n" +
                "}\n";
            expectedError = {line: 0, message: ERRORS.LEGAL_COPYRIGHT};
            this.checkHasLintError(testData, expectedError, true);

            this.initSendBox();
        }

        public testErrorsOther() : void {
            let testData : string = "class C {\n" +
                " public:\n" +
                "    C() {}\n" +
                "\n" +
                "    bool get() {\n" +
                "        return this.val;\n" +
                "    }\n" +
                "\n" +
                " private:\n" +
                "    int func(int $i, int $j, int $k, int $i, int $j, int $k, int $i, " +
                "int $j, int $k, int $i, int $j, int $k, int $i, int $j, int $k, int $i, int $j, int $k);\n" +
                "\n" +
                "    bool val = false;\n" +
                "};\n";
            let expectedError : any = {line: 10, message: ERRORS.OTHER.LINE_LEN};
            this.checkHasLintError(testData, expectedError, true);

            testData = "class C {\n" +
                " public:\n" +
                "\tC() {}\n" +
                "\n" +
                "    bool get() {\n" +
                "        return this.val;\n" +
                "    }\n" +
                "\n" +
                " private:\n" +
                "    bool val = false;\n" +
                "};\n";
            expectedError = {line: 3, message: ERRORS.OTHER.TAB_FOUND};
            this.checkHasLintError(testData, expectedError, true);

            this.initSendBox();
        }

        public testErrorsReadabilityBraces() : void {
            this.data = "int foo(int $bar) {\n" +
                "    if ($bar) {\n" +
                "        func();\n" +
                "    } else\n" +
                "    {\n" +
                "        func();\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.READABILITY.BRACES.ELSE_BRACES_ON_BOTH_SIDES},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int foo(int $bar) {\n" +
                "    if ($bar)\n" +
                "        func();\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.READABILITY.BRACES.IF_ELSE_REQ_BRACES},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int foo(int $bar) {\n" +
                "    if ($bar) {\n" +
                "        func();\n" +
                "    } else\n" +
                "        funcB();\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.READABILITY.BRACES.IF_ELSE_REQ_BRACES},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int foo(int $bar) {\n" +
                "    if ($bar) {\n" +
                "        func();\n" +
                "    };\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.READABILITY.BRACES.REDUNDANT_SEMICOLON},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    if (foo) {\n" +
                "        bar();\n" +
                "    } if (baz) {\n" +
                "        bar();\n" +
                "    }\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.READABILITY.BRACES.NEW_LINE_IF},
                this.errors);
            assert.equal(this.hasError, true);

            const testData = "namespace Com::Wui::Framework::XCppCommons::Enums{\n" +
                "    void foo();\n" +
                "}\n";
            this.checkHasLintError(testData, {
                line   : 1,
                message: ERRORS.WHITESPACE.BRACES.MISSING_BEFORE_OPEN
            });

            this.initSendBox();
        }

        public testErrorsReadabilityWui() : void {
            let expectedError : any;
            let testData : string;

            testData = "class C {\n" +
                " public:\n" +
                "    C() {}\n" +
                "\n" +
                "    bool get(int $foo_bar) {\n" +
                "        return this.val;\n" +
                "    }\n" +
                "\n" +
                " private:\n" +
                "    bool val = false;\n" +
                "};\n";
            expectedError = {line: 5, message: ERRORS.READABILITY.WUI.HAS_UNDERSCORE};
            this.checkHasLintError(testData, expectedError, true);

            testData = "class C {\n" +
                " public:\n" +
                "    C() {}\n" +
                "\n" +
                "    bool get(int foo) {\n" +
                "        return this.val;\n" +
                "    }\n" +
                "\n" +
                " private:\n" +
                "    bool val = false;\n" +
                "};\n";
            expectedError = {line: 5, message: ERRORS.READABILITY.WUI.MISSING_DOLLAR};
            this.checkHasLintError(testData, expectedError, true);

            testData = "class A {\n" +
                "    typedef int myINT;\n" +
                "\n" +
                "    int func();\n" +
                "\n" +
                " public:\n" +
                "    int f();\n" +
                "};";

            expectedError = {line: 1, message: ERRORS.READABILITY.WUI.CLASS_ORDER};
            this.checkHasLintError(testData, expectedError, true);

            testData = "class A {\n" +
                "    typedef int myINT;\n" +
                "\n" +
                " private:\n" +
                "    int func();\n" +
                "\n" +
                " public:\n" +
                "    int f();\n" +
                "};\n";

            expectedError = {line: 1, message: ERRORS.READABILITY.WUI.CLASS_ORDER};
            this.checkHasLintError(testData, expectedError, true);

            testData = "class A {\n" +
                "    typedef int myINT;\n" +
                "\n" +
                " public:\n" +
                "    int f();\n" +
                "\n" +
                " private:\n" +
                "    int func();\n" +
                "\n" +
                " protected:\n" +
                "    int bar();\n" +
                "};\n";

            expectedError = {line: 1, message: ERRORS.READABILITY.WUI.CLASS_ORDER};
            this.checkHasLintError(testData, expectedError, true);

            testData = "class A {\n" +
                "    typedef int myINT;\n" +
                "\n" +
                " protected:\n" +
                "    int f();\n" +
                "\n" +
                " public:\n" +
                "    int func();\n" +
                "};\n";

            expectedError = {line: 1, message: ERRORS.READABILITY.WUI.CLASS_ORDER};
            this.checkHasLintError(testData, expectedError, true);

            testData = "class A {\n" +
                "    typedef int myINT;\n" +
                "\n" +
                " public:\n" +
                "    int f();\n" +
                "\n" +
                " private:\n" +
                "    int func();\n" +
                "};\n";

            expectedError = {line: 1, message: ERRORS.READABILITY.WUI.CLASS_ORDER};
            this.checkHasLintError(testData, expectedError, false);

            testData = "class A {\n" +
                "    typedef int myINT;\n" +
                "\n" +
                " public:\n" +
                "    int f();\n" +
                "\n" +
                " protected:\n" +
                "    int bar();\n" +
                "\n" +
                " private:\n" +
                "    int func();\n" +
                "};\n";

            expectedError = {line: 1, message: ERRORS.READABILITY.WUI.CLASS_ORDER};
            this.checkHasLintError(testData, expectedError, false);

            testData = "class A {\n" +
                "    // comment before public\n" +
                " public:\n" +
                "    myINT foo();\n" +
                "};\n";

            expectedError = {line: 2, message: ERRORS.READABILITY.WUI.VISIBILITY_SPEC_COMMENT};
            this.checkHasLintError(testData, expectedError, true);

            testData = "class A {\n" +
                "    /**\n" +
                "     * foo comment\n" +
                "     */\n" +
                " public:\n" +
                "    myINT foo();\n" +
                "};\n";

            expectedError = {line: 2, message: ERRORS.READABILITY.WUI.VISIBILITY_SPEC_COMMENT};
            this.checkHasLintError(testData, expectedError, true);

            testData = "/**\n" +
                " * Copyright (c) 2018 NXP\n" +
                " */\n" +
                "\n" +
                "namespace {\n" +
                "    void foo(struct BAR *$bar) {\n" +
                "        baz();\n" +
                "    }\n" +
                "}\n";

            expectedError = {line: 6, message: ERRORS.READABILITY.WUI.STRUCT_AS_PARAM};
            // this.checkHasLintError(testData, expectedError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceBlankLine() : void {
            this.data = "class Foo {\n" +
                " public:\n" +
                "    void bar();\n" +
                "\n" +
                " protected:\n" +
                "    void baz();\n" +
                "\n" +
                " private:\n" +
                "\n" +
                "     void bav();\n" +
                "};\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 10, message: ERRORS.WHITESPACE.BLANK_LINE.AFTER_VISIBILITY_SPEC},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int foo(int $bar) {\n" +
                "    if ($bar) {\n" +
                "        func();\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.WHITESPACE.BLANK_LINE.AT_END},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int foo(int $bar) {\n" +
                "    if ($bar) {\n" +
                "\n" +
                "        func();\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.WHITESPACE.BLANK_LINE.AT_START},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "class Foo {\n" +
                " public:\n" +
                "    void bar();\n" +
                " private:\n" +
                "    void baz();\n" +
                "};\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.WHITESPACE.BLANK_LINE.PRECEDED},
                this.errors);
            assert.equal(this.hasError, true);

            const testData = "class A {\n" +
                " private:\n" +
                "    /**\n" +
                "     * foo comment\n" +
                "     */\n" +
                "    void foo();\n" +
                "};\n";
            this.checkHasMultipleLintErrors(testData, <any[]>[
                {line: 2, message: ERRORS.WHITESPACE.BLANK_LINE.PRECEDED},
                {line: 3, message: ERRORS.WHITESPACE.BLANK_LINE.PRECEDED},
                {line: 4, message: ERRORS.WHITESPACE.BLANK_LINE.PRECEDED}
            ], false);

            this.initSendBox();
        }

        public testErrorsWhitespaceBraces() : void {
            this.data = "int main() {\n" +
                "    if (foo)\n" +
                "    {\n" +
                "        bar();\n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.BRACES.BRACE_AT_PREV_LINE},
                this.errors);
            assert.equal(this.hasError, true);

            const testData = "void func() {\n" +
                "    boost::array<string, 6> extensions =\n" +
                "            {\"\", \".exe\", \".com\", \".bat\", \".cmd\", \".msc\"};\n" +
                "}\n";
            this.checkHasLintError(testData, {
                line: 2, message: ERRORS.WHITESPACE.BRACES.BRACE_AT_PREV_LINE
            }, false);

            this.data = "int main() {\n" +
                "    if (foo) {\n" +
                "        bar [0] = 1;\n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.WHITESPACE.BRACES.EXTRA_SPACE_BEFORE_BRACE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main(){\n" +
                "    if (foo){\n" +
                "        bar[0] = 1;\n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 1, message: ERRORS.WHITESPACE.BRACES.MISSING_BEFORE_OPEN},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.BRACES.MISSING_BEFORE_OPEN},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "class A {\n" +
                " public:\n" +
                "    int bar();\n" +
                " private\n" +
                "    int baz();\n" +
                "    class B{\n" +
                "        int foo();\n" +
                "    };\n" +
                "};\n" +
                "\n" +
                "int main() {\n" +
                "    bar();\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 6, message: ERRORS.WHITESPACE.BRACES.MISSING_BEFORE_OPEN},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    if (foo) {\n" +
                "        bar[0] = 1;\n" +
                "    }else {\n" +
                "        bar[0] = 2;\n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.WHITESPACE.BRACES.MISSING_BEFORE_ELSE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    do {\n" +
                "        foo();\n" +
                "    }while (bar);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.WHITESPACE.BRACES.MISSING_BEFORE_WHILE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    do {\n" +
                "        foo();\n" +
                "    }while (bar);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.WHITESPACE.BRACES.MISSING_BEFORE_WHILE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "void func() {\n" +
                "    try {\n" +
                "        foo();\n" +
                "    }catch (std::exception &ex) {\n" +
                "        bar();\n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.WHITESPACE.BRACES.MISSING_BEFORE_CATCH},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceColon() : void {
            this.data = "class Foo {\n" +
                " public :\n" +
                "    void bar();\n" +
                "};\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.COLON_INVALID_SPACING},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceComma() : void {
            this.data = "class Foo {\n" +
                " public:\n" +
                "    void bar(int $a , int $b);\n" +
                "\n" +
                "    void baz(int $a,int $b) {\n" +
                "        bar($a ,$b);\n" +
                "    }\n" +
                "};\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.WHITESPACE.COMMA_INVALID_SPACING},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                new LintError(5, ERRORS.WHITESPACE.COMMA_INVALID_SPACING),
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 6, message: ERRORS.WHITESPACE.COMMA_INVALID_SPACING},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceEmptyBody() : void {
            this.data = "int main() {\n" +
                "    {\n" +
                "        foo();\n" +
                "    }\n" +
                "\n" +
                "    foo();\n" +
                "\n" +
                "    {}\n" +
                "\n" +
                "    foo();\n" +
                "\n" +
                "    {\n" +
                "    }\n" +
                "\n" +
                "    foo();\n" +
                "\n" +
                "    {\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    foo();\n" +
                "\n" +
                "    {\n" +
                "        foo();\n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 8, message: ERRORS.WHITESPACE.EMPTY_BODY.BLOCK},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 12, message: ERRORS.WHITESPACE.EMPTY_BODY.BLOCK},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 17, message: ERRORS.WHITESPACE.EMPTY_BODY.BLOCK},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    for (int i = 0; i < 10; i++) {\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    for (int i = 0; i < 10; i++) {\n" +
                "    }\n" +
                "\n" +
                "    for (int i = 0; i < 10; i++) {}\n" +
                "\n" +
                "    for (int i = 0; i < 10; i++) { }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.EMPTY_BODY.FOR},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 6, message: ERRORS.WHITESPACE.EMPTY_BODY.FOR},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 9, message: ERRORS.WHITESPACE.EMPTY_BODY.FOR},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 11, message: ERRORS.WHITESPACE.EMPTY_BODY.FOR},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    if (foo) {\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    if (foo) {\n" +
                "    }\n" +
                "\n" +
                "    if (foo) {}\n" +
                "\n" +
                "    if (foo) { }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.EMPTY_BODY.IF},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 6, message: ERRORS.WHITESPACE.EMPTY_BODY.IF},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 9, message: ERRORS.WHITESPACE.EMPTY_BODY.IF},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 11, message: ERRORS.WHITESPACE.EMPTY_BODY.IF},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    while (bar) {\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    while (bar) {\n" +
                "    }\n" +
                "\n" +
                "    while (bar) {}\n" +
                "\n" +
                "    while (bar) { }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.EMPTY_BODY.WHILE},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 6, message: ERRORS.WHITESPACE.EMPTY_BODY.WHILE},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 9, message: ERRORS.WHITESPACE.EMPTY_BODY.WHILE},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 11, message: ERRORS.WHITESPACE.EMPTY_BODY.WHILE},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceForColon() : void {
            this.data = "int main() {\n" +
                "    for (const auto foo : someVec) {\n" +
                "        bar();\n" +
                "    }\n" +
                "\n" +
                "    for (const auto foo :someVec) {\n" +
                "        bar();\n" +
                "    }\n" +
                "\n" +
                "    for (const auto foo: someVec) {\n" +
                "        bar();\n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 6, message: ERRORS.WHITESPACE.FOR_COLON_MISSING},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 10, message: ERRORS.WHITESPACE.FOR_COLON_MISSING},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceIndentLineEnds() : void {
            this.data = "int main() {\n" +
                "    for (const auto foo : someVec) { \n" +
                "        bar(); \n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.INDENT.LINE_ENDS_WHITESPACE},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.WHITESPACE.INDENT.LINE_ENDS_WHITESPACE},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceIndentLineStart() : void {
            this.data = "int main() {\n" +
                "   for (const auto foo : someVec) {\n" +
                "        bar();\n" +
                "    }\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.INDENT.LINE_START},
                this.errors);
            assert.equal(this.hasError, true);

            let testData = "class foo {\n" +
                " public:\n" +
                "    int bar() { return this->foo; }\n" +
                "\n" +
                " private:\n" +
                "    int foo = 0;\n" +
                "};\n";
            this.checkHasLintError(testData, {
                line: 3, message: ERRORS.WHITESPACE.INDENT.LINE_START
            }, false);

            testData = "void func() {\n" +
                "    typedef struct {\n" +
                "        WORD wLanguage;\n" +
                "        WORD wCodePage;\n" +
                "    } LanguageCodePage;\n" +
                "}\n";
            this.checkHasMultipleLintErrors(testData, <any[]>[
                {line: 3, message: ERRORS.WHITESPACE.INDENT.LINE_START},
                {line: 4, message: ERRORS.WHITESPACE.INDENT.LINE_START},
                {line: 5, message: ERRORS.WHITESPACE.INDENT.LINE_START}
            ], false);

            this.initSendBox();
        }

        public testErrorsWhitespaceNewline() : void {
            this.data = "int main() {\n" +
                "    do { foo(); } while (bar);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.NEWLINE.DO_WHILE_SINGLE_LINE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    do {\n" +
                "        foo();\n" +
                "    }\n" +
                "    while (bar);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.WHITESPACE.NEWLINE.DO_WHILE_AFTER_BRACE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    if (foo) {\n" +
                "        bar();\n" +
                "    }\n" +
                "    else {\n" +
                "        baz();\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.WHITESPACE.NEWLINE.ELSE_ON_SAME_LINE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    if (foo) {\n" +
                "        bar(); bar();\n" +
                "    } else {\n" +
                "        baz();\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.WHITESPACE.NEWLINE.MULTIPLE_COMMANDS},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceOperators() : void {
            this.data = "int main() {\n" +
                "    if (a > b) {\n" +
                "        c();\n" +
                "    }\n" +
                "\n" +
                "    if (a> b) {\n" +
                "        c();\n" +
                "    }\n" +
                "\n" +
                "    if (a >=b) {\n" +
                "        c();\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 6, message: ERRORS.WHITESPACE.OPERATORS.INVALID_COMPARISON},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 10, message: ERRORS.WHITESPACE.OPERATORS.INVALID_COMPARISON},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    if (a > b) {\n" +
                "        c = c << 1;\n" +
                "        c ++;\n" +
                "        -- c;\n" +
                "        ++ c;\n" +
                "        c --;\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.WHITESPACE.OPERATORS.INVALID_UNARY},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.WHITESPACE.OPERATORS.INVALID_UNARY},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 6, message: ERRORS.WHITESPACE.OPERATORS.INVALID_UNARY},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 7, message: ERRORS.WHITESPACE.OPERATORS.INVALID_UNARY},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    a = b;\n" +
                "    b=c;\n" +
                "    b= c;\n" +
                "    c =d;\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.WHITESPACE.OPERATORS.MISSING_SPACE_EQUAL},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.WHITESPACE.OPERATORS.MISSING_SPACE_EQUAL},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.WHITESPACE.OPERATORS.MISSING_SPACE_EQUAL},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceParens() : void {
            this.data = "int main() {\n" +
                "    class A {\n" +
                "        void foo();\n" +
                "\n" +
                "        void bar ();\n" +
                "    };\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.WHITESPACE.PARENS.EXTRA_SPACE_BEFORE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    Bar(Foo( ));\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.PARENS.EXTRA_SPACE_AFTER},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    bar(( foo));\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.PARENS.EXTRA_SPACE_AFTER},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    func (bar());\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.PARENS.EXTRA_SPACE_BEFORE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    Bar( Foo() );\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.PARENS.INVALID_SPACING},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    if(foo) {\n" +
                "        bar();\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.PARENS.INVALID_SPACING},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    if  (foo) {\n" +
                "        bar();\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.PARENS.INVALID_SPACING},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    Bar(\n" +
                "        Foo()\n" +
                "    );\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.PARENS.SHOULD_BE_MOVED_UP},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceSemicolon() : void {
            this.data = "int main() {\n" +
                "    class A {\n" +
                "        void foo();\n" +
                "\n" +
                "        void bar() ;\n" +
                "    };\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.WHITESPACE.SEMICOLON.EXTRA_SPACE_BEFORE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    class A {\n" +
                "        void foo();\n" +
                "\n" +
                "        ;\n" +
                "    };\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.WHITESPACE.SEMICOLON.ONLY},
                this.errors);
            assert.equal(this.hasError, true);

            const data : string = "int main() {\n" +
                "    struct A {\n" +
                "        void foo();\n" +
                "\n" +
                "        ;\n" +
                "    };\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.checkHasLintError(data, {line: 5, message: ERRORS.WHITESPACE.SEMICOLON.ONLY});
            this.checkHasLintError(data, {line: 6, message: ERRORS.WHITESPACE.SEMICOLON.ONLY}, false);

            this.data = "int main() {\n" +
                "    if (foo) {\n" +
                "        ;\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.WHITESPACE.SEMICOLON.ONLY},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeTemplate() : void {
            this.data = "template <typename T, typename T2,\n" +
                "        typename T3>\n" +
                "int faz(T $c) {\n" +
                "    while (b &&\n" +
                "           c > d &&\n" +
                "           (foo(bar,\n" +
                "                baz))) {\n" +
                "        foo();\n" +
                "    }\n" +
                "}\n" +
                "\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 1, message: ERRORS.WHITESPACE.TEMPLATE.SPACE_AFTER_TEMPLATE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "template< typename T, typename T2,\n" +
                "        typename T3>\n" +
                "int faz(T $c) {\n" +
                "    while (b &&\n" +
                "           c > d &&\n" +
                "           (foo(bar,\n" +
                "                baz))) {\n" +
                "        foo();\n" +
                "    }\n" +
                "}\n" +
                "\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 1, message: ERRORS.WHITESPACE.TEMPLATE.SPACE_AFTER_OPEN_BRACE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "template<typename T, typename T2,\n" +
                "        typename T3 >\n" +
                "int faz(T $c) {\n" +
                "    while (b &&\n" +
                "           c > d &&\n" +
                "           (foo(bar,\n" +
                "                baz))) {\n" +
                "        foo();\n" +
                "    }\n" +
                "}\n" +
                "\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 1, message: ERRORS.WHITESPACE.TEMPLATE.SPACE_BEFORE_CLOSE_BRACE},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsReadabilityInheritance() : void {
            this.data = "template<typename T>\n" +
                "class A {\n" +
                " public:\n" +
                "    int bar();\n" +
                "    int foo() override final;\n" +
                "\n" +
                " private:\n" +
                "    int foo();\n" +
                "};\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.READABILITY.INHERITANCE.OVERRIDE_REDUNDANT},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "template<typename T>\n" +
                "class A {\n" +
                " public:\n" +
                "    int bar();\n" +
                "    virtual int foo() override;\n" +
                "\n" +
                " private:\n" +
                "    int foo();\n" +
                "};\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.READABILITY.INHERITANCE.VIRT_REDUNDANT},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "template<typename T>\n" +
                "class A {\n" +
                " public:\n" +
                "    int bar();\n" +
                "    virtual int foo() final;\n" +
                "\n" +
                " private:\n" +
                "    int foo();\n" +
                "};\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.READABILITY.INHERITANCE.VIRT_REDUNDANT},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsReadabilityCasting() : void {
            this.data = "int main() {\n" +
                "    (int) foo;\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.READABILITY.CASTING.C_STYLE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    int(foo);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.READABILITY.CASTING.DEPRECATED_STYLE},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    &static_cast<A*>(b);\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.READABILITY.CASTING.DEREFERENCED_CAST},
                this.errors);
            assert.equal(this.hasError, true);

            let testData = "void func() {\n" +
                "    int c = bar()+baz;\n" +
                "    d = c-b;\n" +
                "    d = c/b;\n" +
                "    d = c%b;\n" +
                "}\n";
            this.checkHasMultipleLintErrors(testData, <any[]>[
                {line: 2, message: ERRORS.WHITESPACE.OPERATORS.INVALID_BINARY_OPERATOR},
                {line: 3, message: ERRORS.WHITESPACE.OPERATORS.INVALID_BINARY_OPERATOR},
                {line: 4, message: ERRORS.WHITESPACE.OPERATORS.INVALID_BINARY_OPERATOR},
                {line: 5, message: ERRORS.WHITESPACE.OPERATORS.INVALID_BINARY_OPERATOR}
            ]);

            testData = "int func() {\n" +
                "    return -1;\n" +
                "}\n";
            this.checkHasLintError(testData, {
                line: 2, message: ERRORS.WHITESPACE.OPERATORS.INVALID_BINARY_OPERATOR
            }, false);

            this.initSendBox();
        }

        public testErrorsRuntimeArrays() : void {
            this.data = "int main() {\n" +
                "    int i = 6;\n" +
                "    int foo[i];\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.RUNTIME.VAR_LEN_ARRAY},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeExplicit() : void {
            this.data = "class A {\n" +
                "    A();\n" +
                "\n" +
                "    A(int a);\n" +
                "\n" +
                "    A(int a, int b);\n" +
                "};\n" +
                "\n" +
                "A::A() {\n" +
                "    foo();\n" +
                "}\n" +
                "\n" +
                "A::A(int a) {\n" +
                "    foo();\n" +
                "}\n" +
                "\n" +
                "A::A(int a, int b) {\n" +
                "    foo();\n" +
                "}\n" +
                "\n" +
                "int main() {\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 4, message: ERRORS.RUNTIME.EXPLICIT.SINGLE_PARAM_CTOR},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "class B {\n" +
                "    B(int a = 0);\n" +
                "};\n" +
                "\n" +
                "B::B() {\n" +
                "    func();\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.EXPLICIT.CALLABLE_ONE_ARG_CTOR},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "class B {\n" +
                "    explicit B();\n" +
                "};\n" +
                "\n" +
                "B::B() {\n" +
                "    func();\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.EXPLICIT.ZERO_PARAM_CTOR},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeIndent() : void {
            this.data = "int main() {\n" +
                "        foo();\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.WHITESPACE.INDENT.INVALID_INDENT},
                this.errors);
            // assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeInit() : void {
            this.data = "class A {\n" +
                " public:\n" +
                "    A() : foo_(foo_) {\n" +
                "        bar();\n" +
                "    }\n" +
                "\n" +
                " private:\n" +
                "    int foo_;\n" +
                "};\n" +
                "\n" +
                "int main() {\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.RUNTIME.INIT_ITSELF},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeInt() : void {
            this.data = "int main() {\n" +
                "    short port = 8888;\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.INT.PORTS},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    short foo = 8888;\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.INT.USE_INT64},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorOtherFileNewLine() : void {
            this.data = "class A {\n" +
                "    typedef int myINT;\n" +
                "\n" +
                " public:\n" +
                "    myINT bar();\n" +
                "\n" +
                " protected:\n" +
                "    int baz();\n" +
                "\n" +
                " private:\n" +
                "    int foo();\n" +
                "};";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 0, message: ERRORS.OTHER.FILE_NEW_LINE},
                this.errors);
            // assert.equal(this.hasError, true);

            const testData = "bool ObjectValidator::IsHexadecimal(const string &$input) {\n" +
                "    if (fooo) {\n" +
                "        bar();\n" +
                "    } else if (inputLower.find('#') == 0) {\n" +
                "        bar();\n" +
                "    } else {\n" +
                "        bar();\n" +
                "    }\n" +
                "\n" +
                "    return isHex;\n" +
                "}\n";
            this.checkHasLintError(testData, {line: 0, message: ERRORS.OTHER.FILE_NEW_LINE}, false);

            this.initSendBox();
        }

        public testErrorWhitespaceNewlineMultipleCommands() : void {
            this.data = "a();b();\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 1, message: ERRORS.WHITESPACE.NEWLINE.MULTIPLE_COMMANDS},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeInvalidIncrement() : void {
            this.data = "int main() {\n" +
                "    *foo++;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.INVALID_INCREMENT},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeMemberStringReferences() : void {
            this.data = "int main() {\n" +
                "    const string &foo;\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.CONST_STRING_REF},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeMemset() : void {
            this.data = "int main() {\n" +
                "    memset(foo, bar, 0);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.MEMSET},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeOperator() : void {
            this.data = "class A {\n" +
                "\tvoid operator&()\n" +
                "};\n" +
                "\n" +
                "int main() {\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.OPERATOR_AMPERSAND},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimePrintf() : void {
            // TODO(nxa3311) how to cause FORMAT_STRING_BUG ? python regex is not matched while JS is ...

            this.data = "int main() {\n" +
                "    snprintf(buffer, 100, bar.foo);\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.PRINTF.SIZEOF_INSTEAD},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    strcpy(str2, str1);\n" +
                "    strcpy(str, \"these \");\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.PRINTF.SNPRINTF},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.RUNTIME.PRINTF.SNPRINTF},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    sprintf (buffer, \"%d plus %d is %d\", a, b, a+b);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.PRINTF.SPRINTF},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimePrintfFormat() : void {
            this.data = "int main() {\n" +
                "    printf(\"%q\", bar);\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.PRINTF_FORMAT.Q_IN_FORMAT},
                this.errors);
            // assert.equal(this.hasError, true);

            this.data = "int main() {\n" +
                "    printf(\"%1$\", bar);\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.PRINTF_FORMAT.N_FORMATS},
                this.errors);
            // assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeReference() : void {
            this.data = "void func(int &$foo) {\n" +
                "    func(1);\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 1, message: ERRORS.RUNTIME.NON_CONST_REF},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeString() : void {
            this.data = "#include <string>\n" +
                "\n" +
                "static std::string foo = \"bar\";\n" +
                "int main() {\n" +
                "static std::string bar = \"bar\";\n" +
                "    static std::string bar = \"bar\";\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.RUNTIME.STRING.NOT_PERMITTED},
                this.errors);
            assert.equal(this.hasError, true);
            this.hasError = this.isErrorInArray(
                {line: 5, message: ERRORS.RUNTIME.STRING.NOT_PERMITTED},
                this.errors);
            assert.equal(this.hasError, true);

            this.data = "#include <string>\n" +
                "\n" +
                "const string foo = \"saas\";\n" +
                "int main() {\n" +
                "    return 0;\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 3, message: ERRORS.RUNTIME.STRING.USE_C_STYLE_STRING},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsRuntimeThreadsafeFn() : void {
            this.data = "int main() {\n" +
                "    bar = asctime(foo);\n" +
                "}\n";
            this.cleanAndRun();
            this.hasError = this.isErrorInArray(
                {line: 2, message: ERRORS.RUNTIME.THREADSAFE_FN},
                this.errors);
            assert.equal(this.hasError, true);

            this.initSendBox();
        }

        public testErrorsWhitespaceCppCasting() : void {
            let testData = "void func() {\n" +
                "    int i = reinterpret_cast <int>(foo);\n" +
                "}\n";
            let expectedError : any = {line: 2, message: ERRORS.WHITESPACE.CPP_CASTING};
            this.checkHasLintError(testData, expectedError);

            testData = "void func() {\n" +
                "    int i = reinterpret_cast< int>(foo);\n" +
                "}\n";
            expectedError = {line: 2, message: ERRORS.WHITESPACE.CPP_CASTING};
            this.checkHasLintError(testData, expectedError);

            testData = "void func() {\n" +
                "    int i = reinterpret_cast<int >(foo);\n" +
                "}\n";
            expectedError = {line: 2, message: ERRORS.WHITESPACE.CPP_CASTING};
            this.checkHasLintError(testData, expectedError);

            testData = "void func() {\n" +
                "    int i = reinterpret_cast<int> (foo);\n" +
                "}\n";
            expectedError = {line: 2, message: ERRORS.WHITESPACE.CPP_CASTING};
            this.checkHasLintError(testData, expectedError);

            this.initSendBox();
        }

        public testErrorsLintOff() : void {
            let testData = "class A {\n" +
                "    // xcpplint:disable-next-line runtime-non_const_ref\n" +
                "    int foo(string &$target);\n" +
                "\n" +
                "    int foo2(string &$target);  // xcpplint:disable-line runtime-non_const_ref\n" +
                "};\n" +
                "\n" +
                "int A::foo(string &$target) {\n" +
                "    $target = \"foo\";\n" +
                "    func();\n" +
                "}\n" +
                "\n" +
                "void bar(string &$bar = \"someDefaultParameter\") {\n" +
                "    func();\n" +
                "}\n";
            let expectedError : any = {line: 13, message: ERRORS.RUNTIME.NON_CONST_REF};

            this.checkHasLintError(testData, expectedError, true);

            this.checkHasMultipleLintErrors(testData, [
                {line: 3, message: ERRORS.RUNTIME.NON_CONST_REF},
                {line: 5, message: ERRORS.RUNTIME.NON_CONST_REF}
            ], false);

            testData = "// xcpplint:disable runtime-non_const_ref\n" +
                "void foo(string &$bar) {\n" +
                "    $bar = \"ahohj\";\n" +
                "}\n";

            expectedError = {line: 2, message: ERRORS.RUNTIME.NON_CONST_REF};
            this.checkHasLintError(testData, expectedError, false);

            testData = "// xcpplint:disable-next-line runtime-non_const_ref\n" +
                "void foo(string &$bar) {\n" +
                "    $bar = \"some string\";\n" +
                "}\n" +
                "\n" +
                "void baz(string &$bar) {    // xcpplint:disable-line runtime-non_const_ref\n" +
                "    $bar = \"some string\";\n" +
                "}\n" +
                "\n" +
                "// xcpplint:disable runtime-non_const_ref\n" +
                "void func() {}\n" +
                "\n" +
                "void err(string &$bar) {\n" +
                "    $bar = \"some string\";\n" +
                "}\n" +
                "\n" +
                "// xcpplint:enable\n" +
                "\n" +
                "void err2(string &$bar) {\n" +
                "    $bar = \"some string\";\n" +
                "}\n";

            this.checkHasMultipleLintErrors(testData, [
                {line: 2, message: ERRORS.RUNTIME.NON_CONST_REF},
                {line: 6, message: ERRORS.RUNTIME.NON_CONST_REF},
                {line: 13, message: ERRORS.RUNTIME.NON_CONST_REF}
            ], false);

            expectedError = {line: 19, message: ERRORS.RUNTIME.NON_CONST_REF};
            this.checkHasLintError(testData, expectedError);

            this.initSendBox();
        }

        public testErrorsWhitespaceWuiTemplatedVars() : void {
            const testData = "void bar() {\n" +
                "    vector<int > foo;\n" +
                "\n" +
                "    vector< int> foo1;\n" +
                "\n" +
                "    vector <int> foo2;\n" +
                "\n" +
                "    vector<int>foo3;\n" +
                "\n" +
                "    vector<int> foo4;\n" +
                "}\n";
            const expectedErrors : any[] = [
                {line: 2, message: ERRORS.WHITESPACE.WUI.TEMPLATED_VARS.INNER_SPACES},
                {line: 4, message: ERRORS.WHITESPACE.WUI.TEMPLATED_VARS.INNER_SPACES},
                {line: 6, message: ERRORS.WHITESPACE.WUI.TEMPLATED_VARS.SPACE_AT_START},
                {line: 8, message: ERRORS.WHITESPACE.WUI.TEMPLATED_VARS.SPACE_AT_END}
            ];

            this.checkHasMultipleLintErrors(testData, expectedErrors, true);

            this.initSendBox();
        }

        public testErrorsWhitespacePointers() : void {
            const testData = "void func() {\n" +
                "    foo->bar();\n" +
                "    bar.foo();\n" +
                "\n" +
                "    foo-> bar();\n" +
                "    bar. foo();\n" +
                "\n" +
                "    foo ->bar();\n" +
                "    bar .foo();\n" +
                "}\n";
            let expectedErrors : any[] = [
                {line: 5, message: ERRORS.WHITESPACE.POINTERS},
                {line: 6, message: ERRORS.WHITESPACE.POINTERS},
                {line: 8, message: ERRORS.WHITESPACE.POINTERS},
                {line: 9, message: ERRORS.WHITESPACE.POINTERS}
            ];
            this.checkHasMultipleLintErrors(testData, expectedErrors, true);

            expectedErrors = [
                {line: 2, message: ERRORS.WHITESPACE.POINTERS},
                {line: 3, message: ERRORS.WHITESPACE.POINTERS}
            ];
            this.checkHasMultipleLintErrors(testData, expectedErrors, false);

            this.initSendBox();
        }

        public testErrorsWhitespaceNewlineCatchNewline() : void {
            const testData = "int main() {\n" +
                "    try {\n" +
                "        foo();\n" +
                "    }\n" +
                "    catch (std::exception &ex) {\n" +
                "        bar();\n" +
                "    }\n" +
                "}\n";
            const expectedError : any = {line: 5, message: ERRORS.WHITESPACE.NEWLINE.CATCH_NEWLINE};
            this.checkHasLintError(testData, expectedError);

            this.initSendBox();
        }

        private checkHasMultipleLintErrors($testData : string, $expectedErrors : any[], $hasError : boolean = true) : void {
            this.data = $testData;
            this.cleanAndRun();
            $expectedErrors.forEach(($value : any) : void => {
                this.hasError = this.isErrorInArray($value, this.errors);
                assert.equal(this.hasError, $hasError);
            });
        }

        private checkHasLintError($testData : string, $expectedError : any, $hasError : boolean = true) : void {
            this.data = $testData;
            this.cleanAndRun();
            this.hasError = this.isErrorInArray($expectedError, this.errors);
            assert.equal(this.hasError, $hasError);
        }

        private cleanAndRun() : void {
            this.tokenMapLinter.CleanAllDisabledLines();
            this.tokenMapLinter.CleanAllErrors();
            this.map = this.codeParser.Parse(this.data, new ArrayList<ReplacementPair>());
            this.tokenMapLinter.Process(this.map);
            this.errors = this.tokenMapLinter.getAllErrors();
        }
    }
}
