/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Runtime {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;

    export class ThreadsafeFn extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.RUNTIME.THREADSAFE_FN);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const UNSAFE_FUNC_PREFIX = "(?:[-+*/=%^&|(<]\\s*|>\\s+)";
            const THREADING_LIST = [
                ["asctime(", "asctime_r(", UNSAFE_FUNC_PREFIX + "asctime\\([^)]+\\)"],
                ["ctime(", "ctime_r(", UNSAFE_FUNC_PREFIX + "ctime\\([^)]+\\)"],
                ["getgrgid(", "getgrgid_r(", UNSAFE_FUNC_PREFIX + "getgrgid\([^)]+\)"],
                ["getgrnam(", "getgrnam_r(", UNSAFE_FUNC_PREFIX + "getgrnam\([^)]+\)"],
                ["getlogin(", "getlogin_r(", UNSAFE_FUNC_PREFIX + "getlogin\(\)"],
                ["getpwnam(", "getpwnam_r(", UNSAFE_FUNC_PREFIX + "getpwnam\([^)]+\)"],
                ["getpwuid(", "getpwuid_r(", UNSAFE_FUNC_PREFIX + "getpwuid\([^)]+\)"],
                ["gmtime(", "gmtime_r(", UNSAFE_FUNC_PREFIX + "gmtime\([^)]+\)"],
                ["localtime(", "localtime_r(", UNSAFE_FUNC_PREFIX + "localtime\([^)]+\)"],
                ["rand(", "rand_r(", UNSAFE_FUNC_PREFIX + "rand\(\)"],
                [
                    "strtok(", "strtok_r(",
                    UNSAFE_FUNC_PREFIX + "strtok\([^)]+\)"
                ],
                ["ttyname(", "ttyname_r(", UNSAFE_FUNC_PREFIX + "ttyname\([^)]+\)"]
            ];

            THREADING_LIST.forEach(($value : string[]) : void => {
                const singleThreadFunc = $value[0];
                const multiThreadSafeFunc = $value[1];
                const pattern = $value[2];

                const re = new RegExp(pattern, "gm");

                if (re.test($processingInfo.Child().body)) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            });
        }
    }
}
