/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IBaseLintRule = Com.Wui.Framework.XCppLint.Interfaces.IBaseLintRule;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ReplacementPair = Com.Wui.Framework.XCppLint.Structures.ReplacementPair;
    import CharType = Com.Wui.Framework.XCppLint.Enums.CharType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export class TokenMapLinter extends BaseObject {
        private lines : string[];
        private nestingState : number;
        private readonly filename : string;
        private readonly errorReporter : ErrorReporter;
        private rules : ArrayList<IBaseLintRule>;
        private config : Config;
        private errorsToFix : ArrayList<ErrorToFix>;

        public static getPrevChild($parent : IToken, $childIndex : number) : IToken {
            if (!$parent) {
                return null;
            }

            if ($parent.children.length < $childIndex || $childIndex <= 0) {
                return null;
            }

            return $parent.children[$childIndex - 1];
        }

        /**
         * @param {Config} $config Config to be used.
         * @param {string} $fileName FileName of the file that is linted.
         */
        constructor($config : Config, $fileName : string = "") {
            super();
            this.lines = [];
            this.errorReporter = new ErrorReporter();
            this.filename = $fileName;
            this.rules = new ArrayList<IBaseLintRule>();
            this.errorsToFix = new ArrayList<ErrorToFix>();
            this.config = $config;

            const reflection : Reflection = Reflection.getInstance();
            reflection.getAllClasses()
                .forEach(($className : string) : void => {
                    if (reflection.ClassHasInterface($className, IBaseLintRule) &&
                        $className !== BaseLintRule.ClassName()) {
                        const classObject : any = reflection.getClass($className);
                        const rule : IBaseLintRule = new classObject();
                        rule.LoadConfig(this.config);
                        this.rules.Add(rule);
                    }
                });
        }

        /**
         * @return {ArrayList<LintError>} Return array of errors generated during the lint.
         */
        public getAllErrors() : ArrayList<LintError> {
            return this.errorReporter.getAllErrors();
        }

        /**
         * @param {string} $filePath Set file path to which the errors shoould belong.
         */
        public setOwner($filePath : string) : void {
            this.errorReporter.setOwner($filePath);
        }

        public CleanAllErrors() : void {
            this.errorReporter.ClearErrors();
        }

        public CleanAllDisabledLines() : void {
            this.errorReporter.ClearErrors();
        }

        /**
         * @param $config Store reference to used config.
         */
        public LoadConfig($config : Config) : void {
            this.config = $config;
        }

        /**
         * @param {IToken} $map Root to the token map. Traverse the map and lint all its children.
         */
        public Process($map : IToken) : void {
            const read = ($parent : IToken) : void => {
                for (const child of $parent.children) {
                    child.nesting = this.nestingState;
                    this.processChild($parent, child);
                    this.nestingState++;
                    read(child);
                }
                this.nestingState--;
            };
            this.nestingState = 0;
            read($map);
        }

        /**
         * Fix found errors, if fix is enabled.
         */
        public fixErrors() : void {
            this.errorsToFix.foreach(($value : ErrorToFix) : void => {
                if ($value.Rule().Fix($value.ProcesingInfo())) {
                    $value.LintErrorReference().Fixed();
                } else {
                    $value.LintErrorReference().FixFailed();
                }
            });
        }

        /**
         * @param {IToken} $map Root to the token map. All its contents are stored to a file.
         * @param {ArrayList<ReplacementPair>} $replacementPairs Original and replaced string pairs, will be replaced back.
         * @return {string} Return file with original strings restored.
         */
        public getFinalFile($map : IToken, $replacementPairs : ArrayList<ReplacementPair> = null) : string {
            let file = "";
            const traverse = ($parent : IToken) : void => {
                file += $parent.body;
                for (const child of $parent.children) {
                    traverse(child);
                }
            };
            traverse($map);

            file = this.fixLineEndings(file);

            file = Utils.RestoreOriginalString(file, $replacementPairs);

            return file;
        }

        private fixLineEndings($input : string) : string {
            const le : string = (<string>this.config.getPropertyValue("lineEnding"));

            if (le === "<CRLF>") {
                const crlfPlaceholder : string = "<:CRLF:>";
                $input = StringUtils.Replace($input, CharType.CRLF, crlfPlaceholder);
                $input = StringUtils.Replace($input, CharType.LF, CharType.CRLF);
                $input = StringUtils.Replace($input, crlfPlaceholder, CharType.CRLF);
            } else if (le === "<LF>") {
                $input = StringUtils.Replace($input, CharType.CRLF, CharType.LF);
            }

            return $input;
        }

        private getNextToken($parent : IToken, $child : IToken) : IToken {
            if (!$parent) {
                return null;
            }

            if ($child.children.length > 0) {
                return $child.children[0];
            } else if ($parent.children.length > $child.childIndex + 1) {
                return $parent.children[$child.childIndex + 1];
            } else {
                return null;
            }
        }

        private getPrevToken($parent : IToken, $child : IToken) : IToken {
            if (!$parent) {
                return null;
            }

            const index : number = $child.childIndex;
            const prevChild : IToken = TokenMapLinter.getPrevChild($parent, index);

            if (!prevChild) {
                return null;
            }

            if (prevChild.children.length === 0) {
                return prevChild;
            }
            return prevChild.children[prevChild.children.length - 1];
        }

        private processChild($parent : IToken, $child : IToken) : void {
            this.checkNextTokenForLintControl($parent, $child);
            this.checkCurrentTokenForLintControl($child);

            const processingInfo : BaseProcessingInfo = new BaseProcessingInfo();
            processingInfo.Child($child);
            processingInfo.Config(this.config);
            processingInfo.FileName(this.filename);
            processingInfo.NextToken(this.getNextToken($parent, $child));
            processingInfo.Parent($parent);
            processingInfo.PrevChild(TokenMapLinter.getPrevChild($parent, $child.childIndex));
            processingInfo.PrevLine(this.getPrevLine($child.line));
            processingInfo.PrevToken(this.getPrevToken($parent, $child));

            this.rules.foreach(($rule : IBaseLintRule) : void => {
                if ($rule.IsSensitiveTo($child)) {
                    $rule.Process(processingInfo,
                        ($errorObject : IErrorReporterObject, $line : number, $column : number) : void => {
                            this.errorReporter.ReportError($errorObject, $line, $column);
                            if (this.config.ShouldFix($errorObject)) {
                                const errToFix : ErrorToFix = new ErrorToFix();
                                errToFix.LintErrorReference(this.errorReporter.getAllErrors().getLast());
                                errToFix.ProcesingInfo(processingInfo);
                                errToFix.Rule($rule);
                                this.errorsToFix.Add(errToFix);
                            }
                        });
                }
            });

            this.rules.foreach(($rule : IBaseLintRule) : void => {
                $rule.ResetBefore();
            });
        }

        private isLintControl($commentBody : string, $childLine : number, $nextTokenLine : number,
                              $callback : ($errorToIgnore : string, $line : number, $global : boolean) => void) : void {
            let matches : RegExpExecArray = /xcpplint\s*:\s*disable-line\s*(:?)\s*([\w-_]*)/gm.exec($commentBody);

            if (matches) {
                if (!StringUtils.IsEmpty(matches[2])) {
                    $callback(matches[2], $nextTokenLine, false);
                }
            } else {
                matches = /xcpplint\s*:\s*disable-next-line\s*(:?)\s*([\w-_]*)/gm.exec($commentBody);
                if (matches) {
                    if (!StringUtils.IsEmpty(matches[2])) {
                        $callback(matches[2], $nextTokenLine + 1, false);
                    }
                } else {
                    matches = /xcpplint\s*:\s*disable\s*(:?)\s*([\w-_]*)/gm.exec($commentBody);
                    if (matches) {
                        if (!StringUtils.IsEmpty(matches[2])) {
                            $callback(matches[2], $nextTokenLine, true);
                        }
                    } else {
                        matches = /xcpplint\s*:\s*enable/gm.exec($commentBody);
                        if (matches) {
                            this.errorReporter.CleanAllGlobalLines();
                        }
                    }
                }
            }
        }

        private checkCurrentTokenForLintControl($child : IToken) : void {
            if ($child.type !== TokenType.COMMENT) {
                return;
            }

            const commentContents = $child.body;
            this.isLintControl(commentContents, $child.line, $child.line,
                ($errorToIgnore : string, $line : number, $global : boolean = false) : void => {
                    this.errorReporter.AddIgnoreErrorLine($errorToIgnore, $line, $global);
                });
        }

        private checkNextTokenForLintControl($parent : IToken, $child : IToken) : void {
            const nextToken : IToken = this.getNextToken($parent, $child);
            if (!nextToken) {
                return;
            }

            if (nextToken.type !== TokenType.COMMENT) {
                return;
            }

            const commentContents = nextToken.body;
            this.isLintControl(commentContents, $child.line, nextToken.line,
                ($errorToIgnore : string, $line : number, $global : boolean = false) : void => {
                    this.errorReporter.AddIgnoreErrorLine($errorToIgnore, $line, $global);
                });
        }

        private checkForNonStandardConstructs($parent : IToken, $child : IToken) : void {
            if (/printf\s*\(.*".*%[-+ ]?\d*q/gm.test($child.body)) {
                this.errorReporter.ReportError(ERRORS.RUNTIME.PRINTF_FORMAT.Q_IN_FORMAT, $child.line);
            }

            if (/printf\s*\(.*".*%\d+\$/gm.test($child.body)) {
                this.errorReporter.ReportError(ERRORS.RUNTIME.PRINTF_FORMAT.N_FORMATS, $child.line);
            }

            const body : string = StringUtils.Remove($child.body, "\\\\");

            if (/("|\').*\\(%|\[|\(|{)/gm.test(body)) {
                this.errorReporter.ReportError(ERRORS.BUILD.UNDEFINED_ESCAPES, $child.line);
            }
        }

        private getLines($data : string) : void {
            $data = StringUtils.Replace($data, "\r", "");
            this.lines = StringUtils.Split($data, "\n");
        }

        private getPrevLine($index : number) : string {
            return ($index - 1 > 0 ? this.lines[$index - 2] : "");
        }

        private checkLanguage($parent : IToken, $child : IToken) : void {
            let printfArgs : string = this.getTextInside($child, /\b(string)?printf\s*\(/gmi);

            const collapseString = ($input : string) : string => {
                if (!printfArgs) {
                    return printfArgs;
                }
                const first : number = StringUtils.IndexOf($input, "\"");
                const last : number = StringUtils.IndexOf($input, "\"", false);
                return StringUtils.Remove($input, StringUtils.Substring($input, first + 1, last));
            };

            printfArgs = collapseString(printfArgs);

            if (printfArgs) {
                const match = /([\w.\->()]+)$/.exec(printfArgs);
                if (match !== undefined && match[1] !== "__VA_ARGS__") {
                    this.errorReporter.ReportError(ERRORS.RUNTIME.PRINTF.FORMAT_STRING_BUG, $child.line);
                }
            }
        }

        private getTextInside($child : IToken, $pattern : any) : string {
            const openingPunctuation : string[] = ["(", "["];
            const closingPunctuation : string[] = [")", "]"];

            const match : RegExpExecArray = $pattern.exec($child.body);
            if (!match) {
                return null;
            }

            const text : string = $child.body;
            const startPosition : number = StringUtils.IndexOf($child.body, match[0]) + StringUtils.Length(match[0]);
            const firstBrace : string = text[startPosition - 1];
            const punctuationStack : string[] = [closingPunctuation[openingPunctuation.indexOf(firstBrace)]];

            let position : number = startPosition;
            while (punctuationStack.length > 0 && position < StringUtils.Length($child.body)) {
                if (text[position] === punctuationStack[punctuationStack.length - 1]) {
                    punctuationStack.pop();
                } else if (openingPunctuation.indexOf(text[position]) !== -1) {
                    punctuationStack.push(
                        closingPunctuation[openingPunctuation.indexOf(text[position])]
                    );
                }
                position++;
            }

            return StringUtils.Substring(text, startPosition, position - 1);
        }
    }

    class ErrorToFix extends EventArgs {
        private lintErrorReference : LintError;
        private pInfo : BaseProcessingInfo;
        private rule : IBaseLintRule;

        public LintErrorReference($lintError? : LintError) : LintError {
            if (!ObjectValidator.IsEmptyOrNull($lintError)) {
                this.lintErrorReference = $lintError;
            }
            return this.lintErrorReference;
        }

        public ProcesingInfo($pInfo? : BaseProcessingInfo) : BaseProcessingInfo {
            if (!ObjectValidator.IsEmptyOrNull($pInfo)) {
                this.pInfo = $pInfo;
            }
            return this.pInfo;
        }

        public Rule($rule? : IBaseLintRule) : IBaseLintRule {
            if (!ObjectValidator.IsEmptyOrNull($rule)) {
                this.rule = $rule;
            }
            return this.rule;
        }
    }
}
