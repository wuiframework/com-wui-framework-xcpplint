/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Comment = Com.Wui.Framework.XCppLint.Core.Structures.Comment;
    import Directive = Com.Wui.Framework.XCppLint.Core.Structures.Directive;
    import CharType = Com.Wui.Framework.XCppLint.Enums.CharType;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;
    import TokenInfo = Com.Wui.Framework.XCppLint.Core.Model.TokenInfo;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;
    import ReplacementPair = Com.Wui.Framework.XCppLint.Structures.ReplacementPair;
    import SpecType = Com.Wui.Framework.XCppLint.Enums.SpecType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class CodeBlocksParser extends BaseObject {
        private indices : ArrayList<TokenInfo>;
        private bracketsStack : ArrayList<CharType>;
        private lastCharType : CharType;
        private lastIndex : number;
        private lineCounter : number;
        private colCounter : number;
        private lineHasSemicolon : boolean;
        private readonly errorReporter : ErrorReporter;
        private config : Config;

        public static getLeadingNewlines($input : string) : string {
            const match : RegExpExecArray = /^(\n+)/.exec($input);
            if (!match) {
                return "";
            }
            return match[1];
        }

        constructor() {
            super();
            this.indices = new ArrayList<TokenInfo>();
            this.bracketsStack = new ArrayList<CharType>();
            this.errorReporter = new ErrorReporter();
            this.lastCharType = CharType.INIT;
            this.lastIndex = 0;
            this.lineCounter = 1;
            this.colCounter = 1;
            this.lineHasSemicolon = false;
            this.config = new Config(this.errorReporter);
        }

        /**
         * @param {string} $filePath Set file name to which all errors will belong.
         */
        public setOwner($filePath : string) : void {
            this.errorReporter.setOwner($filePath);
        }

        /**
         * @param {Config} $config Config class to load.
         */
        public LoadConfig($config : Config) : void {
            this.config = $config;
        }

        /**
         * @param {string} $data String to create token map from.
         * @param {ArrayList<ReplacementPair>} $replacementPairs Array of original strings and their replacement in the modified file.
         * @return {IToken} Return the root of the resulting map.
         */
        public Parse($data : string, $replacementPairs : ArrayList<ReplacementPair> = null) : IToken {
            this.lastCharType = CharType.INIT;
            this.bracketsStack.Clear();
            this.colCounter = 0;
            this.indices.Clear();
            this.lineHasSemicolon = false;
            this.lastIndex = 0;
            this.lineCounter = 1;

            const rememberPair : any = ($pair : ReplacementPair) : void => {
                if ($replacementPairs) {
                    const replacementPair : ReplacementPair = new ReplacementPair();
                    replacementPair.Replacement($pair.Replacement());
                    replacementPair.Original($pair.Original());
                    $replacementPairs.Add(replacementPair);
                }
            };

            const matches : RegExpExecArray[] = Utils.FindAllMatches(new RegExp("(?<!\\\\)\\\\\\\""), $data);

            matches.reverse().forEach(($match : RegExpExecArray) => {
                const before : string = StringUtils.Substring($data, 0, $match.index);
                const after : string = StringUtils.Substring($data, $match.index + $match[0].length);

                const toRemove : string = "\\\"";

                const replacementString : string = this.generateReplacementString(toRemove);

                const replacementPair : ReplacementPair = new ReplacementPair();
                replacementPair.Original(toRemove);
                replacementPair.Replacement(replacementString);
                rememberPair(replacementPair);

                $data = before + replacementString + after;
            });

            let re : RegExp;
            let strings : string[];

            // remove chars, i.e '/'
            {
                re = /'(.*?)'/;

                let chars : string[] = Utils.FindAllStrings(re, 1, $data);

                chars = chars.filter(($elem : string, $index : number, $self : string[]) : boolean => {
                    return ($index === $self.indexOf($elem));
                });

                chars.forEach(($value : string) => {
                    const replacementString : string = this.generateReplacementString($value);
                    const replacementPair : ReplacementPair = new ReplacementPair();

                    replacementPair.Original($value);
                    replacementPair.Replacement(replacementString);

                    rememberPair(replacementPair);

                    $data = StringUtils.Replace($data,
                        "'" + replacementPair.Original() + "'",
                        "'" + replacementPair.Replacement() + "'");
                });
            }

            // remove R strings
            {
                re = /R"\((.*?)\)"/;
                strings = Utils.FindAllStrings(re, 1, $data);

                strings.forEach(($value : string) => {
                    const replacementString : string = this.generateReplacementString($value);

                    const replacementPair : ReplacementPair = new ReplacementPair();
                    replacementPair.Original($value);
                    replacementPair.Replacement(replacementString);
                    rememberPair(replacementPair);

                    $data = StringUtils.Replace($data, $value, replacementString);
                });
            }

            strings = [];

            // remove other strings
            {
                re = /#?\w*\s*\"(.*?)\"/;

                const stringMatches : RegExpExecArray[] = Utils.FindAllMatches(re, $data);

                stringMatches.forEach(($value : RegExpExecArray) : void => {
                    if (!StringUtils.StartsWith($value[0].trim(), "#")) {
                        strings.push($value[1]);
                    }
                });

                strings = strings.filter(($elem : string, $index : number, $self : string[]) : boolean => {
                    return $index === $self.indexOf($elem);
                });

                strings.forEach(($value : string) => {
                    const replacementString : string = this.generateReplacementString($value);

                    const replacementPair : ReplacementPair = new ReplacementPair();
                    replacementPair.Original($value);
                    replacementPair.Replacement(replacementString);
                    rememberPair(replacementPair);

                    $data = StringUtils.Replace($data,
                        "\"" + replacementPair.Original() + "\"",
                        "\"" + replacementPair.Replacement() + "\"");
                });
            }

            if ($data[$data.length - 1] !== "\n") {
                const errorObject : any = ERRORS.OTHER.FILE_NEW_LINE;
                this.errorReporter.ReportError(errorObject, 0, -1);

                if (this.config.ShouldFix(errorObject)) {
                    $data += "\n";
                    this.errorReporter.getAllErrors().getLast().Fixed();
                }
            }

            $data = StringUtils.Replace($data, CharType.CRLF, CharType.LF);

            for (let i = 0; i <= StringUtils.Length($data); i++) {
                this.handleChar($data, i);
            }

            this.checkBracketsStackEmpty();

            this.parseDirectives($data);

            this.parseComments($data);

            return this.createTokenMap($replacementPairs);
        }

        /**
         * @return {ArrayList<LintError>} Returns an array of all errors that were generated during parsing.
         */
        public getAllErrors() : ArrayList<LintError> {
            return this.errorReporter.getAllErrors();
        }

        private generateReplacementString($input : string) : string {
            const random : string = Math.random().toString();
            const time : string = Date.now().toString();
            return StringUtils.getSha1($input + random + time);
        }

        private checkSpace($char : string) : boolean {
            return (/\s/.test($char));
        }

        private checkAlnum($char : string) : boolean {
            return (/^[a-z0-9A-Z]+$/.test($char));
        }

        private getCharType($data : string) : CharType {
            if ($data === "(") {
                return CharType.L_ROUNDS;
            }

            if ($data === ")") {
                return CharType.R_ROUNDS;
            }

            if ($data === "{") {
                return CharType.L_CURLY;
            }

            if ($data === "}") {
                return CharType.R_CURLY;
            }

            if ($data === "[") {
                return CharType.L_SQUARE;
            }

            if ($data === "]") {
                return CharType.R_SQUARE;
            }

            if ($data === ";") {
                return CharType.SEMICOLON;
            }

            if ($data === "=") {
                return CharType.EQUAL;
            }

            if ($data === "\t") {
                return CharType.TAB;
            }

            if ($data === "\0") {
                return CharType.NUL;
            }

            if ($data === "\"") {
                return CharType.STRING_QUOTE;
            }

            if ($data === "\ufffd") {
                return CharType.INVALID_UTF;
            }

            if ($data === CharType.CRLF || $data === CharType.LF) {
                return CharType.NEWLINE;
            }

            if (this.checkSpace($data)) {
                return CharType.SPACE;
            }

            if (this.checkAlnum($data) || $data === "_") {
                return CharType.ALNUM;
            }

            return CharType.OTHER;
        }

        private checkBracketsStackEmpty() : void {
            if (this.bracketsStack.Length() !== 0) {
                this.errorReporter.ReportError(ERRORS.OTHER.INVALID_BRACKETS, 0);
            }
        }

        private handleChar($data : string, $idx : number) : void {
            if ($idx === StringUtils.Length($data)) {
                this.indices.Add(
                    new TokenInfo(
                        this.lastIndex,
                        $idx,
                        0,
                        0,
                        this.lastCharType,
                        StringUtils.Substring($data, this.lastIndex, $idx)));
                return;
            }

            const char : string = $data[$idx];

            if (this.lastCharType === CharType.INIT) {
                this.lastCharType = this.getCharType(char);
                this.lastIndex = $idx;
                return;
            }

            const charType : CharType = this.getCharType(char);

            const col : number = this.colCounter;
            const line : number = this.lineCounter;

            if (charType === CharType.NEWLINE) {
                this.lineCounter++;
                this.colCounter = 1;
            }

            if (this.lastCharType !== charType ||
                (this.lastCharType === CharType.R_CURLY && charType === CharType.R_CURLY) ||
                (this.lastCharType === CharType.L_CURLY && charType === CharType.L_CURLY)) {
                this.indices.Add(
                    new TokenInfo(
                        this.lastIndex,
                        $idx,
                        line,
                        col - StringUtils.Length(StringUtils.Substring($data, this.lastIndex, $idx)),
                        this.lastCharType,
                        StringUtils.Substring($data, this.lastIndex, $idx)));
                this.lastIndex = $idx;
                this.lastCharType = charType;
            }

            this.colCounter++;

            this.verifyMatchingBracket(charType);
        }

        private parseDirectives($data : string) : void {
            const instance : DirectivesParser = new DirectivesParser();
            const directives : ArrayList<Directive> = instance.Parse($data);

            const isIndexDirective = ($index : number) : boolean => {
                for (let i = 0; i < directives.Length(); i++) {
                    const indexStart = directives.getItem(i).StartIndex();
                    const indexEnd = directives.getItem(i).EndIndex();
                    if ($index >= indexStart && $index < indexEnd) {
                        return true;
                    }
                }
                return false;
            };

            const invalidIndices : ArrayList<number> = new ArrayList<number>();

            for (let i = 0; i < this.indices.Length(); i++) {
                if (isIndexDirective(this.indices.getItem(i).getStartIndex())) {
                    invalidIndices.Add(i);
                }
            }

            for (let i = 0; i < invalidIndices.Length(); i++) {
                this.indices.Add(new TokenInfo(
                    this.indices.getItem(invalidIndices.getItem(i)).getStartIndex(),
                    this.indices.getItem(invalidIndices.getItem(i)).getEndIndex(),
                    this.indices.getItem(invalidIndices.getItem(i)).getLine(),
                    this.indices.getItem(invalidIndices.getItem(i)).getCol(),
                    TokenType.DIRECTIVE,
                    this.indices.getItem(invalidIndices.getItem(i)).getValue()), invalidIndices.getItem(i));
            }
        }

        private parseComments($data : string) : void {
            const instance : CommentsParser = new CommentsParser();
            const comments : ArrayList<Comment> = instance.Parse($data);

            const isIndexComment = ($index : number) : boolean => {
                for (let i = 0; i < comments.Length(); i++) {
                    const indexStart = comments.getItem(i).StartIndex();
                    const indexEnd = comments.getItem(i).EndIndex();
                    if ($index >= indexStart && $index < indexEnd) {
                        return true;
                    }
                }
                return false;
            };

            const invalidIndices : ArrayList<number> = new ArrayList<number>();

            for (let i = 0; i < this.indices.Length(); i++) {
                if (isIndexComment(this.indices.getItem(i).getStartIndex())) {
                    invalidIndices.Add(i);
                }
            }

            for (let i = 0; i < invalidIndices.Length(); i++) {
                this.indices.Add(new TokenInfo(
                    this.indices.getItem(invalidIndices.getItem(i)).getStartIndex(),
                    this.indices.getItem(invalidIndices.getItem(i)).getEndIndex(),
                    this.indices.getItem(invalidIndices.getItem(i)).getLine(),
                    this.indices.getItem(invalidIndices.getItem(i)).getCol(),
                    TokenType.COMMENT,
                    this.indices.getItem(invalidIndices.getItem(i)).getValue()), invalidIndices.getItem(i));
            }
        }

        private createTokenMap($replacementPairs : ArrayList<ReplacementPair>) : IToken {
            const cleanToken = () : IToken => {
                return <IToken>{
                    body        : "",
                    childIndex  : 0,
                    children    : [],
                    line        : 0,
                    nesting     : 0,
                    originalBody: "",
                    parent      : null,
                    specType    : SpecType.PRIVATE,
                    type        : TokenType.BLOCK
                };
            };

            const cleanCommentToken = ($item : TokenInfo) : IToken => {
                return <IToken>{
                    body    : "",
                    children: [],
                    line    : $item.getLine(),
                    type    : $item.getType()
                };
            };

            let end : number = 0;
            let skipFor : boolean = false;
            let commentOrDirective : IToken = null;

            let lastChild : IToken = null;

            const read = ($parent : IToken, $child : IToken) : void => {
                while (true) {
                    $child.parent = $parent;

                    if (end >= this.indices.Length()) {
                        if (commentOrDirective) {
                            $parent.children.push(commentOrDirective);
                            return;
                        }

                        $child.childIndex = $parent.children.length;
                        const last : TokenInfo = this.indices.getItem(end - 1);
                        $child.body = (last ? last.getValue() : "");
                        $parent.children.push($child);
                        return;
                    }

                    const item : TokenInfo = this.indices.getItem(end);

                    if (item.getType() !== TokenType.COMMENT &&
                        item.getType() !== TokenType.DIRECTIVE) {
                        if (commentOrDirective) {
                            const leadingNewlines : string = CodeBlocksParser.getLeadingNewlines($child.body);
                            if (leadingNewlines) {
                                $child.body = StringUtils.Substring($child.body, leadingNewlines.length);
                                commentOrDirective.body = leadingNewlines + commentOrDirective.body;
                            }

                            commentOrDirective.childIndex = $parent.children.length;
                            $parent.children.push(commentOrDirective);
                            commentOrDirective = null;
                        }

                        if (StringUtils.IsEmpty($child.body.trim())) {
                            $child.line = item.getLine();
                        }

                        const subPart : string = item.getValue();

                        $child.body += subPart;

                        if (subPart === "for") {
                            skipFor = true;
                        }

                        if (item.getType() === CharType.INVALID_UTF) {
                            this.errorReporter.ReportError(ERRORS.READABILITY.UTF8, $child.line);
                        }

                        if (item.getType() === CharType.NUL) {
                            this.errorReporter.ReportError(ERRORS.READABILITY.NUL, $child.line);
                        }

                        if (!skipFor && item.getType() === CharType.SEMICOLON) {
                            if ($child.body === ";") {
                                lastChild.body += ";";
                            } else {
                                $child.childIndex = $parent.children.length;
                                $child.originalBody = Utils.RestoreOriginalString($child.body, $replacementPairs);

                                $parent.children.push($child);
                                lastChild = $child;
                            }
                            $child = cleanToken();
                        } else if (item.getType() === CharType.L_CURLY) {
                            end++;
                            skipFor = false;

                            $child.childIndex = $parent.children.length;

                            lastChild = $child;
                            read($child, cleanToken());

                            $child.originalBody = Utils.RestoreOriginalString($child.body, $replacementPairs);
                            $parent.children.push($child);
                            $child = cleanToken();
                        } else if (item.getType() === CharType.R_CURLY) {
                            $child.childIndex = $parent.children.length;
                            $child.originalBody = Utils.RestoreOriginalString($child.body, $replacementPairs);
                            $parent.children.push($child);
                            lastChild = $child;
                            return;
                        } else if (
                            StringUtils.EndsWith($child.body, " public:") ||
                            StringUtils.EndsWith($child.body, " private:") ||
                            StringUtils.EndsWith($child.body, " protected:")) {
                            $child.childIndex = $parent.children.length;
                            $child.originalBody = Utils.RestoreOriginalString($child.body, $replacementPairs);
                            $parent.children.push($child);
                            lastChild = $child;
                            $child = cleanToken();
                        }
                    } else {
                        if (!commentOrDirective) {
                            commentOrDirective = cleanCommentToken(item);
                        }

                        if (commentOrDirective.type !== item.getType()) {
                            const leadingNewlines : string = CodeBlocksParser.getLeadingNewlines($child.body);
                            if (leadingNewlines) {
                                $child.body = StringUtils.Substring($child.body, leadingNewlines.length);
                                commentOrDirective.body = leadingNewlines + commentOrDirective.body;
                            }

                            commentOrDirective.childIndex = $parent.children.length;
                            $parent.children.push(commentOrDirective);
                            commentOrDirective = cleanCommentToken(item);
                        }

                        commentOrDirective.body += item.getValue();
                    }
                    end++;
                }
            };

            const parent : IToken = cleanToken();

            read(parent, cleanToken());

            return parent;
        }

        private verifyMatchingBracket($bracketType : CharType) : void {
            if ($bracketType < CharType.L_ROUNDS || $bracketType > CharType.R_SQUARE) {
                return;
            }

            // round brackets
            if ($bracketType === CharType.L_ROUNDS) {
                this.bracketsStack.Add(CharType.L_ROUNDS);
                return;
            }

            if ($bracketType === CharType.R_ROUNDS) {
                if (this.bracketsStack.getLast() !== CharType.L_ROUNDS) {
                    this.errorReporter.ReportError(ERRORS.OTHER.INVALID_BRACKETS, 0);
                } else {
                    this.bracketsStack.RemoveLast();
                }
                return;
            }

            // curly brackets
            if ($bracketType === CharType.L_CURLY) {
                this.bracketsStack.Add(CharType.L_CURLY);
                return;
            }

            if ($bracketType === CharType.R_CURLY) {
                if (this.bracketsStack.getLast() !== CharType.L_CURLY) {
                    this.errorReporter.ReportError(ERRORS.OTHER.INVALID_BRACKETS, 0);
                } else {
                    this.bracketsStack.RemoveLast();
                }
                return;
            }

            // square brackets
            if ($bracketType === CharType.L_SQUARE) {
                this.bracketsStack.Add(CharType.L_SQUARE);
                return;
            }

            if ($bracketType === CharType.R_SQUARE) {
                if (this.bracketsStack.getLast() !== CharType.L_SQUARE) {
                    this.errorReporter.ReportError(ERRORS.OTHER.INVALID_BRACKETS, 0);
                } else {
                    this.bracketsStack.RemoveLast();
                }
                return;
            }
        }
    }
}
