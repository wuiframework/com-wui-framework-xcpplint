/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.HttpProcessor {
    "use strict";

    export class HttpResolver extends Com.Wui.Framework.Commons.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Com.Wui.Framework.XCppLint.Index;
            resolvers["/index"] = Com.Wui.Framework.XCppLint.Index;
            resolvers["/web/"] = Com.Wui.Framework.XCppLint.Index;
            resolvers["/web/LinterTest"] = RuntimeTests.LinterTest;
            return resolvers;
        }
    }
}
