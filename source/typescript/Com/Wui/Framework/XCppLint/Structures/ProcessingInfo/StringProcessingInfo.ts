/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Structures.ProcessingInfo {
    "use strict";

    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class StringProcessingInfo extends BaseProcessingInfo {
        private commonCondition : boolean;

        constructor() {
            super();
            this.Clear();
        }

        public Clear() : void {
            this.commonCondition = false;
        }

        public CommonCondition($commonCondition? : boolean) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($commonCondition)) {
                this.commonCondition = $commonCondition;
            }
            return this.commonCondition;
        }
    }
}
