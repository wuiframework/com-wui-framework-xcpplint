/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Before {
    "use strict";
    import BaseBefore = Com.Wui.Framework.XCppLint.Structures.BaseBefore;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import TemplateProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.TemplateProcessingInfo;

    export class TemplateBefore extends BaseBefore {
        private static instance : TemplateBefore;
        private processingInfo : TemplateProcessingInfo;

        public static getInstance() : BaseBefore {
            if (!TemplateBefore.instance) {
                TemplateBefore.instance = new TemplateBefore();
            }
            return TemplateBefore.instance;
        }

        constructor() {
            super();
            this.processingInfo = new TemplateProcessingInfo();
        }

        public Reset() : void {
            super.Reset();
            this.processingInfo.Clear();
        }

        public getResults($processingInfo : BaseProcessingInfo) : TemplateProcessingInfo {
            this.run($processingInfo);
            return this.processingInfo;
        }

        private run($processingInfo : BaseProcessingInfo) : void {
            if (!this.Done()) {
                const regex : RegExp = /template(\s*)<(\s*[\w\s,]*\s*)>/gm;
                this.processingInfo.Matches(regex.exec($processingInfo.Child().body));

                this.Done(true);
            }
        }
    }
}
