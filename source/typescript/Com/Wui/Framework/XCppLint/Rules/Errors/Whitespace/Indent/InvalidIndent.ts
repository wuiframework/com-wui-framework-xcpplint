/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Indent {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;
    import Config = Com.Wui.Framework.XCppLint.Core.Config;

    export class InvalidIndent extends BaseLintRule {

        private expectedIndentSize : number;

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.INDENT.INVALID_INDENT);
            this.setSensitivity(TokenType.BLOCK, TokenType.COMMENT);
            this.expectedIndentSize = 4;
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const startsWithSpaces = ($body : string, $numberOfSpaces : number) : boolean => {
                const match : RegExpExecArray = /(?:\n*)( *).*/gm.exec($body);
                const spaces : string = match[1];
                return $numberOfSpaces === spaces.length;
            };

            if (this.requiresNewLine($processingInfo.Parent().body) ||
                BaseLintRule.isDo($processingInfo.Parent().body) ||
                BaseLintRule.isTokenInExtern($processingInfo.Child())) {
                if (!$processingInfo.Config().ShouldIgnore(this.getErrorObject())) {
                    if ($processingInfo.Child().body.trim() === ";" ||
                        $processingInfo.Child().body.trim() === ");" ||
                        $processingInfo.Child().body.trim() === ")) {" ||
                        $processingInfo.Child().body.trim() === "else {" ||
                        StringUtils.StartsWith($processingInfo.Child().body.trim(), "else if (") ||
                        StringUtils.StartsWith($processingInfo.Child().body.trim(), "catch")) {
                        return;
                    }

                    if ($processingInfo.PrevChild() && $processingInfo.PrevChild().body.trim() === "do {" &&
                        StringUtils.StartsWith($processingInfo.Child().body.trim(), "while (")) {
                        return;
                    }

                    if (StringUtils.Contains($processingInfo.Parent().body, "regex")) {
                        return;
                    }

                    if (StringUtils.EndsWith($processingInfo.Parent().body.trim(), ">{")) {
                        return;
                    }

                    if (StringUtils.StartsWith($processingInfo.Parent().body.trim(), "struct")) {
                        return;
                    }

                    // ignore comments as unused parameters
                    if ($processingInfo.PrevChild() && $processingInfo.PrevChild().line === $processingInfo.Child().line &&
                        $processingInfo.PrevChild().type === TokenType.COMMENT) {
                        return;
                    }
                    if ($processingInfo.Child().type === TokenType.COMMENT &&
                        $processingInfo.NextToken() && $processingInfo.NextToken().line === $processingInfo.Child().line &&
                        $processingInfo.NextToken().type === TokenType.BLOCK) {
                        return;
                    }

                    if ($processingInfo.PrevToken()) {
                        if ($processingInfo.Child().type === TokenType.COMMENT) {
                            if ($processingInfo.PrevToken().line === $processingInfo.Child().line) {
                                return;
                            }
                        }
                        if ($processingInfo.PrevToken().type === TokenType.DIRECTIVE) {
                            return;
                        }
                    }

                    if (StringUtils.EndsWith($processingInfo.Parent().body, "{") &&
                        $processingInfo.Child().type === TokenType.COMMENT &&
                        $processingInfo.Parent().type === TokenType.BLOCK &&
                        $processingInfo.Child().line === $processingInfo.Parent().line &&
                        StringUtils.Contains($processingInfo.Child().body, "LINT")) {
                        return;
                    }

                    // filter one-line methods in class or their implementations
                    if (BaseLintRule.isTokenInClass($processingInfo.Child())) {
                        if (BaseLintRule.isFunction($processingInfo.Parent().body) &&
                            ($processingInfo.Parent().children.length === 2 || $processingInfo.Parent().children.length === 1)) {
                            return;
                        }
                    }

                    // filter one-line ctors and dtors in class or their implementations
                    if ((BaseLintRule.isDtor($processingInfo.Parent().body) || BaseLintRule.isCtor($processingInfo.Parent().body)) &&
                        $processingInfo.Child().body.trim() === "}" &&
                        ($processingInfo.Parent().children.length === 2 || $processingInfo.Parent().children.length === 1)) {
                        return;
                    }

                    if (!BaseLintRule.isTokenInClass($processingInfo.Child()) &&
                        BaseLintRule.isFunction($processingInfo.Parent().body) &&
                        $processingInfo.Parent().children.length === 1) {
                        return;
                    }

                    if (StringUtils.Contains($processingInfo.Parent().body, "json")) {
                        return;
                    }

                    if ($processingInfo.Child().body === "));") {
                        return;
                    }

                    let nesting : number = $processingInfo.Child().nesting;

                    let visibilitySpecIndent : number = 0;
                    if ($processingInfo.Child().body.trim() === "public:" ||
                        $processingInfo.Child().body.trim() === "protected:" ||
                        $processingInfo.Child().body.trim() === "private:") {
                        visibilitySpecIndent = 1 - this.expectedIndentSize;
                    }

                    if ($processingInfo.Child().body.trim() === "}" ||
                        $processingInfo.Child().body.trim() === "};") {
                        nesting -= 1;
                    }

                    if (BaseLintRule.isTokenInExtern($processingInfo.Child())) {
                        if (nesting > 0) {
                            nesting -= 1;
                        }
                    }

                    const indentCorrect : boolean = startsWithSpaces(
                        $processingInfo.Child().body,
                        (nesting * this.expectedIndentSize) + visibilitySpecIndent
                    );

                    if (!indentCorrect) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const cleanBody : string = $pinfo.Child().body.trim();
            const beforeSpaces : string = /(\n*) */.exec($pinfo.Child().body)[1];
            const beforeEnd : string = /.*(\n*$)/.exec($pinfo.Child().body)[1];
            let spaces : string;
            let numOfSpaces : number = 0;

            if ($pinfo.Child().body.trim() === "public:" ||
                $pinfo.Child().body.trim() === "protected:" ||
                $pinfo.Child().body.trim() === "private:") {
                numOfSpaces = $pinfo.Child().nesting * this.expectedIndentSize - this.expectedIndentSize + 1;
            } else if ($pinfo.Child().body.trim() === "}" || $pinfo.Child().body.trim() === "};") {
                numOfSpaces = ($pinfo.Child().nesting - 1) * this.expectedIndentSize;
            } else {
                numOfSpaces = $pinfo.Child().nesting * this.expectedIndentSize;
            }

            if (BaseLintRule.isTokenInExtern($pinfo.Child())) {
                numOfSpaces -= this.expectedIndentSize;
            }

            spaces = this.generateSpaces(numOfSpaces);

            $pinfo.Child().body = beforeSpaces + spaces + cleanBody + beforeEnd;

            return true;
        }

        public LoadConfig($config : Config) : void {
            const temp : number = (<number>$config.getPropertyValue("spaces"));
            if (temp) {
                this.expectedIndentSize = temp;
            }
        }
    }
}
