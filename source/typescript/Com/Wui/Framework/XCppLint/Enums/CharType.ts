/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class CharType extends BaseEnum {
        public static readonly INIT : string = ":init:";
        public static readonly L_ROUNDS : string = "(";
        public static readonly R_ROUNDS : string = ")";
        public static readonly L_CURLY : string = "{";
        public static readonly R_CURLY : string = "}";
        public static readonly L_SQUARE : string = "[";
        public static readonly R_SQUARE : string = "]";
        public static readonly SEMICOLON : string = ";";
        public static readonly EQUAL : string = "=";
        public static readonly SPACE : string = " ";
        public static readonly ALNUM : string = ":alnum:";
        public static readonly NEWLINE : string = ":newline:";
        public static readonly CRLF : string = "\r\n";
        public static readonly LF : string = "\n";
        public static readonly TAB : string = "\t";
        public static readonly NUL : string = "\0";
        public static readonly STRING_QUOTE : string = "\"";
        public static readonly INVALID_UTF : string = "\ufffd";
        public static readonly OTHER : string = ":other:";
    }
}
