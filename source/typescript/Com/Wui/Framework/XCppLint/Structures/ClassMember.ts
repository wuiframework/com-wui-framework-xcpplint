/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Structures {
    "use strict";

    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ClassMember {
        private member : IToken;
        private comment : IToken;

        /**
         * @param {IToken} $member If set, set this.member to this value.
         * @return {IToken} Returns a member of a class.
         */
        public Member($member? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($member)) {
                this.member = $member;
            }
            return this.member;
        }

        /**
         * @param {IToken} $comment Set associated comment.
         * @return {IToken} Return a token associated with a memeber of a class.
         */
        public Comment($comment? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($comment)) {
                this.comment = $comment;
            }
            return this.comment;
        }
    }
}
