/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint {
    "use strict";
    import HttpResolver = Com.Wui.Framework.XCppLint.HttpProcessor.HttpResolver;

    /**
     * Loader class provides handling of application content singleton.
     */
    export class Loader extends Com.Wui.Framework.Commons.Loader {

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver(this.getEnvironmentArgs().getProjectName());
        }
    }
}
