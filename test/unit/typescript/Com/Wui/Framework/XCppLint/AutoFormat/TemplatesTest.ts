/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class TemplatesTest extends UnitTestRunner {
        private configData : any;

        constructor() {
            super();

            this.configData = {
                extensions: [
                    "cpp",
                    "hpp"
                ],
                fix       : [
                    "errors-whitespace-newline-missingAfterOpenBrace",
                    "errors-other-fileNewLine",
                    "errors-whitespace-newline-multipleCommands",
                    "errors-whitespace-newline-missingBeforeCloseBrace",
                    "errors-whitespace-newline-missingAfterCloseBrace",
                    "errors-whitespace-newline-missingAfterVisibilitySpec",
                    "errors-whitespace-newline-missingAfterComment",
                    "errors-whitespace-newline-missingBeforeComment",
                    "errors-whitespace-indent-invalidIndent",
                    "errors-whitespace-indent-initializerListIndent",
                    "errors-whitespace-blankLine-preceded",
                    "errors-whitespace-newline-missingAfterTemplate",
                    "errors-whitespace-newline-missingBeforeDecDef",
                    "errors-whitespace-newline-missingBeforeDo",
                    "errors-readability-wui-methodOrder",
                    "errors-readability-wui-classOrder",
                    "errors-readability-wui-lineEndings",
                    "errors-whitespace-commaInFunction"
                ],
                ignore    : [],
                lineEnding: "<LF>",
                maxLineLen: 140,
                spaces    : 4
            };
        }

        public testBasic() : void {
            const baseDir : string = "test/resource/data/Com/Wui/Framework/XCppLint/AutoFormat/" +
                "Basic/";
            const inOutPairs : ArrayList<[string, string]> = new ArrayList<[string, string]>();
            inOutPairs.Add(["test1In.hpp", "test1Out.hpp"]);
            inOutPairs.Add(["test2In.hpp", "test2Out.hpp"]);
            inOutPairs.Add(["test3In.hpp", "test3Out.hpp"]);
            inOutPairs.Add(["test4In.hpp", "test4Out.hpp"]);
            inOutPairs.Add(["test5In.hpp", "test5Out.hpp"]);
            inOutPairs.Add(["test6In.hpp", "test6Out.hpp"]);
            inOutPairs.Add(["test7In.hpp", "test7Out.hpp"]);

            this.checkPairs(inOutPairs, baseDir);

            this.initSendBox();
        }

        public testTemplates() : void {
            const baseDir : string = "test/resource/data/Com/Wui/Framework/XCppLint/AutoFormat/" +
                "testAllNodes/";
            const inOutPairs : ArrayList<[string, string]> = new ArrayList<[string, string]>();

            inOutPairs.Add(["include/BaseContext.hpp", "include/BaseContextOut.hpp"]);
            inOutPairs.Add(["include/BaseGraph.hpp", "include/BaseGraphOut.hpp"]);
            inOutPairs.Add(["include/Context0.hpp", "include/Context0Out.hpp"]);
            inOutPairs.Add(["include/Graph0.hpp", "include/Graph0Out.hpp"]);
            inOutPairs.Add(["include/IManager.hpp", "include/IManagerOut.hpp"]);
            inOutPairs.Add(["include/Manager.hpp", "include/ManagerOut.hpp"]);
            inOutPairs.Add(["include/Reference.hpp", "include/ReferenceOut.hpp"]);
            inOutPairs.Add(["include/VisualGraph.hpp", "include/VisualGraphOut.hpp"]);
            inOutPairs.Add(["source/BaseContext.cpp", "source/BaseContextOut.cpp"]);
            inOutPairs.Add(["source/BaseGraph.cpp", "source/BaseGraphOut.cpp"]);
            inOutPairs.Add(["source/Context0.cpp", "source/Context0Out.cpp"]);
            inOutPairs.Add(["source/Graph0.cpp", "source/Graph0Out.cpp"]);
            inOutPairs.Add(["source/main.cpp", "source/mainOut.cpp"]);
            inOutPairs.Add(["source/Manager.cpp", "source/ManagerOut.cpp"]);
            inOutPairs.Add(["source/VisualGraph.cpp", "source/VisualGraphOut.cpp"]);

            this.checkPairs(inOutPairs, baseDir);

            this.initSendBox();
        }

        private checkPairs($pairs : ArrayList<[string, string]>, $baseDir : string) : void {
            const linter : Linter = new Linter();

            $pairs.foreach(($pair : [string, string]) : void => {
                const fileInPath : string = $baseDir + $pair[0];
                const fileOutPath : string = $baseDir + $pair[1];

                const fileInData : string = this.readFileRelative(fileInPath, this);
                const fileOutData : string = this.readFileRelative(fileOutPath, this);

                linter.Run(fileInData, fileInPath, this.configData,
                    ($formattedFileData : string) : void => {
                        assert.equal($formattedFileData, fileOutData, "Failed for: " + $pair[0]);
                    });
            });
        }
    }
}
