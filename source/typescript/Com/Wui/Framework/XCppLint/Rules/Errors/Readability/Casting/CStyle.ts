/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Readability.Casting {
    "use strict";
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;

    export class CStyle extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.CASTING.C_STYLE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const expectingFunctionArgs = ($body : string) : boolean => {
                return (/\bstd::m?function\s*\<\s*$/gm.test($body));
            };

            const expectingFunction : boolean = expectingFunctionArgs($processingInfo.Child().body);

            if (!expectingFunction) {
                this.checkCStyleCast($processingInfo.Parent(),
                    $processingInfo.Child(),
                    "static_cast", /\((int|float|double|bool|char|u?int(16|32|64))\)/gm,
                    $onError);
            }

            if (!this.checkCStyleCast($processingInfo.Parent(),
                $processingInfo.Child(),
                "const_cast", /\((char\s?\*+\s?)\)\s*"/gm,
                $onError)) {
                this.checkCStyleCast($processingInfo.Parent(),
                    $processingInfo.Child(),
                    "reinterpret_cast", /\((\w+\s?\*+\s?)\)/gm,
                    $onError);
            }
        }

        private checkCStyleCast($parent : IToken, $child : IToken, $castType : string, $pattern : any,
                                $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : boolean {
            const match : RegExpExecArray = $pattern.exec($child.body);

            if (!match) {
                return false;
            }

            const matchStart : number = StringUtils.IndexOf($child.body, match[0]);
            const context : string = StringUtils.Substring($child.body, 0, matchStart - 1);

            if (/.*\b(?:sizeof|alignof|alignas|[_A-Z][_A-Z0-9]*)\s*$/gm.test(context)) {
                return false;
            }

            if (/.*\b[_A-Z][_A-Z0-9]*\s*\((?:\([^()]*\)|[^()])*$/gm.test(context)) {
                return false;
            }

            if (StringUtils.EndsWith(context, " operator++") || StringUtils.EndsWith(context, " operator--")) {
                return false;
            }

            const remainder = StringUtils.Substring($child.body, matchStart + StringUtils.Length(match[0]));

            if (/^\s*(?:;|const\b|throw\b|final\b|override\b|[=>{),]|->)/gm.test(remainder)) {
                return false;
            }

            $onError(this.getErrorObject(), $child.line, -1);

            return true;
        }
    }
}
