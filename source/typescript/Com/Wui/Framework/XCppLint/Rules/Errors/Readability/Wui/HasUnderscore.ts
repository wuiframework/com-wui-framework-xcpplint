/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Readability.Wui {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import WuiBefore = Com.Wui.Framework.XCppLint.Rules.Before.WuiBefore;
    import WuiProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.WuiProcessingInfo;

    export class HasUnderscore extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.WUI.HAS_UNDERSCORE);
            this.setOnBefore(WuiBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : WuiProcessingInfo = <WuiProcessingInfo>this.getOnBefore().getResults($processingInfo);

            const subArgs : string[] = pInfo.SubArg();

            subArgs.forEach(($value : string) : void => {
                if (StringUtils.Contains($value, "_")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            });

        }
    }
}
