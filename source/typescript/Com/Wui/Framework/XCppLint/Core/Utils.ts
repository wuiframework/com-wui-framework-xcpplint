/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ReplacementPair = Com.Wui.Framework.XCppLint.Structures.ReplacementPair;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class Utils extends BaseObject {

        public static FindAllStrings($regex : any, $indexOfMatch : number, $input : string, $flags = "gm") : string[] {
            let match;
            const result : string[] = [];
            const re = new RegExp($regex, $flags);

            do {
                match = re.exec($input);
                if (match) {
                    result.push(match[$indexOfMatch]);
                }
            } while (match);
            return result;
        }

        public static FindAllMatches($regex : any, $input : string, $flags = "gm") : RegExpExecArray[] {
            let match;
            const result : RegExpExecArray[] = new Array<RegExpExecArray>();
            const re = new RegExp($regex, $flags);

            do {
                match = re.exec($input);
                if (match) {
                    result.push(match);
                }
            } while (match);
            return result;
        }

        public static RestoreOriginalString($input : string, $replacementPairs : ArrayList<ReplacementPair> = null) : string {
            if (!$replacementPairs) {
                return $input;
            }

            let file : string = $input;

            for (let i = $replacementPairs.Length() - 1; i >= 0; i--) {
                file = StringUtils.Replace(
                    file,
                    $replacementPairs.getItem(i).Replacement(),
                    $replacementPairs.getItem(i).Original()
                );
            }

            return file;
        }
    }
}
