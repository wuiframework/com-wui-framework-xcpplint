/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Other {
    "use strict";
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import Config = Com.Wui.Framework.XCppLint.Core.Config;

    export class LineLen extends BaseLintRule {
        private maxLineLen : number;

        constructor() {
            super();
            this.setErrorObject(ERRORS.OTHER.LINE_LEN);
            this.maxLineLen = 140;
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const linesInChild : string[] = StringUtils.Split($processingInfo.Child().originalBody, "\n");
            linesInChild.forEach(($value : string) : void => {
                if (StringUtils.Length($value) > this.maxLineLen) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            });
        }

        public LoadConfig($config : Config) : void {
            const tempMaxLineLen : number = <number>$config.getPropertyValue("maxLineLen");
            if (tempMaxLineLen) {
                this.maxLineLen = tempMaxLineLen;
            }
        }
    }
}
