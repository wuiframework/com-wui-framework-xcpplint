/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Runtime {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Utils = Com.Wui.Framework.XCppLint.Core.Utils;

    export class NonConstRef extends BaseLintRule {

        private readonly RE_PATTERN_IDENT : string = "[_a-zA-Z\\$]\\w*";
        private readonly RE_PATTERN_TYPE : string = "(?:const\\s+)?(?:typename\\s+|class\\s+|struct\\s+|union\\s+|enum\\s+)?" +
            "(?:\\w|" + "\\s*<(?:<(?:<[^<>]*>|[^<>])*>|[^<>])*>|" + "::)+";
        private readonly RE_PATTERN_REF_PARAM : string = "(" + this.RE_PATTERN_TYPE + "(?:\\s*(?:\\bconst\\b|[*]))*\\s*" +
            "&\\s*" + this.RE_PATTERN_IDENT + ")\\s*(?:=[^,()]+)?[,)]";
        private readonly RE_PATTERN_CONST_REF_PARAM : string = "(?:.*\\s*\\bconst\\s*&\\s*" + this.RE_PATTERN_IDENT +
            "|const\\s+" + this.RE_PATTERN_TYPE + "\\s*&\\s*" + this.RE_PATTERN_IDENT + ")";
        private readonly RE_PATTERN_REF_STREAM_PARAM : string = "(?:.*stream\\s*&\\s*" + this.RE_PATTERN_IDENT + ")";

        constructor() {
            super();
            this.setErrorObject(ERRORS.RUNTIME.NON_CONST_REF);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (!StringUtils.Contains($processingInfo.Child().body, "&")) {
                return;
            }

            if (StringUtils.Contains($processingInfo.Child().body, "override")) {
                return;
            }

            if (/^([^()]*\w+)\(/gm.test($processingInfo.Child().body)) {
                if (/^[^()]*\w+::\w+\(/gm.test($processingInfo.Child().body)) {
                    return;
                }
            }

            // avoid initializer list
            if (/\s:\s/gm.test($processingInfo.Child().body)) {
                return;
            }

            if (BaseLintRule.isLambda($processingInfo.Child().body)) {
                return;
            }

            if (StringUtils.Contains($processingInfo.Child().body, " & ")) {    // " & " to avoid & usage as logical and
                return;
            }

            const whitelistedFunctions = /(?:[sS]wap(?:<\w:+>)?|operator\s*[<>][<>]|static_assert|COMPILE_ASSERT)\s*\(/gm;
            if (whitelistedFunctions.test($processingInfo.Child().body)) {
                return;
            }

            const arr : string[] = Utils.FindAllStrings(this.RE_PATTERN_REF_PARAM, 1, $processingInfo.Child().body);

            for (const parameter of arr) {
                if (!(new RegExp(this.RE_PATTERN_CONST_REF_PARAM, "gm")).test(parameter) &&
                    !(new RegExp(this.RE_PATTERN_REF_STREAM_PARAM, "gm")).test(parameter)) {
                    if (!(StringUtils.StartsWith($processingInfo.Child().body.trim(), "catch ") &&
                        StringUtils.StartsWith($processingInfo.PrevChild().body.trim(), "try "))) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }
    }
}
