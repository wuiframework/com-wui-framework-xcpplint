/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class Config extends BaseObject {
        private fixableErrors : ArrayList<string>;
        private ignoredErrors : ArrayList<string>;
        private extensions : ArrayList<string>;
        private ignoredFiles : ArrayList<string>;
        private readonly errorReporter : ErrorReporter;
        private readonly properties : ArrayList<IPropertyEntry>;

        /**
         * @param {ErrorReporter} $errorReporter Reference to error reporter, used for reverse errors lookup.
         */
        constructor($errorReporter : ErrorReporter) {
            super();
            this.fixableErrors = new ArrayList<string>();
            this.ignoredErrors = new ArrayList<string>();
            this.extensions = new ArrayList<string>();
            this.ignoredFiles = new ArrayList<string>();
            this.errorReporter = $errorReporter;
            this.properties = new ArrayList<IPropertyEntry>();
        }

        /**
         * @param {any} $configFile Load config from json data.
         */
        public Load($configFile : any) : void {
            if (ObjectValidator.IsEmptyOrNull($configFile)) {
                return;
            }

            this.readPropertyArray("fix", $configFile, this.fixableErrors);
            this.readPropertyArray("ignore", $configFile, this.ignoredErrors);
            this.readPropertyArray("extensions", $configFile, this.extensions);
            this.readPropertyArray("ignoredFiles", $configFile, this.ignoredFiles);

            this.properties.Add(<IPropertyEntry>{propName: "spaces", propValue: this.readProperty<number>("spaces", $configFile)});
            this.properties.Add(<IPropertyEntry>{propName: "lineEnding", propValue: this.readProperty<string>("lineEnding", $configFile)});
        }

        /**
         * @param {string} $fileName Check if $fileName has an extension which is stated in config.
         * @return {boolean} Return true if filename has extension from config, false otherwise.
         */
        public HasValidExtension($fileName : string) : boolean {
            if (StringUtils.IsEmpty($fileName)) {
                return true;
            }

            for (let i = 0; i < this.extensions.Length(); i++) {
                const extension : string = this.extensions.getItem(i);
                if (StringUtils.EndsWith($fileName, extension)) {
                    return true;
                }
            }
            return false;
        }

        /**
         * @param {string} $fileName Check if filename is among ignored files names. If so, do not lint.
         * @return {boolean} Return if $fileName is in ignored files.
         */
        public IsIgnoredFile($fileName : string) : boolean {
            if (StringUtils.IsEmpty($fileName)) {
                return true;
            }

            return this.ignoredFiles.Contains($fileName);
        }

        /**
         * @param {IErrorReporterObject} $errorObject Check if config allows $errorObject to be fixed.
         * @return {boolean} Return true if the error is fixable, false otherwise.
         */
        public ShouldFix($errorObject : IErrorReporterObject) : boolean {
            const camelCaseMessage : string = this.errorReporter.FindCamelCase($errorObject);
            return (!StringUtils.IsEmpty(camelCaseMessage) && this.fixableErrors.Contains(camelCaseMessage));
        }

        /**
         * @param {IErrorReporterObject} $errorObject Check if $errorObject should be ignored.
         * @return {boolean} Return true if ignored, false otherwise.
         */
        public ShouldIgnore($errorObject : IErrorReporterObject) : boolean {
            const camelCaseMessage : string = this.errorReporter.FindCamelCase($errorObject);
            return (!StringUtils.IsEmpty(camelCaseMessage) && this.ignoredErrors.Contains(camelCaseMessage));
        }

        /**
         * @return {boolean} Check if config has at least one error allowed to fix.
         */
        public HasErrorsToFix() : boolean {
            return !this.fixableErrors.IsEmpty();
        }

        /**
         * @param {string} $propname Return value registered under name $propName.
         * @return {any} Return value that was registered in function Load().
         */
        public getPropertyValue($propname : string) : any {
            for (let i = 0; i < this.properties.Length(); i++) {
                if (this.properties.getItem(i).propName === $propname) {
                    return this.properties.getItem(i).propValue;
                }
            }
            return null;
        }

        private readProperty<T>($propName : string, $json : any) : T {
            const prop : T = $json[$propName];

            if (!prop) {
                return null;
            }

            return prop;
        }

        private readPropertyArray($propName : string, $json : any, $target : ArrayList<string>) : void {
            const prop : string[] = this.readProperty<string[]>($propName, $json);

            if (!prop) {
                return;
            }

            for (const elem of prop) {
                $target.Add(StringUtils.ToCamelCase(elem));
            }
        }
    }

    interface IPropertyEntry {
        propName : string;
        propValue : any;
    }
}
