/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_MANAGER_HPP_
#define NEWVISUALPROJECT_MANAGER_HPP_

#include "IManager.hpp"
#include "VisualGraph.hpp"

namespace NewVisualProject {    /**
     * This class implements runtime manager.
     */    class API_EXPORT Manager : public IManager {     public:        int API_CALL Create() override;        int API_CALL Validate() override;        int API_CALL Process(const ProcessOptions &$options) override;        void API_CALL Stop() override;        bool API_CALL IsRunning() override;        void API_CALL setIsHeadless(bool $value) override;        void API_CALL setIsLooped(bool $value) override;     private:        bool isRunning = false;        VisualGraph* visualGraph = nullptr;    };}

#endif  // NEWVISUALPROJECT_MANAGER_HPP_
