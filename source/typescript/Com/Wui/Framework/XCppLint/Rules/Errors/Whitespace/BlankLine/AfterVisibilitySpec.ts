/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.BlankLine {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;

    export class AfterVisibilitySpec extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.BLANK_LINE.AFTER_VISIBILITY_SPEC);
            this.setSensitivity(TokenType.COMMENT, TokenType.BLOCK);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const prevChild : IToken = $processingInfo.PrevChild();
            if (prevChild &&
                (StringUtils.EndsWith(prevChild.body, "public:") ||
                    StringUtils.EndsWith(prevChild.body, "protected:") ||
                    StringUtils.EndsWith(prevChild.body, "private:"))) {
                if (StringUtils.StartsWith($processingInfo.Child().body, "\n\n")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
