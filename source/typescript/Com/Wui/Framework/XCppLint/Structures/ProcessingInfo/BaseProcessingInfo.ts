/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Structures.ProcessingInfo {
    "use strict";

    import Config = Com.Wui.Framework.XCppLint.Core.Config;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class BaseProcessingInfo extends EventArgs {
        private parent : IToken;
        private child : IToken;
        private config : Config;
        private fileName : string;
        private nextToken : IToken;
        private prevChild : IToken;
        private prevLine : string;
        private prevToken : IToken;

        public Parent($parent? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($parent)) {
                this.parent = $parent;
            }
            return this.parent;
        }

        public Child($child? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($child)) {
                this.child = $child;
            }
            return this.child;
        }

        public Config($config? : Config) : Config {
            if (!ObjectValidator.IsEmptyOrNull($config)) {
                this.config = $config;
            }
            return this.config;
        }

        public FileName($fileName? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($fileName)) {
                this.fileName = $fileName;
            }
            return this.fileName;
        }

        public NextToken($value? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.nextToken = $value;
            }
            return this.nextToken;
        }

        public PrevChild($value? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.prevChild = $value;
            }
            return this.prevChild;
        }

        public PrevLine($value? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.prevLine = $value;
            }
            return this.prevLine;
        }

        public PrevToken($value? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.prevToken = $value;
            }
            return this.prevToken;
        }
    }
}
