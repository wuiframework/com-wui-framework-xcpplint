/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Readability.Wui {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import SpecType = Com.Wui.Framework.XCppLint.Enums.SpecType;
    import WuiProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.WuiProcessingInfo;
    import WuiBefore = Com.Wui.Framework.XCppLint.Rules.Before.WuiBefore;
    import ClassMember = Com.Wui.Framework.XCppLint.Structures.ClassMember;

    export class MethodNaming extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.WUI.METHOD_NAMING);
            this.setOnBefore(WuiBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : WuiProcessingInfo = <WuiProcessingInfo>this.getOnBefore().getResults($processingInfo);
            const methods : ArrayList<ClassMember> = pInfo.Members();

            if (!methods || methods.IsEmpty()) {
                return;
            }

            for (const method of methods.getAll()) {
                const methodSpec : SpecType = method.Member().specType;
                const matches : RegExpExecArray =
                    /\s*(\w(?:\w|::|<|>|\s)+)\s((\w|::|\*|\&|operator=|<<|>>)*)(\(.*)/gm.exec(method.Member().body);

                if (matches) {
                    const methodName : string = StringUtils.Split(matches[2], "::").reverse()[0];
                    if (methodSpec === SpecType.PUBLIC) {
                        if (!/^(\*)?[A-Z]/.test(methodName)) {
                            if (!/^[*&]*[gs]et/gm.test(methodName)) {
                                $onError(this.getErrorObject(), method.Member().line, -1);
                            }
                        } else if (/^[*&]*[GS]et/.test(methodName)) {
                            $onError(this.getErrorObject(), method.Member().line, -1);
                        }
                    } else if (methodSpec === SpecType.PROTECTED || methodSpec === SpecType.PRIVATE) {
                        if (!/^(\*)?[a-z]/.test(methodName)) {
                            $onError(this.getErrorObject(), method.Member().line, -1);
                        }
                    }
                }
            }
        }
    }
}
