/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;

    export class MissingBeforeCloseBrace extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MISSING_BEFORE_CLOSE_BRACE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (/^ *}/g.test($processingInfo.Child().body) &&
                this.requiresNewLine($processingInfo.Parent().body) &&
                ($processingInfo.PrevChild() && $processingInfo.PrevChild().type === TokenType.BLOCK)) {
                if (BaseLintRule.isTokenInClass($processingInfo.Child())) {
                    if (BaseLintRule.isFunction($processingInfo.Parent().body) &&
                        ($processingInfo.Parent().children.length === 2 || $processingInfo.Parent().children.length === 1) &&
                        $processingInfo.Parent().line === $processingInfo.Child().line) {
                        return;
                    }
                }

                if (!BaseLintRule.isTokenInClass($processingInfo.Child()) &&
                    BaseLintRule.isFunction($processingInfo.Parent().body) &&
                    $processingInfo.Parent().children.length === 1) {
                    return;
                }

                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            if (!StringUtils.StartsWith($pinfo.Child().body, "\n")) {
                $pinfo.Child().body = "\n" + $pinfo.Child().body;
            }
            return true;
        }
    }
}
