/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Interfaces {
    "use strict";
    import Severity = Com.Wui.Framework.XCppLint.Enums.Severity;
    import Priority = Com.Wui.Framework.XCppLint.Enums.Priority;
    import Config = Com.Wui.Framework.XCppLint.Core.Config;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;

    export interface IBaseLintRule extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {
        Process($processingInfo : BaseProcessingInfo,
                $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void;

        Severity($severity? : Severity) : void;

        Priority($priority? : Priority) : void;

        Message() : string;

        setErrorObject($object : IErrorReporterObject) : void;

        getErrorObject() : IErrorReporterObject;

        ResetBefore() : void;

        IsSensitiveTo($token : IToken) : boolean;

        Fix($pinfo : BaseProcessingInfo) : boolean;

        LoadConfig($config : Config) : void;
    }
}
