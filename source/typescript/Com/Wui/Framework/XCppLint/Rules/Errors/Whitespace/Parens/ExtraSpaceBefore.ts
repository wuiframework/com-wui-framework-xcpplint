/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Parens {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import ParensBefore = Com.Wui.Framework.XCppLint.Rules.Before.ParensBefore;
    import ParensProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.ParensProcessingInfo;

    export class ExtraSpaceBefore extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.PARENS.EXTRA_SPACE_BEFORE);
            this.setOnBefore(ParensBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : ParensProcessingInfo = <ParensProcessingInfo>this.getOnBefore().getResults($processingInfo);

            const fncall = pInfo.Fncall();
            if (pInfo.CommonCondition()) {
                if (/\w\s+\(/g.test(fncall) &&
                    !/_{0,2}asm_{0,2}\s+_{0,2}volatile_{0,2}\s+\(/.test(fncall) &&
                    !/#\s*define|typedef|using\s+\w+\s*=/.test(fncall) &&
                    !/\w\s+\((\w+::)*\*\w+\)\(/.test(fncall) &&
                    !/\bcase\s+\(/.test(fncall)) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
