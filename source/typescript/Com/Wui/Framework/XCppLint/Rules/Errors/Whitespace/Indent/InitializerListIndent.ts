/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Indent {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Config = Com.Wui.Framework.XCppLint.Core.Config;

    export class InitializerListIndent extends BaseLintRule {

        private expectedIndentSize : number;

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.INDENT.INITIALIZER_LIST_INDENT);
            this.expectedIndentSize = 4;
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const nesting : number = $processingInfo.Child().nesting;

            if (BaseLintRule.isCtor($processingInfo.Child().body)) {
                if (/\)\s*:\s*/m.test($processingInfo.Child().body)) {  // check if ctor contains initializer list
                    const re : RegExp = new RegExp(
                        "(\\r\\n|\\n) {" + ((this.expectedIndentSize * nesting) + (2 * this.expectedIndentSize)) + "}: ",
                        "m");
                    if (!re.test($processingInfo.Child().body)) {    // check if colon is correctly formatted
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const firstEndBrace : number = StringUtils.IndexOf($pinfo.Child().body, ")");
            const originalInitList : string = StringUtils.Substring($pinfo.Child().body, firstEndBrace + 1);
            const elems : string[] = StringUtils.Split(originalInitList, ",");

            const nesting : number = $pinfo.Child().nesting;

            for (let i = 0; i < elems.length; i++) {
                elems[i] = elems[i].trim();
                if (StringUtils.StartsWith(elems[i], ":")) {
                    elems[i] = "\n"
                        + this.generateSpaces((this.expectedIndentSize * nesting) + (2 * this.expectedIndentSize))
                        + elems[i];
                } else {
                    elems[i] = "\n"
                        + this.generateSpaces((this.expectedIndentSize * nesting) + (2 * this.expectedIndentSize) + 2)
                        + elems[i];
                }
            }

            const fixedList : string = elems.join();

            $pinfo.Child().body = StringUtils.Replace($pinfo.Child().body, originalInitList, fixedList);

            return true;
        }

        public LoadConfig($config : Config) : void {
            const temp : number = (<number>$config.getPropertyValue("spaces"));
            if (temp) {
                this.expectedIndentSize = temp;
            }
        }
    }
}
