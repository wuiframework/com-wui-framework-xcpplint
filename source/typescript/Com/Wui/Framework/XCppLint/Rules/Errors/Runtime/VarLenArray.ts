/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Runtime {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class VarLenArray extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.RUNTIME.VAR_LEN_ARRAY);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const match : RegExpExecArray = /\s*(.+::)?(\w+) [a-z]\w*\[(.+)];/gm.exec($processingInfo.Child().body);
            if (match &&
                match[2] !== "return" &&
                match[2] !== "delete" &&
                !StringUtils.Contains(match[3], "]")) {
                const tokens = StringUtils.Split(match[3],
                    " ", "+", "-", "*", "/", "<<", ">>");
                let isConst = true;
                let skipNext = false;

                for (let tok of tokens) {
                    if (skipNext) {
                        skipNext = false;
                        continue;
                    }

                    if (/sizeof\(.+\)/gm.test(tok)) {
                        continue;
                    }

                    if (/arraysize\(\w+\)/gm.test(tok)) {
                        continue;
                    }

                    tok = this.lstrip(tok, "(");
                    tok = this.rstrip(tok, ")");

                    if (!tok) {
                        continue;
                    }

                    if (/\d+/gm.test(tok) ||
                        /0[xX][0-9a-fA-F]+/gm.test(tok) ||
                        /k[A-Z0-9]\w*/gm.test(tok) ||
                        /(.+::)?k[A-Z0-9]\w*/gm.test(tok) ||
                        /(.+::)?[A-Z][A-Z0-9_]*/gm.test(tok)) {
                        continue;
                    }

                    if (StringUtils.StartsWith(tok, "sizeof")) {
                        skipNext = true;
                        continue;
                    }
                    isConst = false;
                    break;
                }
                if (!isConst) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }

        private lstrip($data : string, $charToTrim : string) : string {
            const tmp : string = $data.trim();

            if (StringUtils.Length(tmp) === 0) {
                return $data;
            }

            if (tmp[0] === $charToTrim) {
                return StringUtils.Substring(tmp, 1);
            }

            return $data;
        }

        private rstrip($data : string, $charToTrim : string) : string {
            const tmp : string = $data.trim();

            if (StringUtils.Length(tmp) === 0) {
                return $data;
            }

            if (tmp[StringUtils.Length(tmp) - 1] === $charToTrim) {
                return StringUtils.Substring(tmp, 0, StringUtils.Length(tmp) - 2);
            }

            return $data;
        }
    }
}
