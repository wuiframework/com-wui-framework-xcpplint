/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Before {
    "use strict";
    import BaseBefore = Com.Wui.Framework.XCppLint.Structures.BaseBefore;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ExplicitProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.ExplicitProcessingInfo;

    export class ExplicitBefore extends BaseBefore {
        private static instance : ExplicitBefore;
        private processingInfo : ExplicitProcessingInfo;

        public static getInstance() : BaseBefore {
            if (!ExplicitBefore.instance) {
                ExplicitBefore.instance = new ExplicitBefore();
            }
            return ExplicitBefore.instance;
        }

        constructor() {
            super();
            this.processingInfo = new ExplicitProcessingInfo();
        }

        public Reset() : void {
            super.Reset();
            this.processingInfo.Clear();
        }

        public getResults($processingInfo : BaseProcessingInfo) : ExplicitProcessingInfo {
            this.run($processingInfo);
            return this.processingInfo;
        }

        private run($processingInfo : BaseProcessingInfo) : void {
            if (!this.Done()) {
                if (!StringUtils.StartsWith($processingInfo.Parent().body.trim(), "class")) {
                    return;
                }

                const classNameMatch = /(\w*)(?:\s*{)/.exec($processingInfo.Parent().body);
                const className = classNameMatch[1];

                const re : RegExp = new RegExp("\\s+(?:(?:inline|constexpr)\\s+)*(explicit\\s+)?(?:(?:inline|constexpr)\\s+)*" +
                    className +
                    "\\s*\\(((?:[^()]|\\([^()]*\\))*)\\)", "gm");
                const explicitCtorMatch : RegExpExecArray = re.exec($processingInfo.Child().body);

                if (!explicitCtorMatch) {
                    return;
                }

                const isMarkedExplicit = explicitCtorMatch[1];
                let ctorArgs = [];
                if (explicitCtorMatch[2]) {
                    ctorArgs = StringUtils.Split(explicitCtorMatch[2], ",");
                }

                let i = 0;
                while (i < ctorArgs.length) {
                    let ctorArg : string = ctorArgs[i];
                    while (StringUtils.OccurrenceCount(ctorArg, "<") > StringUtils.OccurrenceCount(ctorArg, ">") ||
                    StringUtils.OccurrenceCount(ctorArg, "(") > StringUtils.OccurrenceCount(ctorArg, ")")) {
                        ctorArg += "," + ctorArgs[i + 1];
                        ctorArgs = ctorArgs.splice(i + 1, 1);
                    }
                    ctorArgs[i] = ctorArg;
                    i += 1;
                }

                const defaultedArgs : string[] = ctorArgs.filter(($arg : string) => {
                    return StringUtils.Contains($arg, "=");
                });

                const noargCtor : boolean = (ctorArgs.length === 0 ||
                    (ctorArgs.length === 1 && ctorArgs[0].trim() === "void"));

                const oneargCtor : boolean = ((ctorArgs.length === 1 && !noargCtor) ||
                    (ctorArgs.length >= 1 && !noargCtor && defaultedArgs.length >= ctorArgs.length));

                const initializerListCtor : boolean = (oneargCtor &&
                    /\bstd\s*::\s*initializer_list\b/gm.test(ctorArgs[0]));

                const copyCtor : boolean = (oneargCtor &&
                    (new RegExp("(const\\s+)?" +
                        className +
                        "(\\s*<[^>]*>)?(\\s+const)?\\s*(?:<\\w+>\\s*)?&")).test(ctorArgs[0].trim()));

                this.processingInfo.CopyCtor(copyCtor);
                this.processingInfo.InitializerListCtor(initializerListCtor);
                this.processingInfo.IsMarkedExplicit(isMarkedExplicit);
                this.processingInfo.NoArgCtor(noargCtor);
                this.processingInfo.OneArgCtor(oneargCtor);
                this.processingInfo.DefaultedArgs(defaultedArgs);

                this.Done(true);
            }
        }
    }
}
