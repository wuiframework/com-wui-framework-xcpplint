/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_BASEGRAPH_HPP_
#define NEWVISUALPROJECT_BASEGRAPH_HPP_

#include <string>
#include <map>

#include <VX/vx.h>

#include "Reference.hpp"

namespace NewVisualProject {
    /**
     * BaseGraph class holds all generalised operations on OpenVX graph.
     */
    class BaseGraph {
     public:
        static vx_uint32 getImageHeight(vx_reference $image);

        static vx_uint32 getImageWidth(vx_reference $image);

        static vx_df_image_e getImageType(vx_reference $image);

        /**
         * Constructs BaseGraph from parent Context.
         * @param context Specify parent context pointer.
         */
        explicit BaseGraph(BaseContext *context);

        /**
         * Destructs BaseGraph object, parent context will NOT be affected.
         */
        ~BaseGraph();

        /**
         * @return Returns pointer to parent context.
         */
        const BaseContext *getParent() const;

        /**
         * @return Returns reference to internal vx_graph instance.
         */
        vx_graph getVxGraph() const;

        /**
         * Generic getter for data instances owned by this graph.
         * @param name Specify name of data to get.
         * @return Returns reference to requested data or nullptr if not found.
         */
        vx_reference getData(const std::string &name) const;

        /**
         * Creates internal nodes and data instances.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status create() = 0;

        /**
         * Process graph validation.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status validate() = 0;

        /**
         * Run previously prepared graph.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status process() const = 0;

     protected:
        std::map<std::string, Io::VisionSDK::Studio::Libs::VXUtils::Primitives::VxReferenceInfo> vxDataMap = {};
        std::map<std::string, Io::VisionSDK::Studio::Libs::VXUtils::Primitives::VxReferenceInfo> vxNodesMap = {};

        vx_reference createImage(vx_context $context, vx_uint32 $width, vx_uint32 $height, vx_df_image $color);

        virtual bool loopCondition(int $loopCnt) const;

     private:
        BaseContext *parent;
        vx_graph vxGraph;
    };
}

#endif  // NEWVISUALPROJECT_BASEGRAPH_HPP_
