/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Com.Wui.Framework.XCppLint.Interfaces {
    "use strict";
    export let IErrorReporterObject : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "message",
                "severity",
                "priority"
            ]);
        }();
}

namespace Com.Wui.Framework.XCppLint.Interfaces {
    "use strict";
    export let IToken : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "body",
                "childIndex",
                "children",
                "type",
                "line",
                "nesting",
                "originalBody",
                "specType",
                "parent"
            ]);
        }();
}

namespace Com.Wui.Framework.XCppLint.Interfaces {
    "use strict";
    export let IBaseLintRule : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Process",
                "Severity",
                "Priority",
                "Message",
                "setErrorObject",
                "getErrorObject",
                "ResetBefore",
                "IsSensitiveTo",
                "Fix",
                "LoadConfig"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

/* tslint:enable */
