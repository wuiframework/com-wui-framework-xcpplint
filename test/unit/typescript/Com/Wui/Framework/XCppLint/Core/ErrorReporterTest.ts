/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import Priority = Com.Wui.Framework.XCppLint.Enums.Priority;
    import Severity = Com.Wui.Framework.XCppLint.Enums.Severity;

    export class ErrorReporterTest extends UnitTestRunner {
        private errorReporter : ErrorReporter;

        constructor() {
            super();
            this.errorReporter = new ErrorReporter();
        }

        public testBasicReport() : void {
            this.errorReporter.ReportError(ERRORS.OTHER.LINE_LEN, 2);

            const hasError : boolean = this.isErrorInArray({
                line   : 2,
                message: ERRORS.OTHER.LINE_LEN
            }, this.errorReporter.getAllErrors());

            assert.equal(hasError, true);

            this.initSendBox();
        }

        public testTurnLintOffOneLine() : void {
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 20);

            let hasError : boolean = this.isErrorInArray({
                line   : 20,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            this.errorReporter.AddIgnoreErrorLine("ERRORS.READABILITY.CASTING.C_STYLE",
                21,
                false);
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 21);
            hasError = this.isErrorInArray({
                line   : 21,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 22);
            hasError = this.isErrorInArray({
                line   : 22,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            this.initSendBox();
        }

        public testTurnLintOffOneLineSyntax() : void {
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 20);

            let hasError : boolean = this.isErrorInArray({
                line   : 20,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            let lineNum = 32;
            this.errorReporter.AddIgnoreErrorLine("ERRORS.READABILITY.CASTING.C_STYLE",
                lineNum,
                false);
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, lineNum);
            hasError = this.isErrorInArray({
                line   : lineNum,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            lineNum = 22;
            this.errorReporter.AddIgnoreErrorLine("ERRORS-READABILITY-CASTING-C_STYLE",
                lineNum,
                false);
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, lineNum);
            hasError = this.isErrorInArray({
                line   : lineNum,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            lineNum = 23;
            this.errorReporter.AddIgnoreErrorLine("errors-READABILITY-CASTING-C_style",
                lineNum,
                false);
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, lineNum);
            hasError = this.isErrorInArray({
                line   : lineNum,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            lineNum = 24;
            this.errorReporter.AddIgnoreErrorLine("readability-CASTING-C_style",
                lineNum,
                false);
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, lineNum);
            hasError = this.isErrorInArray({
                line   : lineNum,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            this.initSendBox();
        }

        public testTurnLintOffGlobal() : void {
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 20);

            let hasError : boolean = this.isErrorInArray({
                line   : 20,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            this.errorReporter.AddIgnoreErrorLine("ERRORS.READABILITY.CASTING.C_STYLE",
                21,
                true);

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 22);
            hasError = this.isErrorInArray({
                line   : 22,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 19);
            hasError = this.isErrorInArray({
                line   : 19,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            this.initSendBox();
        }

        public testTurnLintOffGlobalUseCase() : void {
            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 20);

            let hasError : boolean = this.isErrorInArray({
                line   : 20,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            this.errorReporter.AddIgnoreErrorLine("ERRORS.RUNTIME.NON_CONST_REF",
                19,
                true);
            this.errorReporter.AddIgnoreErrorLine("ERRORS.RUNTIME.INIT_ITSELF",
                19,
                true);
            this.errorReporter.AddIgnoreErrorLine("ERRORS.READABILITY.CASTING.C_STYLE",
                21,
                true);

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 22);
            hasError = this.isErrorInArray({
                line   : 22,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 19);
            hasError = this.isErrorInArray({
                line   : 19,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            this.errorReporter.ReportError(ERRORS.RUNTIME.INIT_ITSELF, 19);
            hasError = this.isErrorInArray({
                line   : 19,
                message: ERRORS.RUNTIME.INIT_ITSELF
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            this.errorReporter.CleanAllGlobalLines();

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 22);
            hasError = this.isErrorInArray({
                line   : 22,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            this.initSendBox();
        }

        public testTurnLintOffGlobalUseCaseAlreadyDisabled() : void {

            let hasError : boolean;

            this.errorReporter.AddIgnoreErrorLine("ERRORS.RUNTIME.NON_CONST_REF",
                19,
                true);
            this.errorReporter.AddIgnoreErrorLine("ERRORS.RUNTIME.INIT_ITSELF",
                19,
                true);
            this.errorReporter.AddIgnoreErrorLine("ERRORS.READABILITY.CASTING.C_STYLE",
                21,
                true);

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 22);
            hasError = this.isErrorInArray({
                line   : 22,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 19);
            hasError = this.isErrorInArray({
                line   : 19,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, true);

            this.errorReporter.AddIgnoreErrorLine("ERRORS.READABILITY.CASTING.C_STYLE",
                22);

            this.errorReporter.ReportError(ERRORS.READABILITY.CASTING.C_STYLE, 22);
            hasError = this.isErrorInArray({
                line   : 22,
                message: ERRORS.READABILITY.CASTING.C_STYLE
            }, this.errorReporter.getAllErrors());
            assert.equal(hasError, false);

            this.initSendBox();
        }

        public testInvalidMessage() : void {
            const invalidMsg = "some invalid error message";
            this.errorReporter.ReportError(<IErrorReporterObject>{
                message : invalidMsg,
                priority: Priority.MEDIUM,
                severity: Severity.UNKNOWN
            }, 2);

            const hasError : boolean = this.isErrorInArray({
                line   : 2,
                message: invalidMsg
            }, this.errorReporter.getAllErrors());

            assert.equal(hasError, true);

            this.initSendBox();
        }

        protected setUp() : void | IUnitTestRunnerPromise {
            this.errorReporter.ClearErrors();
            this.errorReporter.CleanAllLines();
            return;
        }
    }
}
