/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;
    import ERROR_MESSAGES = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class CodeBlockParserTest extends UnitTestRunner {
        private errorReporter : ErrorReporter;

        constructor() {
            super();
            this.errorReporter = new ErrorReporter();
        }

        public testErrors() : void {
            let data;
            let errors : ArrayList<LintError>;
            const codeParser : CodeBlocksParser = new CodeBlocksParser();

            data = "a())\n";
            codeParser.Parse(data);
            errors = codeParser.getAllErrors();
            const hasError : boolean = this.isErrorInArray(
                new LintError(0, ERROR_MESSAGES.OTHER.INVALID_BRACKETS), errors);
            assert.deepEqual(hasError, true);

            this.initSendBox();
        }

        public testParsing() : void {
            let data;
            let errors : ArrayList<LintError>;
            let map : IToken;
            const codeParser : CodeBlocksParser = new CodeBlocksParser();

            data = "{}\n";
            map = codeParser.Parse(data);
            errors = codeParser.getAllErrors();
            assert.deepEqual(errors.Length(), 0);
            this.compareJsonsByChildBody(map, "test/resource/data/Com/Wui/Framework/XCppLint/Core/CodeBlockParser/test_1.json");

            data = "int main() {\n" +
                "    int a = 0;\n" +
                "    int b = a;\n" +
                "    return b;\n" +
                "}\n";
            map = codeParser.Parse(data);
            errors = codeParser.getAllErrors();
            assert.deepEqual(errors.Length(), 0);
            this.compareJsonsByChildBody(map, "test/resource/data/Com/Wui/Framework/XCppLint/Core/CodeBlockParser/test_2.json");

            data = "int foo(void) {\n" +
                "    for (int i = 0; i < 10; i++) {\n" +
                "        char c = i;\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "int main() {\n" +
                "    int a = 0;\n" +
                "    int b = a;\n" +
                "    return b;\n" +
                "}\n";
            map = codeParser.Parse(data);
            errors = codeParser.getAllErrors();
            assert.deepEqual(errors.Length(), 0);
            this.compareJsonsByChildBody(map, "test/resource/data/Com/Wui/Framework/XCppLint/Core/CodeBlockParser/test_3.json");

            data = "class C {\n" +
                " public:\n" +
                "    C() {}\n" +
                " private:\n" +
                "    int func(int $i, int $j, int $k);\n" +
                "};\n" +
                "\n" +
                "int foo(void) {\n" +
                "    C c();\n" +
                "}\n" +
                "\n" +
                "int main() {\n" +
                "    int a = 0;\n" +
                "    int b = a;\n" +
                "    foo();\n" +
                "    return b;\n" +
                "}\n";
            map = codeParser.Parse(data);
            errors = codeParser.getAllErrors();
            assert.deepEqual(errors.Length(), 0);
            this.compareJsonsByChildBody(map, "test/resource/data/Com/Wui/Framework/XCppLint/Core/CodeBlockParser/test_4.json");

            data = "class C {\n" +
                " public:\n" +
                "    C() {}\n" +
                "\n" +
                "    bool get() {\n" +
                "        return this.val;\n" +
                "    }\n" +
                "\n" +
                " private:\n" +
                "    int func(int $i, int $j, int $k);\n" +
                "\n" +
                "    bool val = false;\n" +
                "};\n" +
                "\n" +
                "int foo(void) {\n" +
                "    C c();\n" +
                "    if (c.get()) {\n" +
                "        if (foo()) {\n" +
                "            while (true) {\n" +
                "                c.get();\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "int main() {\n" +
                "    int a = 0;\n" +
                "    int b = a;\n" +
                "    foo();\n" +
                "    return b;\n" +
                "}\n";
            map = codeParser.Parse(data);
            errors = codeParser.getAllErrors();
            assert.deepEqual(errors.Length(), 0);
            this.compareJsonsByChildBody(map, "test/resource/data/Com/Wui/Framework/XCppLint/Core/CodeBlockParser/test_5.json");
        }

        protected setUp() {
            this.errorReporter.ClearErrors();
            this.errorReporter.CleanAllLines();
        }
    }
}
