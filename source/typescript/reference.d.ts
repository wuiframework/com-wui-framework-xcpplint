/* tslint:disable:no-reference */
/// <reference path="../../dependencies/com-wui-framework-commons/source/typescript/reference.d.ts" />

// generated-code-start
/// <reference path="Com/Wui/Framework/XCppLint/Interfaces/IBaseLintRule.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Interfaces/IErrorReporterObject.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Interfaces/IToken.ts" />
/// <reference path="interfacesMap.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/HttpProcessor/HttpResolver.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Enums/Severity.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Enums/TokenType.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Enums/Priority.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Enums/FixStatus.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Enums/CharType.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Enums/SpecType.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Enums/CommentType.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Enums/DirectiveType.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Index.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Loader.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ClassMember.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ReplacementPair.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/Model/TokenInfo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/Model/Comment.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/Model/Directive.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/Config.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/RuntimeTests/XCppLintTest.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/Model/LintError.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/Utils.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/DirectivesParser.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/CommentsParser.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ProcessingInfo/BaseProcessingInfo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ProcessingInfo/ExplicitProcessingInfo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ProcessingInfo/ParensProcessingInfo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ProcessingInfo/TemplatedVarsProcessingInfo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ProcessingInfo/TemplateProcessingInfo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ProcessingInfo/StringProcessingInfo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/ProcessingInfo/WuiProcessingInfo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Structures/BaseBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/ErrorReporter.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/Linter.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Before/ExplicitBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Before/ParensBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Before/TemplateBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Before/TemplatedVarsBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Before/StringBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/Model/BaseLintRule.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/RuntimeTests/LinterTest.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Build/EndIfComment.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Build/ExplicitMakePair.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Build/InnerFwdDecl.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Build/MaxMinDeprecated.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Build/StorageClass.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Other/LineLen.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Other/TabFound.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Braces/ElseBraceOnBothSides.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Braces/IfElseReqBraces.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Braces/NewLineIf.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Braces/RedundantSemicolon.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Casting/CStyle.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Casting/DeprecatedStyle.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Casting/DereferencedCast.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Inheritance/OverrideRedundant.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Inheritance/VirtRedundant.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Wui/NullUsed.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Wui/StructAsParam.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Wui/VisibilitySpecComment.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Int/Ports.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Int/UseInt64.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Printf/SizeofInstead.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Printf/Snprintf.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Printf/Sprintf.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/ConstStringRef.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/InitItself.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/InvalidIncrement.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Memset.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/OperatorAmpersand.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/ThreadsafeFn.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/VarLenArray.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/BlankLine/AfterVisibilitySpec.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/BlankLine/AtEnd.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/BlankLine/AtStart.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/BlankLine/Preceded.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Braces/MissingBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Braces/BraceAtPrevLine.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Braces/ExtrSpaceBeforeBrace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Braces/MissingBeforeCatch.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Braces/MissingBeforeElse.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Braces/MissingBeforeWhile.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Indent/InitializerListIndent.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Indent/InvalidIndent.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Indent/LineEndsWhitespace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/CatchNewline.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/ElseOnSameLine.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingAfterCloseBrace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingAfterComment.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingAfterOpenBrace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingAfterTemplate.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingAfterVisibilitySpec.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingBeforeCloseBrace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingBeforeDo.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Operators/InvalidBinaryOperator.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Operators/InvalidComparison.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Operators/InvalidUnary.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Operators/MissingSpaceEqual.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Parens/InvalidSpacing.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Semicolon/DefiningEmptyStatement.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Semicolon/ExtraSpaceBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Semicolon/InvalidSpacing.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Semicolon/Only.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/EmptyBody/For.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/ColonInvalidSpacing.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Commas/CommaInFunction.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Commas/CommaInvalidSpacing.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/CppCasting.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/EmptyBody/Block.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/EmptyBody/If.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/EmptyBody/While.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/ForColonMissing.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Pointers.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/LegalCopyright.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Build/Namespaces/UnnamedNamespace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Build/Namespaces/UsingNamespace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/CodeBlocksParser.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Explicit/CallableOneArgCtor.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Explicit/SingleParamCtor.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/Explicit/ZeroParamCtor.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/String/NotPermitted.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/String/UseCStyleString.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Runtime/NonConstRef.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Parens/ExtraSpaceAfter.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Parens/ExtraSpaceBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Parens/ShouldBeMovedUp.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Template/SpaceAfterOpenBrace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Template/SpaceAfterTemplate.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Template/SpaceBeforeCloseBrace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Wui/TemplatedVars/InnerSpaces.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Wui/TemplatedVars/SpaceAtEnd.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Wui/TemplatedVars/SpaceAtStart.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Core/TokenMapLinter.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingBeforeDecDef.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Before/WuiBefore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Wui/ClassOrder.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Wui/HasUnderscore.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Wui/MethodNaming.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Wui/MethodOrder.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Readability/Wui/MissingDollar.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MissingBeforeComment.ts" />
// generated-code-end

/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Indent/LineStart.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/DoWhileAfterBrace.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/DoWhileSingleLine.ts" />
/// <reference path="Com/Wui/Framework/XCppLint/Rules/Errors/Whitespace/Newline/MultipleCommands.ts" />
