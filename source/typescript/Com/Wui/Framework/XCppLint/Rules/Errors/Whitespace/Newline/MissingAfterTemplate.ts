/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class MissingAfterTemplate extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MISSING_AFTER_TEMPLATE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const matches : RegExpExecArray = this.parseTemplate($processingInfo.Child().body);
            if (!matches) {
                return;
            }

            if (!StringUtils.StartsWith(matches[2], "\n")) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const matches : RegExpExecArray = this.parseTemplate($pinfo.Child().body);
            $pinfo.Child().body = matches[1] + "\n" + this.generateSpaces($pinfo.Child().nesting * 4) + matches[2].trim();
            return true;
        }

        private parseTemplate($body : string) : RegExpExecArray {
            return /(template\s*<\s*[\w\s,]*\s*>)(\s*.*)/.exec($body);
        }
    }
}
