/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import CommentType = Com.Wui.Framework.XCppLint.Enums.CommentType;
    import Comment = Com.Wui.Framework.XCppLint.Core.Structures.Comment;

    export class CommentsParser extends BaseObject {

        /**
         * @param {string} $data Input string on which comments are detected.
         * @return {ArrayList<Comment>} Returns array of comments.
         */
        public Parse($data : string) : ArrayList<Comment> {
            const output : ArrayList<Comment> = new ArrayList<Comment>();
            const dataLength : number = StringUtils.Length($data);
            let charIndex : number = 0;
            let start : number;
            let lastSpaceIndex : number;
            let startLineIndex : number;
            let isString : boolean;
            let stringCharacter : string;
            let isMultiLine : boolean;
            let isInComment : boolean;

            const resetCounters : any = () : void => {
                start = -1;
                lastSpaceIndex = -1;
                startLineIndex = -1;
                isString = false;
                stringCharacter = "";
                isMultiLine = false;
                isInComment = false;
            };
            resetCounters();

            while (charIndex < dataLength) {
                if (!isInComment) {
                    if (!isString && ($data[charIndex] === "\"" || $data[charIndex] === "'")) {
                        stringCharacter = $data[charIndex];
                    }
                    if ($data[charIndex] === stringCharacter && $data[charIndex + 1] !== "\\") {
                        isString = !isString;
                    }

                    if (!isString) {
                        if (!isMultiLine) {
                            if ($data[charIndex] === "/" && $data[charIndex + 1] === "*") {
                                start = charIndex;
                                isMultiLine = true;
                            }
                            if ($data[charIndex] === "/" && $data[charIndex + 1] === "/") {
                                start = charIndex;
                                isInComment = true;
                            }

                            if (!isMultiLine && !isInComment) {
                                if ($data[charIndex] === "\n") {
                                    lastSpaceIndex = -1;
                                    startLineIndex = charIndex + 1;
                                }
                                if (charIndex > 0 && $data[charIndex - 1] !== " " && $data[charIndex] === " ") {
                                    lastSpaceIndex = charIndex;
                                }
                            }
                        }
                    }
                }

                if (charIndex > 0 && $data[charIndex - 1] === "*" && $data[charIndex] === "/" && isMultiLine) {
                    const comment : Comment = new Comment();
                    if (lastSpaceIndex !== -1) {
                        start = lastSpaceIndex;
                    }
                    let end : number = charIndex + 1;
                    if (startLineIndex === -1 || startLineIndex === start) {
                        if ($data[charIndex + 1] === "\r" && $data[charIndex + 2] === "\n") {
                            end += 2;
                            if ($data[charIndex + 3] === "\r" && $data[charIndex + 4] === "\n") {
                                end += 2;
                            } else if ($data[charIndex + 3] === "\n") {
                                end += 1;
                            }
                        } else if ($data[charIndex + 1] === "\n") {
                            end += 1;
                            if ($data[charIndex + 2] === "\r" && $data[charIndex + 3] === "\n") {
                                end += 2;
                            } else if ($data[charIndex + 2] === "\n") {
                                end += 1;
                            }
                        }
                    }
                    comment.StartIndex(start);
                    comment.EndIndex(end);
                    comment.Type(CommentType.MULTILINE);
                    comment.Contents(StringUtils.Substring($data, comment.StartIndex(), comment.EndIndex()));
                    output.Add(comment);
                    resetCounters();
                } else if ($data[charIndex] === "\n" && isInComment) {
                    const comment : Comment = new Comment();
                    if (lastSpaceIndex !== -1) {
                        start = lastSpaceIndex;
                    }
                    let end : number = charIndex + 1;
                    if (startLineIndex !== -1 && startLineIndex !== start) {
                        end = charIndex;
                        if (charIndex > 0 && $data[charIndex - 1] === "\r") {
                            end--;
                        }
                    }
                    comment.StartIndex(start);
                    comment.EndIndex(end);
                    comment.Type(CommentType.ONELINE);
                    comment.Contents(StringUtils.Substring($data, comment.StartIndex(), comment.EndIndex()));
                    output.Add(comment);
                    resetCounters();
                }
                charIndex++;
            }
            return output;
        }
    }
}
