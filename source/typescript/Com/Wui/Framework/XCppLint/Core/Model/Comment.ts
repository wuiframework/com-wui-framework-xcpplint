/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core.Structures {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import CommentType = Com.Wui.Framework.XCppLint.Enums.CommentType;

    export class Comment extends BaseObject {
        private startIndex : number;
        private endIndex : number;
        private contents : string;
        private type : CommentType;

        /**
         * @param {number} $index If set, set $index to this.startIndex.
         * @return {number} Return starting position of comment.
         */
        public StartIndex($index? : number) : number {
            if (!ObjectValidator.IsEmptyOrNull($index)) {
                this.startIndex = $index;
            }
            return this.startIndex;
        }

        /**
         * @param {number} $index If set, set $index to this.endIndex.
         * @return {number} Return end position of comment.
         */
        public EndIndex($index? : number) : number {
            if (!ObjectValidator.IsEmptyOrNull($index)) {
                this.endIndex = $index;
            }
            return this.endIndex;
        }

        /**
         * @param {string} $text If set, set $text to this.contents.
         * @return {string} Return contents of a comment.
         */
        public Contents($text? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($text)) {
                this.contents = $text;
            }
            return this.contents;
        }

        /**
         * @param {CommentType} $type If set, set $type to this.type.
         * @return {CommentType} Return type of comment.
         */
        public Type($type? : CommentType) : CommentType {
            if (!ObjectValidator.IsEmptyOrNull($type)) {
                this.type = $type;
            }
            return this.type;
        }
    }
}
