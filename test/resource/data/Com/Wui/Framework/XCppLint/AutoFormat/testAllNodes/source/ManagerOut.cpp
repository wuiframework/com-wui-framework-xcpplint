/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "Manager.hpp"
#include "VisualGraph.hpp"
#include <VxUtils.hpp>

using Io::VisionSDK::Studio::Libs::VXUtils::Utils::StopWatch;
using Io::VisionSDK::Studio::Libs::VXUtils::Utils::VxTranslator;

namespace NewVisualProject {
    int API_CALL Manager::Create() {
        this->visualGraph = new VisualGraph();
        return this->visualGraph->create();
    }

    int API_CALL Manager::Validate() {
        return this->visualGraph->validate();
    }

    int API_CALL Manager::Process(const ProcessOptions &$options) {
        this->isRunning = true;
        StopWatch stopWatch{};
        this->visualGraph->setHeadless($options.headless);
        this->visualGraph->setIsLooped($options.looped);
        std::string name;
        stopWatch.Start();
        int status = this->visualGraph->process();
        stopWatch.Stop();
        std::cout << "Application finished, spend: " << stopWatch.getElapsed() << " ms" << std::endl;
        if (!$options.headless) {
            if (status == VX_SUCCESS) {
                std::cout << "Run successfully ..." << std::endl;
            } else {
                std::cout << "Run has failed with status: " << VxTranslator::Translate(status) << std::endl;
            }
            std::cout << "Press any key to finish ...";
            std::cin.get();
        }
        this->isRunning = false;
        return status;
    }

    bool API_CALL Manager::IsRunning() {
        return this->isRunning;
    }

    void API_CALL Manager::Stop() {
        this->visualGraph->setIsLooped(false);
        delete this->visualGraph;
    }

    void API_CALL Manager::setIsHeadless(bool $value) {
        if (this->visualGraph != nullptr) {
            this->visualGraph->setHeadless($value);
        }
    }

    void API_CALL Manager::setIsLooped(bool $value) {
        if (this->visualGraph != nullptr) {
            this->visualGraph->setIsLooped($value);
        }
    }
}

extern "C" {
// interface should be published as extern "C" function to produce undecorated link symbols

API_EXPORT NewVisualProject::Manager *API_CALL getInstance() {
    static NewVisualProject::Manager manager;
    return &manager;
}

API_EXPORT void API_CALL setIsHeadless(bool $value) {
    getInstance()->setIsHeadless($value);
}

API_EXPORT void API_CALL setIsLooped(bool $value) {
    getInstance()->setIsLooped($value);
}

API_EXPORT int API_CALL Create() {
    return getInstance()->Create();
}

API_EXPORT int API_CALL Validate() {
    return getInstance()->Validate();
}

API_EXPORT int API_CALL Process(const NewVisualProject::ProcessOptions &$options) {
    return getInstance()->Process($options);
}

API_EXPORT void API_CALL StopGraph() {
    getInstance()->Stop();
}

API_EXPORT bool API_CALL IsGraphRunning() {
    return getInstance()->IsRunning();
}
}
