/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Readability.Braces {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class IfElseReqBraces extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.BRACES.IF_ELSE_REQ_BRACES);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (((StringUtils.StartsWith($processingInfo.Child().body.trim(), "if (") ||
                StringUtils.StartsWith($processingInfo.Child().body.trim(), "else")) &&
                !StringUtils.EndsWith($processingInfo.Child().body.trim(), " {")) ||
                (StringUtils.OccurrenceCount($processingInfo.Child().body, "if (") > 1)) {
                if (!StringUtils.EndsWith($processingInfo.Child().body.trim(), ">{")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
