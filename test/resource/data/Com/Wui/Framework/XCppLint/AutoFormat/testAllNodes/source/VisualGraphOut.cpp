/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "BaseContext.hpp"
#include "BaseGraph.hpp"
#include "Context0.hpp"
#include "VisualGraph.hpp"

namespace NewVisualProject {
    namespace VXUtils = Io::VisionSDK::Studio::Libs::VXUtils;

    vx_status VisualGraph::create() {
        vx_status status;
        this->contextsMap["context0"] = new Context0(this);
        status = this->contextsMap["context0"]->create();
        if (status != VX_SUCCESS) {
            delete this->contextsMap["context0"];
            return status;
        }
        return status;
    }

    vx_status VisualGraph::validate() {
        vx_status status = VX_SUCCESS;
        if (status == VX_SUCCESS) {
            status = this->contextsMap["context0"]->validate();
        }
        return status;
    }

    vx_status VisualGraph::process() {
        vx_status status = VX_FAILURE;
        BaseContext *context0 = const_cast<BaseContext *>(this->getContext("context0"));
        if (context0 != nullptr) {
            status = VX_SUCCESS;
        }
        BaseGraph *graph_0_0 = nullptr;
        if (status == VX_SUCCESS) {
            graph_0_0 = const_cast<BaseGraph *>(context0->getGraph("graph0"));
            if (graph_0_0 != nullptr) {
                status = VX_SUCCESS;
            } else {
                status = VX_FAILURE;
            }
        }
        if (status == VX_SUCCESS) {
            this->ioComMap.emplace("ioCom0", std::make_shared<VXUtils::Media::Image>("../../target/resource/data/VisionSDK.png", context0->getVxContext()));
        }
        if (status == VX_SUCCESS) {
            this->ioComMap.emplace("ioCom1", std::make_shared<VXUtils::Media::Image>("../data/output/ioCom1.png", context0->getVxContext()));
        }
        if (status == VX_SUCCESS) {
            auto ioCom2 = std::make_shared<VXUtils::Media::Camera>(context0->getVxContext());
            this->ioComMap.emplace("ioCom2", ioCom2);
            ioCom2->Open(0);
            auto vxData119Scalar = (vx_float32)10;
            vxCopyScalar((vx_scalar)graph_0_0->getData("vxData119"), &vxData119Scalar, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
        }
        if (status == VX_SUCCESS) {
            if (!this->headless) {
                this->ioComMap.emplace("ioCom3", std::make_shared<VXUtils::Media::Display>(context0->getVxContext(), "Display for ioCom3"));
            }
        }
        if (status == VX_SUCCESS) {
            status = context0->process();
        }
        this->removeContext("context0");
        return status;
    }

    bool VisualGraph::IsHeadless() const {
        return this->headless;
    }

    void VisualGraph::setHeadless(bool value) {
        this->headless = value;
    }

    const std::string &VisualGraph::getStreamDataName() const {
        return this->streamDataName;
    }

    void VisualGraph::setStreamDataName(const std::string &name) {
        this->streamDataName = name;
    }

    const BaseContext *VisualGraph::getContext(const std::string &name) const {
        if (this->contextsMap.find(name) != this->contextsMap.end()) {
            return this->contextsMap.at(name);
        }
        return nullptr;
    }

    void VisualGraph::removeContext(const std::string &name) {
        if (this->contextsMap.find(name) != this->contextsMap.end()) {
            delete this->contextsMap.at(name);
            this->contextsMap.erase(name);
        }
    }

    std::shared_ptr<VXUtils::Interfaces::IIOCom> VisualGraph::getIoCom(const std::string &$name) const {
        std::shared_ptr<VXUtils::Interfaces::IIOCom> retPtr = nullptr;
        auto it = this->ioComMap.find($name);
        if (it != this->ioComMap.end()) {
            retPtr = it->second;
        }
        return retPtr;
    }

    bool VisualGraph::IsLooped() const {
        return this->isLooped;
    }

    void VisualGraph::setIsLooped(bool $isLooped) {
        this->isLooped = $isLooped;
    }
}
