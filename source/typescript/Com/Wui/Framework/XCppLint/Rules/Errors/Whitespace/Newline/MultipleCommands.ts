/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;

    export class MultipleCommands extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MULTIPLE_COMMANDS);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (StringUtils.Contains($processingInfo.Child().body, "for (")) {
                return;
            }

            if (StringUtils.EndsWith($processingInfo.Child().body, ";")) {
                const nextChild : IToken = this.getNextChild($processingInfo.Parent(), $processingInfo.Child().childIndex);
                if (nextChild && nextChild.type === TokenType.BLOCK) {
                    if (nextChild.line === $processingInfo.Child().line &&
                        $processingInfo.Child().childIndex !== $processingInfo.Parent().children.length - 2) {
                        if (($processingInfo.Child().body.trim() !== ");" ||
                            ($processingInfo.Child().body.trim() === ");" && !StringUtils.StartsWith(nextChild.body, "\n"))) &&
                            $processingInfo.Child().body.trim() !== "};") {
                            if (!($processingInfo.PrevChild() && $processingInfo.PrevChild().body.trim() === "do {")) {
                                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                            }
                        }
                    }
                }
                if ($processingInfo.PrevToken() &&
                    StringUtils.EndsWith($processingInfo.PrevToken().body, ";")) {
                    if ($processingInfo.Child().line === $processingInfo.PrevToken().line) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                    if ($processingInfo.Child().line === $processingInfo.PrevToken().line + 1 &&
                        StringUtils.Contains($processingInfo.PrevToken().body, "\n")) {
                        if (($processingInfo.Child().body.trim() !== ");" ||
                            ($processingInfo.Child().body.trim() === ");" && !StringUtils.StartsWith(nextChild.body, "\n"))) &&
                            $processingInfo.Child().body.trim() !== "};") {
                            if (!StringUtils.Contains($processingInfo.Child().body, "using")) {
                                if (!StringUtils.StartsWith($processingInfo.Child().body, "\n") &&
                                    !StringUtils.StartsWith($processingInfo.Child().body, "\r\n")) {
                                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                                }
                            }
                        }
                    }
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const nextChild : IToken = this.getNextChild($pinfo.Parent(), $pinfo.Child().childIndex);
            if (nextChild &&
                !StringUtils.StartsWith(nextChild.body, "\n")) {
                if (!StringUtils.StartsWith(nextChild.body, "\n")) {
                    if ($pinfo.Child().body === ");") {
                        $pinfo.Child().body = $pinfo.Child().body + "\n";
                    } else {
                        nextChild.body = "\n" + nextChild.body;
                    }
                }
            }
            if ($pinfo.PrevToken() &&
                !StringUtils.StartsWith($pinfo.Child().body, "\n")) {
                if (!StringUtils.StartsWith($pinfo.Child().body, "\n")) {
                    if (!StringUtils.EndsWith($pinfo.PrevToken().body, "\n")) {
                        if ($pinfo.Child().body !== ");\n") {
                            $pinfo.Child().body = "\n" + $pinfo.Child().body;
                        }
                    }
                }
            }
            return true;
        }
    }
}
