/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import CodeBlocksParser = Com.Wui.Framework.XCppLint.Core.CodeBlocksParser;

    export class MissingBeforeComment extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MISSING_BEFORE_COMMENT);
            this.setSensitivity(TokenType.COMMENT);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (BaseLintRule.isClass($processingInfo.Parent().body)) {
                const prevChild : IToken = $processingInfo.PrevChild();
                if (prevChild && prevChild.type === TokenType.BLOCK && !BaseLintRule.isVisibilitySpec(prevChild.body) &&
                    !StringUtils.StartsWith($processingInfo.Child().body.trim(), "//")) {
                    if (!StringUtils.StartsWith($processingInfo.Child().body, "\n\n")) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const lnl : string = CodeBlocksParser.getLeadingNewlines($pinfo.Child().body);
            if (lnl.length === 1) {
                $pinfo.Child().body = "\n" + $pinfo.Child().body;
            } else if (lnl.length === 0) {
                $pinfo.Child().body = "\n\n" + $pinfo.Child().body;
            }

            return true;
        }
    }
}
