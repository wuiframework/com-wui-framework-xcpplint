/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Indent {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;

    export class LineStart extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.INDENT.LINE_START);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const regexp : RegExp = /^ +/gm;
            const matchResult : RegExpExecArray = regexp.exec($processingInfo.Child().body);

            const prev : string = $processingInfo.PrevLine();

            let initialSpaces : number = 0;
            if (matchResult !== null) {
                initialSpaces = StringUtils.Length(matchResult[0]);
            }

            if (!/[",=><] *$/gm.test(prev) &&
                !StringUtils.Contains($processingInfo.Child().body, "else") &&
                !StringUtils.Contains($processingInfo.Child().body, "while") &&
                !StringUtils.Contains($processingInfo.Child().body, "private") &&
                !StringUtils.Contains($processingInfo.Child().body, "public") &&
                !StringUtils.Contains($processingInfo.Child().body, "protected") &&
                !StringUtils.Contains($processingInfo.Child().body, "catch") &&
                (initialSpaces === 1 || initialSpaces === 3) &&
                !/\s*\w+\s*:\s*\\?$/g.test($processingInfo.Child().body) &&
                !/^\s*""/g.test($processingInfo.Child().body)) {
                const prevChild : IToken = $processingInfo.PrevChild();
                if (!(prevChild && StringUtils.StartsWith(prevChild.body.trim(), "typedef struct "))) {
                    if ($processingInfo.Parent().line !== $processingInfo.Child().line) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }

        }
    }
}
