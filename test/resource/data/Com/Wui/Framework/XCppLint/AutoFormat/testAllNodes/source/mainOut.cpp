/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <iostream>
#include <string>
#include "IManager.hpp"

#ifdef WIN_PLATFORM
#include <windows.h>
#else
#include <dlfcn.h>
#endif

using std::string;

int main(int argc, const char *argv[]) {
    bool isHeadless = false;
    bool isLooped = false;
    bool printHelp = false;
    for (int i = 0; i < argc - 1; ++i) {
        std::string key = argv[1 + i];
        if (key == "--help" || key == "-h") {
            printHelp = true;
        } else if (key == "--headless") {
            isHeadless = true;
        } else if (key == "--looped") {
            isLooped = true;
        }
    }
    if (printHelp) {
        std::cout << "______________________________________________________________________" << std::endl;
        std::cout << std::endl;
        std::cout << "NewVisualProject" << std::endl;
        std::cout << "\t- " << "Runner for visual graph" << std::endl;
        std::cout << "\tcopyright:  " << "Copyright" << std::endl;
        std::cout << "\tauthor:     " << "Author" << std::endl;
        std::cout << "\tlicense:    " << "License" << std::endl;
        std::cout << "______________________________________________________________________" << std::endl;
        std::cout << "//////////////////////////////////////////////////////////////////////" << std::endl;
        std::cout << std::endl;
        std::cout << "-h, --help" << std::endl;
        std::cout << "\t\tPrint this help and exit." << std::endl;
        std::cout << "--looped" << std::endl;
        std::cout << "\t\tUse this switch to force continuous graph processing." << std::endl;
        std::cout << "--headless" << std::endl;
        std::cout << "\t\tDisable native windows mostly connected with display IOCom." << std::endl;
        std::cout << std::endl;
        std::cout << "----------------------------------------------------------------------" << std::endl;
        return 0;
    }
    int retVal = 0;
    typedef void *(*getInstanceType)();
    getInstanceType getInstance;
    NewVisualProject::IManager *manager = nullptr;
    auto process = [&](getInstanceType &$getInstance) {
        manager = static_cast<NewVisualProject::IManager *>($getInstance());
        if (manager != nullptr) {
            if ((retVal = manager->Create()) == 0) {
                if ((retVal = manager->Validate()) == 0) {
                    retVal = manager->Process(NewVisualProject::ProcessOptions{.looped = isLooped, .headless = isHeadless});
                }
            }
        } else {
            std::cerr << "Loaded instance is NULL. Something wrong happen..." << std::endl;
        }
    };
#ifdef WIN_PLATFORM
    HMODULE libraryHandle = LoadLibrary("VisionGraph.dll");
    if (libraryHandle != nullptr) {
        getInstance = (getInstanceType)GetProcAddress(libraryHandle, "getInstance");
        if (getInstance != nullptr) {
            process(getInstance);
        } else {
            std::cerr << "A \"getInstance\" function can not be located in loaded library. "
                         "Please check if library contains exported method getInstance() and implements IManager interface." << std::endl;
            retVal = static_cast<int>(GetLastError());
        }
        FreeLibrary(libraryHandle);
    } else {
        std::cerr << "Can not load library from path: ./VisionGraph.dll or one of its dependencies." << std::endl;
        retVal = ERROR_DLL_NOT_FOUND;
    }
#else
    void *libraryHandle = dlopen("./libVisionGraph.so", RTLD_LAZY);
    if (libraryHandle != nullptr) {
        getInstance = (getInstanceType)dlsym(libraryHandle, "getInstance");
        if (getInstance != nullptr) {
            process(getInstance);
        } else {
            std::cerr << "A \"getInstance\" function can not be located in loaded library. "
                         "Please check if library contains exported method getInstance() and implements IManager interface." << std::endl;
            std::cerr << dlerror() << std::endl;
            retVal = 1;
        }
        dlclose(libraryHandle);
    } else {
        std::cerr << "Can not load library from path: ./libVisionGraph.so or one of its dependencies." << std::endl;
        std::cerr << dlerror() << std::endl;
        retVal = 1;
    }
#endif
    return retVal;
}
