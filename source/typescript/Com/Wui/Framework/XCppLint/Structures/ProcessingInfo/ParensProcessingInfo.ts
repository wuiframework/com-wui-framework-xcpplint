/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Structures.ProcessingInfo {
    "use strict";

    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ParensProcessingInfo extends BaseProcessingInfo {
        private fncall : string;
        private commonCondition : boolean;

        constructor() {
            super();
            this.fncall = "";
            this.commonCondition = false;
        }

        public Clear() : void {
            this.fncall = "";
            this.commonCondition = false;
        }

        public Fncall($fncall? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($fncall)) {
                this.fncall = $fncall;
            }
            return this.fncall;
        }

        public CommonCondition($commonCondition? : boolean) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($commonCondition)) {
                this.commonCondition = $commonCondition;
            }
            return this.commonCondition;
        }

    }
}
