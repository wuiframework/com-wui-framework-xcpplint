/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.BlankLine {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;

    export class Preceded extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.BLANK_LINE.PRECEDED);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (/(public|private|protected):/g.test($processingInfo.Child().body)) {
                if (!/(\n{2} {1,})(public|private|protected):/.test($processingInfo.Child().body)) {
                    if ($processingInfo.Child().childIndex !== 0) {
                        if ($processingInfo.PrevToken() && $processingInfo.PrevToken().type === TokenType.DIRECTIVE &&
                            StringUtils.EndsWith($processingInfo.PrevToken().body, "\n\n")) {
                            return;
                        }
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            $pinfo.Child().body = "\n" + $pinfo.Child().body;
            return true;
        }
    }
}
