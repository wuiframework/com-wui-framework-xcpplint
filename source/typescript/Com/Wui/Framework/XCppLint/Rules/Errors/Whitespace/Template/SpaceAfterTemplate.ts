/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Template {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import TemplateBefore = Com.Wui.Framework.XCppLint.Rules.Before.TemplateBefore;
    import TemplateProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.TemplateProcessingInfo;

    export class SpaceAfterTemplate extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.TEMPLATE.SPACE_AFTER_TEMPLATE);
            this.setOnBefore(TemplateBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : TemplateProcessingInfo = <TemplateProcessingInfo>this.getOnBefore().getResults($processingInfo);

            if (pInfo.Matches() && StringUtils.EndsWith(pInfo.Matches()[1], " ")) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
