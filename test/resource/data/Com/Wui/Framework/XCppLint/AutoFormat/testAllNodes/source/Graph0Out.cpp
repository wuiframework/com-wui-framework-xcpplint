/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <fstream>

#include "BaseContext.hpp"
#include "BaseGraph.hpp"
#include "Graph0.hpp"
#include "VisualGraph.hpp"

namespace NewVisualProject {
    namespace VXUtils = Io::VisionSDK::Studio::Libs::VXUtils;

    std::vector<std::string> Graph0::testCasesplit(std::string::const_iterator it, std::string::const_iterator end,
                                                   const std::regex &e) {
        std::smatch m{};
        std::vector<std::string> ret{};
        while (std::regex_search(it, end, m, e)) {
            ret.emplace_back(m.str());
            std::advance(it, m.position() + m.length());
        }
        return ret;
    }

    Graph0::Graph0(BaseContext *parent)
            : BaseGraph(parent) {}

    vx_status Graph0::create() {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData0"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->getParent()->Check(this->vxDataMap["vxData0"]);
            vx_int16 vxData46Value[3][3] = {
                    {1, 0, -1},
                    {3, 0, -3},
                    {1, 0, -1}
            };
            this->vxDataMap["vxData46"] = (vx_reference)vxCreateConvolution(this->getParent()->getVxContext(), 3, 3);
            vxCopyConvolutionCoefficients((vx_convolution)this->vxDataMap["vxData46"], &vxData46Value[0][0], VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
            this->getParent()->Check(this->vxDataMap["vxData46"]);
            this->vxDataMap["vxData47"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData45"]), BaseGraph::getImageHeight(this->vxDataMap["vxData45"]), VX_DF_IMAGE_S16);
            this->getParent()->Check(this->vxDataMap["vxData47"]);
            this->vxDataMap["vxData48"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_float32 vxData55Value = 10000.000f;
            std::vector<vx_uint8> vxData105Data = {
                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
                    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                    81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
                    101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
                    121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,
                    141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160,
                    161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180,
                    181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200,
                    201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220,
                    221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240,
                    241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255
            };
            vx_float32 vxData111Value[3][2] = {
                    {1.000f, 1.000f},
                    {2.000f, 2.000f},
                    {0.500f, 0.500f}
            };
            vx_float32 vxData114Value[3][3] = {
                    {1.000f, 1.000f, 1.000f},
                    {2.000f, 2.000f, 2.000f},
                    {0.500f, 0.500f, 0.500f}
            };
        }
        return status;
    }

    vx_status Graph0::validate() {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            status = this->getParent()->Check(vxVerifyGraph(this->getVxGraph()));
        }
        return status;
    }

    vx_status Graph0::process() const {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        int i = 0;
        VXUtils::Utils::StopWatch stopWatch{};
        do {
            std::string vxDataName;
            std::ifstream ifs("build/runtime.config");
            std::string str;
            ifs >> str;
            ifs.close();
            auto items = testCasesplit(str.begin(), str.end(), std::regex(R"(\w+=[\w\d"_-]+)"));
            for (const auto &item : items) {
                auto index = item.find("run=");
                if (index == 0) {
                    std::string tmp = item.substr(4);
                    if (tmp != "true") {
                        const_cast<VisualGraph *>(this->getParent()->getParent())->setIsLooped(false);
                    }
                }
                index = item.find("vxDataName=");
                if (index == 0) {
                    vxDataName = item.substr(11);
                    if (vxDataName[0] == '"') {
                        vxDataName = vxDataName.substr(1, vxDataName.size() - 2);
                    }
                }
            }
            stopWatch.Start();
            if (status == VX_SUCCESS) {
                auto ioCom0 = this->getParent()->getParent()->getIoCom("ioCom0");
                if (i == 0) {
                    vx_image vxData116Ref = (vx_image)this->getData("vxData116");
                    ioCom0->getFrame(vxData116Ref, true);
                }
            }
            if (status == VX_SUCCESS) {
                auto ioCom2 = this->getParent()->getParent()->getIoCom("ioCom2");
                vx_image vxData118Ref = (vx_image)this->getData("vxData118");
                ioCom2->getFrame(vxData118Ref, true);
            }
            if (status == VX_SUCCESS) {
                status = this->getParent()->Check(vxProcessGraph(this->getVxGraph()));
            }
            if (status == VX_SUCCESS) {
                auto ioCom1 = this->getParent()->getParent()->getIoCom("ioCom1");
                vx_image vxData117Ref = (vx_image)this->getData("vxData117");
                ioCom1->setFrame(vxData117Ref);
            }
            if (status == VX_SUCCESS && !this->getParent()->getParent()->IsHeadless()) {
                auto ioCom3 = this->getParent()->getParent()->getIoCom("ioCom3");
                vx_image vxData120Ref = (vx_image)this->getData("vxData120");
                ioCom3->setFrame(vxData120Ref);
            }
            stopWatch.Stop();
            std::cout << "Processing of graph: \"Graph0\" spend (" << i << "): " << stopWatch.getElapsed() << " ms" << std::endl;
            if (this->getParent()->getParent()->IsHeadless() && !vxDataName.empty()) {
                VXUtils::Streaming::VxImageStreamer::SendQuery(this->getParent()->getVxContext(), (vx_image)this->getData(vxDataName));
            }
        } while ((this->loopCondition(i++) || this->getParent()->getParent()->IsLooped()) && (status == VX_SUCCESS));
        return status;
    }

    bool Graph0::loopCondition(int $loopCnt) const {
        return false;
    }
}
