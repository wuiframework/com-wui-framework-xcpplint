namespace NewVisualProject {
    class BaseGraph {
     public:
        static vx_df_image_e getImageType(vx_reference $image);

        explicit BaseGraph(BaseContext *context);

     protected:
        bool loopCondition(int $loopCnt) const {
            int foo;
            int bar;
            if (foo) {
                bar();
            }
        }
    };
}
