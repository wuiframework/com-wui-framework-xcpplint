/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import DirectiveType = Com.Wui.Framework.XCppLint.Enums.DirectiveType;
    import Directive = Com.Wui.Framework.XCppLint.Core.Structures.Directive;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class DirectivesParser extends BaseObject {

        /**
         * @param {string} $data String on which directives are detected.
         * @return {ArrayList<Directive>} Return an array of directives.
         */
        public Parse($data : string) : ArrayList<Directive> {
            const directiveParser : DirectivesParser = new DirectivesParser();
            const directives : ArrayList<Directive> = new ArrayList<Directive>();
            let charIndex : number = 0;
            let lastSpaceIndex : number = -1;
            let start : number = -1;
            const dataLength : number = StringUtils.Length($data);
            while (charIndex < dataLength) {
                if ($data[charIndex] === "\n") {
                    lastSpaceIndex = -1;
                }
                if (charIndex > 0 && $data[charIndex - 1] !== " " && $data[charIndex] === " ") {
                    lastSpaceIndex = charIndex;
                }
                if ($data[charIndex] === "#") {
                    if (lastSpaceIndex !== -1) {
                        start = lastSpaceIndex;
                    } else {
                        start = charIndex;
                    }
                    let currentDirective : Directive = directiveParser.processDirective($data, start);
                    currentDirective = directiveParser.classifyDirective(currentDirective);
                    if (!ObjectValidator.IsEmptyOrNull(currentDirective.Type())) {
                        directives.Add(currentDirective);
                    }
                }
                start = -1;
                charIndex++;
            }
            return directives;
        }

        private processDirective($data : string, $from : number) : Directive {
            const directive : Directive = new Directive();
            let charIndex : number = $from;
            let isSlashed : boolean = false;
            let lineIndex : number = -1;
            let end : number = -1;
            let finished : boolean = false;
            const dataLength : number = StringUtils.Length($data);
            while (charIndex < dataLength) {
                if (!finished) {
                    if ($data[charIndex] === "\n" && !isSlashed) {
                        end = charIndex + 1;
                        finished = true;
                    } else {
                        if ($data[charIndex] === "\\" && $data[charIndex + 2] === "\n" && !isSlashed) {
                            isSlashed = true;
                        }
                        if ($data[charIndex] !== "\\" && $data[charIndex + 2] === "\n" && isSlashed) {
                            end = charIndex + 1;
                            isSlashed = false;
                        }
                    }
                    if (lineIndex === -1 || lineIndex === $from) {
                        if ($data[charIndex + 1] === "\r" && $data[charIndex + 2] === "\n") {
                            end += 2;
                            if ($data[charIndex + 3] === "\r" && $data[charIndex + 4] === "\n") {
                                end += 2;
                            } else if ($data[charIndex + 3] === "\n") {
                                end += 1;
                            }
                        } else if ($data[charIndex + 1] === "\n") {
                            end += 1;
                            if ($data[charIndex + 2] === "\r" && $data[charIndex + 3] === "\n") {
                                end += 2;
                            } else if ($data[charIndex + 2] === "\n") {
                                end += 1;
                            }
                        }
                    }
                    directive.StartIndex($from);
                    directive.EndIndex(end);
                    directive.Contents(StringUtils.Substring($data, $from, end));
                }
                charIndex++;
                end = -1;
                lineIndex = -1;
            }
            return directive;

        }

        private classifyDirective($directive : Directive) : Directive {
            let contents : string = $directive.Contents();

            const matches : RegExpExecArray = /(#)(\s*)(\w*)(\s*).*/gm.exec(contents);

            if (matches) {
                contents = matches[1] + matches[3] + " ";
            }

            let type : string;
            if (StringUtils.StartsWith(contents, "#else") ||
                StringUtils.StartsWith(contents, "#endif") ||
                StringUtils.StartsWith(contents, "#line")) {
                type = contents;
            } else {
                type = StringUtils.Substring(contents, 0,
                    StringUtils.IndexOf(contents, " ", false, StringUtils.IndexOf(contents, "#")));
            }
            if (StringUtils.Contains(type, "#line")) {
                $directive.Type(DirectiveType.LINE);
            } else if (StringUtils.Contains(type, "#else")) {
                $directive.Type(DirectiveType.ELSE);
            } else if (StringUtils.Contains(type, "#elif")) {
                $directive.Type(DirectiveType.ELIF);
            } else if (StringUtils.Contains(type, "#include")) {
                $directive.Type(DirectiveType.INCLUDE);
            } else if (StringUtils.Contains(type, "#undef")) {
                $directive.Type(DirectiveType.UNDEF);
            } else if (type === "#if") {
                $directive.Type(DirectiveType.IF);
            } else if (StringUtils.Contains(type, "#ifndef")) {
                $directive.Type(DirectiveType.IFNDEF);
            } else if (StringUtils.Contains(type, "#ifdef")) {
                $directive.Type(DirectiveType.IFDEF);
            } else if (StringUtils.Contains(type, "#define")) {
                $directive.Type(DirectiveType.DEFINE);
            } else if (StringUtils.Contains(type, "#error")) {
                $directive.Type(DirectiveType.ERROR);
            } else if (StringUtils.Contains(type, "#warning")) {
                $directive.Type(DirectiveType.WARNING);
            } else if (StringUtils.Contains(type, "#pragma")) {
                $directive.Type(DirectiveType.PRAGMA);
            } else if (StringUtils.Contains(type, "#endif")) {
                $directive.Type(DirectiveType.ENDIF);
            } else if (StringUtils.Contains(type, "#using")) {
                $directive.Type(DirectiveType.USING);
            } else if (StringUtils.Contains(type, "#import")) {
                $directive.Type(DirectiveType.IMPORT);
            }
            return $directive;
        }
    }
}
