/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Structures {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;

    export abstract class BaseBefore extends BaseObject {
        private done : boolean;

        constructor() {
            super();
            this.done = false;
        }

        /**
         * @param {BaseProcessingInfo} $processingInfo Additional information neccessary to perform "before" computation.
         * Also runs the computation.
         * @return {BaseProcessingInfo} Return extended BaseProcessingInfo, containing rule-specific additional information.
         */
        public getResults($processingInfo : BaseProcessingInfo) : BaseProcessingInfo {
            return null;
        }

        public Reset() : void {
            this.Done(false);
        }

        protected Done($done? : boolean) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($done)) {
                this.done = $done;
            }
            return this.done;
        }
    }
}
