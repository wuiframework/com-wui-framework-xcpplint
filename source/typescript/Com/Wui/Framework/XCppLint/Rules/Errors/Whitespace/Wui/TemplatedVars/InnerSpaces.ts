/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Wui.TemplatedVars {
    "use strict";
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import TemplatedVarsBefore = Com.Wui.Framework.XCppLint.Rules.Before.TemplatedVarsBefore;
    import TemplatedVarsProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.TemplatedVarsProcessingInfo;

    export class InnerSpaces extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.WUI.TEMPLATED_VARS.INNER_SPACES);
            this.setOnBefore(TemplatedVarsBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (this.isCppCast($processingInfo.Child())) {
                return;
            }

            const pInfo : TemplatedVarsProcessingInfo = <TemplatedVarsProcessingInfo>this.getOnBefore().getResults($processingInfo);

            if (pInfo.Matches() && (StringUtils.Contains(pInfo.Matches()[1], "< ") ||
                StringUtils.Contains(pInfo.Matches()[1], " >"))) {
                if (!StringUtils.Contains($processingInfo.Child().body, pInfo.Matches()[0])) {
                    return;
                }

                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
