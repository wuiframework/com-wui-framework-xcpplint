/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            /* dev:start */
            Echo.Print("<H3>Runtime tests</H3>");
            Echo.Println("<a href=\"#" + this.createLink("/web/LinterTest") + "\">Linter test</a>");
            /* dev:end */
        }
    }
}
