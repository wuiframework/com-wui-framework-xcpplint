/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <list>
#include "BaseContext.hpp"
#include "BaseGraph.hpp"

namespace NewVisualProject {
    using Io::VisionSDK::Studio::Libs::VXUtils::Primitives::VxReferenceInfo;
    using Io::VisionSDK::Studio::Libs::VXUtils::Utils::VxTranslator;

    static std::list<BaseContext *> contexts{};

    void VX_CALLBACK logHandler(vx_context $context, vx_reference $ref, vx_status $status, const vx_char $string[]) {
        for (auto &item : contexts) {
            if (item->getVxContext() == $context) {
                item->AddLog("[" + VxTranslator::Translate($status) + "]: " + $string);
            }
        }
    }

    BaseContext::BaseContext(VisualGraph *parent)
            : parent(parent) {
        this->vxContext = vxCreateContext();
        contexts.emplace_back(this);
        logger.clear();
        vxRegisterLogCallback(this->vxContext, logHandler, vx_false_e);
    }

    BaseContext::~BaseContext() {
        if (this->vxContext != nullptr) {
            contexts.remove(this);
            for (auto it : this->graphsMap) {
                delete it.second;
            }
            this->graphsMap.clear();
            vxReleaseContext(&this->vxContext);
        }
    }

    const VisualGraph *BaseContext::getParent() const {
        return this->parent;
    }

    vx_context BaseContext::getVxContext() const {
        return this->vxContext;
    }

    const BaseGraph *BaseContext::getGraph(const std::string &name) const {
        if (this->graphsMap.find(name) != this->graphsMap.end()) {
            return this->graphsMap.at(name);
        }
        return nullptr;
    }

    void BaseContext::removeGraph(const std::string &name) {
        if (this->graphsMap.find(name) != this->graphsMap.end()) {
            delete this->graphsMap.at(name);
            this->graphsMap.erase(name);
        }
    }

    vx_status BaseContext::Check(const VxReferenceInfo &$referenceInfo) const {
        return this->Check(vxGetStatus($referenceInfo), $referenceInfo.getLine(), $referenceInfo.getFile());
    }

    vx_status BaseContext::Check(const vx_status &$status, int $line, const std::string &$file) const {
        if ($status != VX_SUCCESS) {
            unsigned index = $file.find_last_of('\\');
            if (index == std::string::npos) {
                index = $file.find_last_of('/');
            }
            std::cout << "Check failed: " << VxTranslator::Translate($status, true) << " in " << $file.substr(index + 1) << ":" << $line << std::endl;
            std::string logString = this->logger.str();
            if (!logString.empty()) {
                std::cout << "details: " << std::endl << logString;
                const_cast<BaseContext *>(this)->logger.clear();
            }
        }
        return $status;
    }

    void BaseContext::AddLog(const std::string &$data) const {
        const_cast<BaseContext *>(this)->logger << $data << std::endl;
    }
}
