/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class DirectiveType extends BaseEnum {
        public static readonly IFNDEF : string = "#ifndef";
        public static readonly IFDEF : string = "#ifdef";
        public static readonly INCLUDE : string = "#include";
        public static readonly DEFINE : string = "#define";
        public static readonly UNDEF : string = "#undef";
        public static readonly ERROR : string = "#error";
        public static readonly WARNING : string = "#warning";
        public static readonly LINE : string = "#line";
        public static readonly PRAGMA : string = "#pragma"; // OpenMP only
        public static readonly IF : string = "#if";
        public static readonly ELIF : string = "#elif";
        public static readonly ELSE : string = "#else";
        public static readonly ENDIF : string = "#endif";
        public static readonly USING : string = "#using";
        public static readonly IMPORT : string = "#import";
    }
}
