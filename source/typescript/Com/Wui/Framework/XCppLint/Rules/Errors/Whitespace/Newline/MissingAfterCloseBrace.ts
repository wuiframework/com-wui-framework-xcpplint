/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class MissingAfterCloseBrace extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MISSING_AFTER_CLOSE_BRACE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if ($processingInfo.PrevToken() &&
                $processingInfo.PrevToken().body.trim() === "}" &&
                this.requiresNewLine($processingInfo.PrevChild().body) &&
                !BaseLintRule.isDo($processingInfo.PrevChild().body) &&
                !StringUtils.StartsWith($processingInfo.Child().body, "\n")) {
                if (!StringUtils.StartsWith($processingInfo.Child().body.trim(), "else") &&
                    !StringUtils.StartsWith($processingInfo.PrevChild().body.trim(), "class ")) {
                    if ($processingInfo.Child().body === ");" && StringUtils.EndsWith($processingInfo.PrevToken().body, "}")) {
                        return;
                    }

                    if (StringUtils.Contains($processingInfo.Parent().body, "json")) {
                        return;
                    }

                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            if (!StringUtils.StartsWith($pinfo.Child().body, "\n")) {
                $pinfo.Child().body = "\n" + $pinfo.Child().body;
            }

            return true;
        }
    }
}
