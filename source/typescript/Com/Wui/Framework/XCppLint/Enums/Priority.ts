/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Enums {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class Priority extends BaseEnum {
        public static readonly HIGH : string = "HIGH";
        public static readonly MEDIUM : string = "MEDIUM";
        public static readonly LOW : string = "LOW";
    }
}
