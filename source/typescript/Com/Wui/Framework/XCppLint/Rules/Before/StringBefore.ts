/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Before {
    "use strict";
    import BaseBefore = Com.Wui.Framework.XCppLint.Structures.BaseBefore;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StringProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.StringProcessingInfo;

    export class StringBefore extends BaseBefore {
        private static instance : StringBefore;
        private processingInfo : StringProcessingInfo;

        public static getInstance() : BaseBefore {
            if (!StringBefore.instance) {
                StringBefore.instance = new StringBefore();
            }
            return StringBefore.instance;
        }

        constructor() {
            super();
            this.processingInfo = new StringProcessingInfo();
        }

        public Reset() : void {
            super.Reset();
            this.processingInfo.Clear();
        }

        public getResults($processingInfo : BaseProcessingInfo) : StringProcessingInfo {
            this.run($processingInfo);
            return this.processingInfo;
        }

        private run($processingInfo : BaseProcessingInfo) : void {
            if (!this.Done()) {
                const match : RegExpExecArray =
                    /((?:|static +)(?:|const +))(?::*std::)?string( +const)? +([a-zA-Z0-9_:]+)\b(.*)/gm.exec($processingInfo.Child().body);
                if (match &&
                    !StringUtils.StartsWith(StringUtils.Remove($processingInfo.Child().body, "\n"), " ") &&
                    !/\bstring\b(\s+const)?\s*[\*\&]\s*(const\s+)?\w/gm.test($processingInfo.Child().body) &&
                    !/\boperator\W/gm.test($processingInfo.Child().body) &&
                    !/\s*(<.*>)?(::[a-zA-Z0-9_]+)*\s*\(([^"]|$)/gm.test(match[4])) {
                    this.processingInfo.CommonCondition(true);
                }

                this.Done(true);
            }
        }
    }
}
