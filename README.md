# com-wui-framework-xcpplint v2019.1.0

> Lint tool for Cpp projects, which requires WUI Framework coding standards.

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Fixed indenting.
### v2019.0.2
Updated test resources configuration. Fixed handling of negative lookbehind syntax at unsupported environments. 
Fixed extern C formatting. Usage of new app loader.
### v2019.0.1
Added documentation. Fixed false positives and parser bugs. Added more tests.
### v2019.0.0
Added ability to use rules configuration. Added autofix ability for another set of rules. Added sorting validation and fix. 
Fixed several parser issues. Added more tests.
### v2018.3.0
Refactoring of CPP parser and project structure. Added implementation of linting rules and several autofix methods. Update of WUI Core. 
### v2018.0.0
Update of WUI Core. Added basic Life-Cycle environment. Added basic Rules implementation.
### v1.1.1
Added regEx-based code blocks classifier.
### v1.1.0
Added initial version of source code parser.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
