/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core.Structures {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DirectiveType = Com.Wui.Framework.XCppLint.Enums.DirectiveType;

    export class Directive extends BaseObject {
        private type : DirectiveType;
        private startIndex : number;
        private endIndex : number;
        private contents : string;

        constructor() {
            super();
            this.contents = "";
        }

        /**
         * @param {CommentType} $type If set, set $type to this.type.
         * @return {CommentType} Return type of directive.
         */
        public Type($type? : DirectiveType) : DirectiveType {
            if (!ObjectValidator.IsEmptyOrNull($type)) {
                this.type = $type;
            }
            return this.type;
        }

        /**
         * @param {number} $index If set, set $index to this.startIndex.
         * @return {number} Return starting position of directive.
         */
        public StartIndex($index? : number) : number {
            if (!ObjectValidator.IsEmptyOrNull($index)) {
                this.startIndex = $index;
            }
            return this.startIndex;
        }

        /**
         * @param {number} $index If set, set $index to this.endIndex.
         * @return {number} Return end position of directive.
         */
        public EndIndex($index? : number) : number {
            if (!ObjectValidator.IsEmptyOrNull($index)) {
                this.endIndex = $index;
            }
            return this.endIndex;
        }

        /**
         * @param {string} $text If set, set $text to this.contents.
         * @return {string} Return contents of a directive.
         */
        public Contents($text? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($text)) {
                this.contents = $text;
            }
            return this.contents;
        }

    }
}
