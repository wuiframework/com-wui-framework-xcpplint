/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Parens {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import ParensBefore = Com.Wui.Framework.XCppLint.Rules.Before.ParensBefore;
    import ParensProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.ParensProcessingInfo;

    export class ShouldBeMovedUp extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.PARENS.SHOULD_BE_MOVED_UP);
            this.setOnBefore(ParensBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : ParensProcessingInfo = <ParensProcessingInfo>this.getOnBefore().getResults($processingInfo);

            const fncall = pInfo.Fncall();
            if (pInfo.CommonCondition()) {
                if (/[^)]\s+\)\s*[^{\s]/gm.test(fncall)) {
                    if (/^\s+\)/gm.test(fncall)) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    } else {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }
    }
}
