/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class CommaInFunction extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.COMMA_IN_FUNCTION);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (BaseLintRule.isFunction($processingInfo.Child().body)) {
                const regex : RegExpExecArray = /(,)([^ \n])/gm.exec($processingInfo.Child().body);
                if (regex) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const regex : RegExpExecArray = /(,)([^ ])/gm.exec($pinfo.Child().body);
            const replacement : string = StringUtils.Replace(regex[0], ",", ", ");
            $pinfo.Child().body = StringUtils.Replace($pinfo.Child().body, regex[0], replacement);
            return true;
        }
    }
}
