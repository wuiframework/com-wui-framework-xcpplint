/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Readability.Wui {
    "use strict";
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import SpecType = Com.Wui.Framework.XCppLint.Enums.SpecType;
    import WuiProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.WuiProcessingInfo;
    import WuiBefore = Com.Wui.Framework.XCppLint.Rules.Before.WuiBefore;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ClassMember = Com.Wui.Framework.XCppLint.Structures.ClassMember;

    export class ClassOrder extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.WUI.CLASS_ORDER);
            this.setOnBefore(WuiBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : WuiProcessingInfo = <WuiProcessingInfo>this.getOnBefore().getResults($processingInfo);
            const methods : ArrayList<ClassMember> = pInfo.Members();

            if (!methods || methods.IsEmpty()) {
                return;
            }

            let prevMethodSpec : SpecType = methods.getFirst().Member().specType;

            for (const method of methods.getAll()) {
                const spec : SpecType = method.Member().specType;
                if (prevMethodSpec === SpecType.PRIVATE &&
                    spec === SpecType.PUBLIC) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                } else if (prevMethodSpec === SpecType.PROTECTED &&
                    spec === SpecType.PUBLIC) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                } else if (prevMethodSpec === SpecType.PRIVATE &&
                    spec === SpecType.PROTECTED) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
                prevMethodSpec = spec;
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const getSpec = ($spec : string, $from : IToken[]) : ClassMember => {
                for (const value of $from) {
                    if (StringUtils.StartsWith(value.body.trim(), $spec)) {
                        const ret : ClassMember = new ClassMember();
                        ret.Comment(null);
                        ret.Member(value);
                        return ret;
                    }
                }
                return null;
            };

            const wuiPInfo : WuiProcessingInfo = <WuiProcessingInfo>this.getOnBefore().getResults($pinfo);

            this.formatInsideVisibilitySpecTarget(wuiPInfo.PublicTargetMembers(), (<number>$pinfo.Config().getPropertyValue("spaces")));
            this.formatInsideVisibilitySpecTarget(wuiPInfo.ProtectedTargetMembers(), (<number>$pinfo.Config().getPropertyValue("spaces")));
            this.formatInsideVisibilitySpecTarget(wuiPInfo.PrivateTargetMembers(), (<number>$pinfo.Config().getPropertyValue("spaces")));

            const publicSpec : ClassMember = getSpec("public", $pinfo.Child().children);
            const protectedSpec : ClassMember = getSpec("protected", $pinfo.Child().children);
            const privateSpec : ClassMember = getSpec("private", $pinfo.Child().children);

            const formatVisibilitySpec = ($input : ClassMember, $index : number, $expectedIndentSize : number) : ClassMember => {
                const match : RegExpExecArray = /(\s*)(.*)/gm.exec($input.Member().body);
                const leadingWhitespace : number = match[1].length;
                const body : string = StringUtils.Substring($input.Member().body, leadingWhitespace);
                const spaces : string = this.generateSpaces(
                    $input.Member().nesting * $expectedIndentSize - $expectedIndentSize + 1);

                if ($index === 0) {
                    $input.Member().body = "\n" + spaces + body;
                } else {
                    $input.Member().body = "\n\n" + spaces + body;
                }

                return $input;
            };

            const addSpec = ($spec : ClassMember, $specArray : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                if ($spec) {
                    $target.Add(formatVisibilitySpec($spec, $target.Length(), (<number>$pinfo.Config().getPropertyValue("spaces"))));
                    $target.getLast().Member().childIndex = $target.Length() - 1;
                    $specArray.foreach(($value : ClassMember) : void => {
                        $target.Add($value);
                        $target.getLast().Member().childIndex = $target.Length() - 1;
                    });
                }
            };

            const target : ArrayList<ClassMember> = new ArrayList<ClassMember>();

            addSpec(publicSpec, wuiPInfo.PublicTargetMembers(), target);
            addSpec(protectedSpec, wuiPInfo.PublicTargetMembers(), target);
            addSpec(privateSpec, wuiPInfo.PrivateTargetMembers(), target);

            target.foreach(($value : ClassMember, $index : number) : void => {
                $pinfo.Child().children[$index] = $value.Member();
            });

            return true;
        }
    }
}
