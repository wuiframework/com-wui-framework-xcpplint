/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;

    export class MissingAfterOpenBrace extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MISSING_AFTER_OPEN_BRACE);
            this.setSensitivity(TokenType.COMMENT, TokenType.BLOCK);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (StringUtils.EndsWith($processingInfo.Parent().body, "{") &&
                $processingInfo.Child().childIndex === 0 &&
                (this.requiresNewLine($processingInfo.Parent().body) ||
                    BaseLintRule.isDo($processingInfo.Parent().body) ||
                    BaseLintRule.isTokenInExtern($processingInfo.Child()))) {
                if (StringUtils.EndsWith($processingInfo.Parent().body.trim(), ">{")) {
                    return;
                }

                // filter comments after {, i.e: void foo() {   // NOLINT
                if ($processingInfo.Child().type === TokenType.COMMENT &&
                    $processingInfo.Child().line === $processingInfo.Parent().line &&
                    StringUtils.EndsWith($processingInfo.Child().body, "\n")) {
                    return;
                }

                if (StringUtils.StartsWith($processingInfo.Parent().body.trim(), "struct") &&
                    StringUtils.Contains($processingInfo.Parent().body, "=")) {
                    return;
                }

                if (BaseLintRule.isTokenInClass($processingInfo.Child())) {
                    if (BaseLintRule.isFunction($processingInfo.Parent().body) &&
                        ($processingInfo.Parent().children.length === 2 || $processingInfo.Parent().children.length === 1) &&
                        $processingInfo.Child().children.length === 0) {
                        return;
                    }
                }

                if ((BaseLintRule.isDtor($processingInfo.Parent().body) || BaseLintRule.isCtor($processingInfo.Parent().body)) &&
                    $processingInfo.Child().body.trim() === "}" &&
                    ($processingInfo.Parent().children.length === 2 || $processingInfo.Parent().children.length === 1)) {
                    return;
                }

                if (!BaseLintRule.isTokenInClass($processingInfo.Child()) &&
                    BaseLintRule.isFunction($processingInfo.Parent().body) &&
                    $processingInfo.Parent().children.length === 1) {
                    return;
                }

                if (StringUtils.EndsWith($processingInfo.Parent().body, "{") &&
                    $processingInfo.Child().type === TokenType.COMMENT &&
                    $processingInfo.Parent().type === TokenType.BLOCK &&
                    $processingInfo.Child().line === $processingInfo.Parent().line &&
                    (StringUtils.Contains($processingInfo.Child().body, "LINT") ||
                        StringUtils.Contains($processingInfo.Child().body, "xcpplint"))) {
                    return;
                }

                if (!/^\n\s*/gm.test($processingInfo.Child().body)) {
                    if (!StringUtils.Contains($processingInfo.Parent().body, "regex")) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            if (!StringUtils.StartsWith($pinfo.Child().body, "\n")) {
                $pinfo.Child().body = "\n" + $pinfo.Child().body;
            }

            return true;
        }
    }
}
