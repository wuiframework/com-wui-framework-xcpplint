/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.XCppLint.RuntimeTests {
    "use strict";
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import Linter = Com.Wui.Framework.XCppLint.Core.Linter;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;

    export class LinterTest extends XCppLintTest {

        public testLinter() : IRuntimeTestPromise {
            return ($done : any) : void => {
                const linter : Linter = new Linter();
                const configPath : string = "test/resource/data/Com/Wui/Framework/XCppLint/config.json";

                const lintFile : any = ($path : string, $callback : () => void) : void => {
                    this.readFile(configPath, ($configData : string) : void => {
                        this.readFile($path, ($data : string) : void => {
                            linter.Run($data, $path, JSON.parse($configData), ($fixedFileData : string) : void => {
                                const outFilePath : string =
                                    this.getAbsoluteRoot() + "/../../test/resource/data/Com/Wui/Framework/XCppLint/Core/runtimeTestOut.cpp";
                                this.writeFile(outFilePath, $fixedFileData);
                            });
                            linter.getErrors().forEach(($error : LintError) : void => {
                                Echo.Println($error.ToString());
                            });
                            $callback();
                        });
                    });
                };
                lintFile(this.getAbsoluteRoot() + "/../../test/resource/data/Com/Wui/Framework/XCppLint/Core/runtimeTestFile.cpp", $done);

                Echo.Print("<b>Absolute path to lint file:</b><input id=\"lintFilePath\" type=\"text\" style=\"width: 500px;\">");
                let nextLintEnabled : boolean = true;
                this.addButton("Run lint", () : void => {
                    if (nextLintEnabled) {
                        nextLintEnabled = false;
                        lintFile((<HTMLInputElement>document.getElementById("lintFilePath")).value, () : void => {
                            nextLintEnabled = true;
                        });
                    } else {
                        Echo.Printf("Parallel lint is not allowed");
                    }
                });
            };
        }
    }
}
/* dev:end */
