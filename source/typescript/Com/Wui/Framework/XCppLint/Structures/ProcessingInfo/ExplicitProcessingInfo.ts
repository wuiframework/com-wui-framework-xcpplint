/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Structures.ProcessingInfo {
    "use strict";

    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ExplicitProcessingInfo extends BaseProcessingInfo {
        private noArgCtor : boolean;
        private oneArgCtor : boolean;
        private initializerListCtor : boolean;
        private copyCtor : boolean;
        private isMarkedExplicit : string;
        private defaultedArgs : string[];

        public Clear() : void {
            this.noArgCtor = false;
            this.oneArgCtor = false;
            this.initializerListCtor = false;
            this.copyCtor = false;
            this.isMarkedExplicit = "";
            this.defaultedArgs = [];
        }

        public NoArgCtor($noArgCtor? : boolean) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($noArgCtor)) {
                this.noArgCtor = $noArgCtor;
            }
            return this.noArgCtor;
        }

        public OneArgCtor($oneArgCtor? : boolean) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($oneArgCtor)) {
                this.oneArgCtor = $oneArgCtor;
            }
            return this.oneArgCtor;
        }

        public InitializerListCtor($initializerListCtor? : boolean) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($initializerListCtor)) {
                this.initializerListCtor = $initializerListCtor;
            }
            return this.initializerListCtor;
        }

        public CopyCtor($copyCtor? : boolean) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($copyCtor)) {
                this.copyCtor = $copyCtor;
            }
            return this.copyCtor;
        }

        public IsMarkedExplicit($isMarkedExplicit? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($isMarkedExplicit)) {
                this.isMarkedExplicit = $isMarkedExplicit;
            }
            return this.isMarkedExplicit;
        }

        public DefaultedArgs($defaultedArgs? : string[]) : string[] {
            if (!ObjectValidator.IsEmptyOrNull($defaultedArgs)) {
                this.defaultedArgs = $defaultedArgs;
            }
            return this.defaultedArgs;
        }
    }
}
