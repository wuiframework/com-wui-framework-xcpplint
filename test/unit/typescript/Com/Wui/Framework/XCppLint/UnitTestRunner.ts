/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint {
    "use strict";
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import UnitTestEnvironmentArgs = Com.Wui.Framework.Commons.UnitTestEnvironmentArgs;

    export class UnitTestLoader extends Loader {
        protected initEnvironment() : UnitTestEnvironmentArgs {
            return new UnitTestEnvironmentArgs();
        }
    }

    export class UnitTestRunner extends Com.Wui.Framework.UnitTestRunner {

        public readJsonFromFile($path : string, $source : any) : any {
            return JSON.parse(builder.getFileSystemHandler().Read($source.getAbsoluteRoot() + "/" + $path).toString());
        }

        public readFileRelative($path : string, $source : any) : string {
            return builder.getFileSystemHandler().Read($source.getAbsoluteRoot() + "/" + $path).toString();
        }

        public readFileAbsolute($path : string) : string {
            return builder.getFileSystemHandler().Read($path).toString();
        }

        public Expand($pattern : string | string[]) : string[] {
            return builder.getFileSystemHandler().Expand($pattern);
        }

        public isErrorInArray($errorToFind : LintError | any, $arrayToSearch : ArrayList<LintError>) : boolean {
            let lineToFind : number;
            let messageToFind : string;

            try {
                ObjectValidator.IsObject($errorToFind);
                (<LintError>$errorToFind).IsTypeOf(LintError);

                lineToFind = (<LintError>$errorToFind).getLine();
                messageToFind = (<LintError>$errorToFind).getMessage();
            } catch (exception) {
                lineToFind = (<ILintError>$errorToFind).line;
                if (ObjectValidator.IsString((<ILintError>$errorToFind).message)) {
                    messageToFind = (<string>(<ILintError>$errorToFind).message);
                } else {
                    messageToFind = (<IErrorReporterObject>(<ILintError>$errorToFind).message).message;
                }
            }

            for (const elem of $arrayToSearch.getAll()) {
                if (elem.getMessage() === messageToFind &&
                    elem.getLine() === lineToFind) {
                    return true;
                }
            }

            return false;
        }

        public compareJsonsByChildBody($json1 : any, $json2Path : string) : void {
            this.serializeJsonsByChildBody($json1, this.readJsonFromFile(
                $json2Path, this),
                ($outArray1 : string[], $outArray2 : string[]) : void => {
                    $outArray1.forEach(($value : string, $index : number) : void => {
                        assert.equal($value, $outArray2[$index]);
                    });
                });
        }

        protected initLoader() : void {
            super.initLoader(UnitTestLoader);
        }

        private serializeJsonsByChildBody($json1 : any, $json2 : any,
                                          $callback : ($arr1 : string[], $arr2 : string[]) => void) : void {
            const outArray1 : string[] = [];
            const outArray2 : string[] = [];

            const serialize = ($parent : IToken, $out : string[]) : void => {
                for (const child of $parent.children) {
                    $out.push(child.body);
                    serialize(child, $out);
                }
            };

            serialize($json1, outArray1);
            serialize($json2, outArray2);

            $callback(outArray1, outArray2);
        }
    }

    interface ILintError {
        message : string | IErrorReporterObject;
        line : number;
    }
}
