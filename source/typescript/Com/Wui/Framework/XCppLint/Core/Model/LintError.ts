/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core.Model {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Severity = Com.Wui.Framework.XCppLint.Enums.Severity;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import FixStatus = Com.Wui.Framework.XCppLint.Enums.FixStatus;

    export class LintError extends BaseObject {
        private readonly line : number;
        private readonly message : string;
        private readonly column : number;
        private readonly severity : Severity;
        private owner : string;
        private fixed : FixStatus;

        /**
         * @param {number} $line Line on which error occured.
         * @param {IErrorReporterObject} $message Error object which holds message and severity of the error.
         * @param {number} $column Column on which the error occured.
         */
        constructor($line : number, $message : IErrorReporterObject, $column : number = -1) {
            super();
            this.line = $line;

            this.message = $message.message;
            this.severity = $message.severity;

            this.column = $column;
            this.fixed = FixStatus.FIX_FALSE;
        }

        /**
         * @param {string} $filePath File name to which errors belong.
         */
        public setOwner($filePath : string) : void {
            this.owner = $filePath;
        }

        /**
         * @return {number} Return line of error.
         */
        public getLine() : number {
            return this.line;
        }

        /**
         * @return {string} Return message of error.
         */
        public getMessage() : string {
            return this.message;
        }

        /**
         * @return {number} Return column of error.
         */
        public getColumn() : number {
            return this.column;
        }

        /**
         * @return {Severity} Return severity of error.
         */
        public getSeverity() : Severity {
            return this.severity;
        }

        public Fixed() : void {
            this.fixed = FixStatus.FIX_OK;
        }

        public FixFailed() : void {
            this.fixed = FixStatus.FIX_FAIL;
        }

        public ToString() : string {
            let temp : string = "";

            if (this.fixed === FixStatus.FIX_OK) {
                temp += FixStatus.FIX_OK + ": ";
            } else if (this.fixed === FixStatus.FIX_FAIL) {
                temp += FixStatus.FIX_FAIL + ": ";
            }

            if (this.severity !== Severity.UNKNOWN) {
                temp += this.severity + ": ";
            }

            if (!ObjectValidator.IsEmptyOrNull(this.owner)) {
                temp += this.owner + " ";
            } else {
                temp += "unknown file path" + " ";
            }

            temp += "[";
            temp += this.line;
            if (this.column !== -1) {
                temp += ":" + this.column;
            }
            temp += "]";

            temp += " " + this.message;
            return temp;
        }
    }
}
