/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.XCppLint.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import WuiBuilderConnector = Com.Wui.Framework.Commons.Connectors.WuiBuilderConnector;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class XCppLintTest extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner {

        protected readFile($path : string, $callback : ($data : string) => void) : void {
            WuiBuilderConnector.Connect().Send("FileSystem", {
                type : "Read",
                value: $path
            }, ($data : any) : void => {
                $data = ObjectDecoder.Base64($data.data);

                $data = StringUtils.Substring($data, 2, StringUtils.Length($data) - 2);

                $data = StringUtils.Replace($data, "\\r\\n", "\r\n");
                $data = StringUtils.Replace($data, "\\n", "\n");

                $data = StringUtils.Replace($data, "\\w", "\w");
                $data = StringUtils.Replace($data, "\\d", "\d");

                $data = StringUtils.Replace($data, "\\\"", "\"");
                $data = StringUtils.Replace($data, "\\\\\"", "\\\"");
                $data = StringUtils.Replace($data, "\\\n", "\\n");
                $data = StringUtils.Replace($data, "\\\r", "\\r");
                $data = StringUtils.Replace($data, "\\\\t", "\\t");
                $data = StringUtils.Replace($data, "\\\\r", "\\r");
                $data = StringUtils.Replace($data, "\\\\\\\"", "\\\\\"");

                $callback($data);
            });
        }

        protected writeFile($path : string, $data : string) : void {
            WuiBuilderConnector.Connect().Send("FileSystemHandler", {
                path : $path,
                type : "Write",
                value: $data
            });
        }

        protected assertEquals($actual : any, $expected : any, $message? : string) : void {
            super.assertEquals($actual, $expected, $message);
            if (ObjectValidator.IsString($actual) && $actual !== $expected) {
                Echo.Println("<u>actual code print:</u>");
                Echo.PrintCode($actual);
                Echo.Println("<u>expected code print:</u>");
                Echo.PrintCode($expected);
            }
        }

        protected before() : void {
            Echo.Println("<style>pre {background-color: aliceblue;}</style>");
        }
    }
}
/* dev:end */
