/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;

    export class DoWhileAfterBrace extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.DO_WHILE_AFTER_BRACE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const prevChild : IToken = $processingInfo.PrevChild();

            if (prevChild && StringUtils.Contains(prevChild.body, "do {")) {
                const lastPrevChild : IToken = this.getLastChild(prevChild);
                if (lastPrevChild) {
                    if (lastPrevChild.line !== $processingInfo.Child().line) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        private getLastChild($parent : IToken) : any {
            if (!$parent) {
                return null;
            }

            if ($parent.children.length === 0) {
                return null;
            }

            return $parent.children[$parent.children.length - 1];
        }
    }
}
