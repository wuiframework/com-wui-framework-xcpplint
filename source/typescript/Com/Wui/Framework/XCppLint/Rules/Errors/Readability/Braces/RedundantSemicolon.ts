/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Readability.Braces {
    "use strict";
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class RedundantSemicolon extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.BRACES.REDUNDANT_SEMICOLON);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if ($processingInfo.Child().body === ";" ||
                $processingInfo.Child().body.trim() === "};") {
                const parent : IToken = $processingInfo.Parent();

                if (!parent) {
                    return;
                }

                if (BaseLintRule.isIf(parent.body) ||
                    BaseLintRule.isFor(parent.body) ||
                    BaseLintRule.isWhile(parent.body) ||
                    StringUtils.StartsWith(parent.body.trim(), "switch") ||
                    StringUtils.StartsWith(parent.body.trim(), "else") ||
                    StringUtils.StartsWith(parent.body.trim(), "namespace") ||
                    parent.body.trim() === "{" ||
                    BaseLintRule.isFunction(parent.body)) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
