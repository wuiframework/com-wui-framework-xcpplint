/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_VISUALGRAPH_HPP_
#define NEWVISUALPROJECT_VISUALGRAPH_HPP_

#include <string>
#include <map>

#include <VX/vx.h>

#include <VxUtils.hpp>

#include "Reference.hpp"

namespace NewVisualProject {
    namespace VXUtils = Io::VisionSDK::Studio::Libs::VXUtils;

    /**
     * This class holds main application business logic.
     */
    class VisualGraph {
     public:
        /**
         * Creates all contexts aligned with this instance.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        vx_status create();

        /**
         * Process graph validation.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        vx_status validate();

        /**
         * Runs processing routine. This method requires initialized and created VisualGraph.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        vx_status process();

        /**
         * @return Returns true if application should run without displays in native windows, false otherwise.
         */
        bool IsHeadless() const;

        /**
         * @param value Specify true to allow application displays in native window, false otherwise.
         */
        void setHeadless(bool value);

        /**
         * @return Returns current data stream name.
         */
        const std::string &getStreamDataName() const;

        /**
         * @param name Specify new data stream name.
         */
        void setStreamDataName(const std::string &name);

        /**
         * @param $name Specify name of IOCom.
         * @return Returns smart pointer to requested IOCom or nullptr if not found.
         */
        std::shared_ptr<VXUtils::Interfaces::IIOCom> getIoCom(const std::string &$name) const;

        bool IsLooped() const;

        void setIsLooped(bool $isLooped);

     private:
        std::map<std::string, BaseContext *> contextsMap{};
        std::map<std::string, VXUtils::Primitives::Any> propertiesMap{};
        std::map<std::string, std::shared_ptr<VXUtils::Interfaces::IIOCom>> ioComMap{};
        bool headless = false;
        bool isLooped = false;
        std::string streamDataName{};

        const BaseContext *getContext(const std::string &name) const;

        void removeContext(const std::string &name);
    };
}

#endif  // NEWVISUALPROJECT_VISUALGRAPH_HPP_
