/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_CONTEXT0_HPP_
#define NEWVISUALPROJECT_CONTEXT0_HPP_

#include <VX/vx.h>

#include "Reference.hpp"
#include "BaseContext.hpp"

namespace NewVisualProject {
    /**
     * This class overrides BaseContext methods with generated code.
     */
    class Context0 : public BaseContext {
     public:
        /**
         * Constructs Context object from parent VisualGraph.
         * @param parent Specify parent VisualGraph.
         */
        explicit Context0(VisualGraph *parent);

        vx_status create() override;

        vx_status validate() override;

        vx_status process() const override;
    };
}

#endif  // NEWVISUALPROJECT_CONTEXT0_HPP_
