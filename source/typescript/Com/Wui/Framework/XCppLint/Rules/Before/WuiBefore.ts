/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Before {
    "use strict";
    import BaseBefore = Com.Wui.Framework.XCppLint.Structures.BaseBefore;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import WuiProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.WuiProcessingInfo;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import SpecType = Com.Wui.Framework.XCppLint.Enums.SpecType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import TokenMapLinter = Com.Wui.Framework.XCppLint.Core.TokenMapLinter;
    import ClassMember = Com.Wui.Framework.XCppLint.Structures.ClassMember;

    export class WuiBefore extends BaseBefore {
        private static instance : WuiBefore;
        private processingInfo : WuiProcessingInfo;

        public static getInstance() : BaseBefore {
            if (!WuiBefore.instance) {
                WuiBefore.instance = new WuiBefore();
            }
            return WuiBefore.instance;
        }

        constructor() {
            super();
            this.processingInfo = new WuiProcessingInfo();
        }

        public Reset() : void {
            super.Reset();
            this.processingInfo.Clear();
        }

        public getResults($processingInfo : BaseProcessingInfo) : WuiProcessingInfo {
            this.run($processingInfo);
            return this.processingInfo;
        }

        private run($processingInfo : BaseProcessingInfo) : void {
            const parseClass = () : void => {
                if (!/class .* {/gm.test($processingInfo.Child().body)) {
                    return;
                }

                const cleanChildBody = ($child : IToken) : string => {
                    let temp : string = $child.body;
                    temp = StringUtils.Remove(temp, "public:");
                    temp = StringUtils.Remove(temp, "protected:");
                    temp = StringUtils.Remove(temp, "private:");
                    return temp;
                };

                let currentSpec : string = SpecType.PRIVATE;

                const members : ArrayList<ClassMember> = new ArrayList<ClassMember>();

                const getSpec = ($child : IToken) : string => {
                    if (StringUtils.StartsWith($child.body.trim(), SpecType.PUBLIC + ":")) {
                        return SpecType.PUBLIC;
                    } else if (StringUtils.StartsWith($child.body.trim(), SpecType.PROTECTED + ":")) {
                        return SpecType.PROTECTED;
                    } else if (StringUtils.StartsWith($child.body.trim(), SpecType.PRIVATE + ":")) {
                        return SpecType.PRIVATE;
                    } else {
                        return currentSpec;
                    }
                };

                for (const child of $processingInfo.Child().children) {
                    const member : ClassMember = new ClassMember();
                    member.Member(child);
                    member.Comment(null);

                    if (child.body.trim() === ";" ||
                        child.body.trim() === "}" ||
                        child.body.trim() === "};" ||
                        child.body.trim() === ");" ||
                        StringUtils.StartsWith(child.body.trim(), "typedef ") ||
                        StringUtils.StartsWith(child.body.trim(), "using ")) {
                        continue;
                    }

                    if (child.type === TokenType.COMMENT || child.type === TokenType.DIRECTIVE) {
                        continue;
                    }

                    const cleanChild : string = cleanChildBody(child);
                    currentSpec = getSpec(child);
                    child.specType = currentSpec;

                    if (!StringUtils.IsEmpty(cleanChild.trim())) {
                        members.Add(member);

                        const prevChild : IToken = TokenMapLinter.getPrevChild($processingInfo.Child(), child.childIndex);
                        if (prevChild && prevChild.type === TokenType.COMMENT && prevChild.line !== child.line) {
                            member.Comment(prevChild);
                        }

                        if (currentSpec === SpecType.PUBLIC) {
                            this.processingInfo.PublicMembers().Add(member);
                        } else if (currentSpec === SpecType.PROTECTED) {
                            this.processingInfo.ProtectedMembers().Add(member);
                        } else if (currentSpec === SpecType.PRIVATE) {
                            this.processingInfo.PrivateMembers().Add(member);
                        }
                    }
                }

                const getStaticMembers = ($from : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                    $from.foreach(($value : ClassMember) : void => {
                        if (this.isStatic($value.Member().body.trim())) {
                            if (!BaseLintRule.isFunction($value.Member().body)) {
                                $target.Add($value);
                            }
                        }
                    });
                };

                const getMembers = ($from : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                    $from.foreach(($value : ClassMember) : void => {
                        if (!this.isStatic($value.Member().body.trim())) {
                            if (!BaseLintRule.isCtor($value.Member().body.trim())) {
                                if (!BaseLintRule.isDtor($value.Member().body.trim())) {
                                    if (!BaseLintRule.isFunction($value.Member().body)) {
                                        if (!BaseLintRule.isCEFMacro($value.Member().body)) {
                                            $target.Add($value);
                                        }

                                    }
                                }
                            }
                        }
                    });
                };

                const getStaticMethods = ($from : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                    $from.foreach(($value : ClassMember) : void => {
                        if (this.isStatic($value.Member().body.trim())) {
                            if (BaseLintRule.isFunction($value.Member().body)) {
                                $target.Add($value);
                            }
                        }
                    });
                };

                const getCtor = ($from : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                    $from.foreach(($value : ClassMember) : void => {
                        if (BaseLintRule.isCtor($value.Member().body.trim()) &&
                            !BaseLintRule.isCEFMacro($value.Member().body)) {
                            $target.Add($value);
                        }
                    });
                };

                const getDtor = ($from : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                    $from.foreach(($value : ClassMember) : void => {
                        if (BaseLintRule.isDtor($value.Member().body.trim())) {
                            $target.Add($value);
                        }
                    });
                };

                const getMethods = ($from : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                    $from.foreach(($value : ClassMember) : void => {
                        if (!this.isStatic($value.Member().body.trim())) {
                            if (!BaseLintRule.isCtor($value.Member().body.trim())) {
                                if (!BaseLintRule.isDtor($value.Member().body.trim())) {
                                    if (BaseLintRule.isFunction($value.Member().body) &&
                                        !BaseLintRule.isCEFMacro($value.Member().body)) {
                                        $target.Add($value);
                                    }
                                }
                            }
                        }
                    });
                };

                const getCEFMacros = ($from : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                    $from.foreach(($value : ClassMember) : void => {
                        if (!this.isStatic($value.Member().body.trim())) {
                            if (!BaseLintRule.isCtor($value.Member().body.trim())) {
                                if (!BaseLintRule.isDtor($value.Member().body.trim())) {
                                    if (BaseLintRule.isCEFMacro($value.Member().body)) {
                                        $target.Add($value);
                                    }
                                }
                            }

                        }
                    });
                };

                const getExpectedOrder = ($source, $target) : void => {
                    getStaticMembers($source, $target);
                    getMembers($source, $target);
                    getStaticMethods($source, $target);
                    getCtor($source, $target);
                    getDtor($source, $target);
                    getMethods($source, $target);
                    getCEFMacros($source, $target);
                };

                getExpectedOrder(this.processingInfo.PublicMembers(), this.processingInfo.PublicTargetMembers());
                getExpectedOrder(this.processingInfo.ProtectedMembers(), this.processingInfo.ProtectedTargetMembers());
                getExpectedOrder(this.processingInfo.PrivateMembers(), this.processingInfo.PrivateTargetMembers());

                this.processingInfo.Members(members);
            };

            const parseParams = () : void => {
                const body : string = StringUtils.Replace($processingInfo.Child().body, "\n", "");
                let regexp : RegExp =
                    /\s*(?:\w(?:\w|::|\s)+)\s((\w|::|\*|\&|operator=|<<|>>)*)(\(.*)/g; // check if body might be function declaration
                let matchResult : RegExpExecArray = regexp.exec(body);
                let joinedLine : string = "";
                let subArg : string[] = [];

                this.processingInfo.SubArg([]);

                if (matchResult !== null && StringUtils.Length(matchResult[1]) > 0) {
                    joinedLine += matchResult[3];

                    // check return
                    if (!StringUtils.Contains(body, "return")) {
                        // newline already removed, match all between ()
                        regexp = /\((.*)\)/g;
                        matchResult = regexp.exec(joinedLine);
                        if (!matchResult) {
                            return;
                        }

                        joinedLine = matchResult[1];

                        joinedLine = joinedLine.replace(/<.*?>?>+/g, "REF");
                        joinedLine = joinedLine.replace(/const/g, "").trim();
                        joinedLine = joinedLine.replace(/\*|&|\[|\]/g, "");
                        joinedLine = joinedLine.replace(/\s*=\s*(\w|&)(\w|::|_|\(\)|\.)*/g, "");

                        if (!StringUtils.Contains(joinedLine, "'") && !StringUtils.Contains(joinedLine, "\"")) {
                            const split : string[] = StringUtils.Split(joinedLine, ",");

                            for (let arg of split) {
                                arg = arg.replace(/new\s/g, "");

                                subArg = StringUtils.Split(arg.trim(), " ");

                                if (subArg.length < 2) {
                                    break;
                                }

                                if (subArg.length === 2 &&
                                    !StringUtils.Contains(arg, "\'") &&
                                    !StringUtils.Contains(arg, "\"")) {

                                    this.processingInfo.SubArg().push(subArg[1]);
                                }
                            }
                        }
                    }
                }

                this.processingInfo.JoinedLine(joinedLine);
            };

            if (!this.Done()) {
                this.processingInfo.PublicMembers().Clear();
                this.processingInfo.ProtectedMembers().Clear();
                this.processingInfo.PrivateMembers().Clear();
                this.processingInfo.PublicTargetMembers().Clear();
                this.processingInfo.ProtectedTargetMembers().Clear();
                this.processingInfo.PrivateTargetMembers().Clear();
                parseClass();
                parseParams();
                this.Done(true);
            }
        }

        private isStatic($value : string) : boolean {
            return (StringUtils.StartsWith($value.trim(), "static ") ||
                StringUtils.Contains($value.trim(), " static "));
        }
    }
}
