/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core.Model {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import CharType = Com.Wui.Framework.XCppLint.Enums.CharType;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;

    export class TokenInfo extends BaseObject {
        private readonly startIndex : number;
        private readonly endIndex : number;
        private readonly line : number;
        private readonly col : number;
        private readonly type : CharType | TokenType;
        private readonly value : string;

        /**
         * @param {number} $start Starting position of token.
         * @param {number} $end End position of token.
         * @param {number} $line Line on this token is.
         * @param {number} $col Collumn of token.
         * @param {CharType|TokenType} $type The type of the token.
         * @param {string} $value String value of the token.
         */
        constructor($start : number, $end : number, $line : number, $col : number, $type : CharType | TokenType, $value : string) {
            super();
            this.startIndex = $start;
            this.endIndex = $end;
            this.line = $line;
            this.col = $col;
            this.type = $type;
            this.value = $value;
        }

        /**
         * @return {number} Returns start index of the token.
         */
        public getStartIndex() : number {
            return this.startIndex;
        }

        /**
         * @return {number} Returns end index of the token.
         */
        public getEndIndex() : number {
            return this.endIndex;
        }

        /**
         * @return {number} Returns line of the token.
         */
        public getLine() : number {
            return this.line;
        }

        /**
         * @return {number} Returns column of the token.
         */
        public getCol() : number {
            return this.col;
        }

        /**
         * @return {CharType | TokenType} Returns type of the token.
         */
        public getType() : CharType | TokenType {
            return this.type;
        }

        /**
         * @return {string} Returns value of the token.
         */
        public getValue() : string {
            return this.value;
        }
    }
}
