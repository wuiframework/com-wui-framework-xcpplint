/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Readability.Wui {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import WuiProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.WuiProcessingInfo;
    import WuiBefore = Com.Wui.Framework.XCppLint.Rules.Before.WuiBefore;
    import ClassMember = Com.Wui.Framework.XCppLint.Structures.ClassMember;

    export class MethodOrder extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.WUI.METHOD_ORDER);
            this.setOnBefore(WuiBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : WuiProcessingInfo = <WuiProcessingInfo>this.getOnBefore().getResults($processingInfo);

            const checkChunk = ($source : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                if (!$source || $source.IsEmpty()) {
                    return;
                }

                if ($source.Length() !== $target.Length()) {
                    return;
                }

                for (let i = 0; i < $source.Length(); i++) {
                    if ($source.getItem(i).Member().body.trim() !== $target.getItem(i).Member().body.trim()) {
                        $onError(this.getErrorObject(), $source.getItem(i).Member().line, -1);
                        break;
                    }
                }
            };

            checkChunk(pInfo.PublicMembers(), pInfo.PublicTargetMembers());
            checkChunk(pInfo.ProtectedMembers(), pInfo.ProtectedTargetMembers());
            checkChunk(pInfo.PrivateMembers(), pInfo.PrivateTargetMembers());
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const getFrom = ($source : ArrayList<ClassMember>) : number => {
                let start : number = ($source.getLast() ? $source.getLast().Member().childIndex : 0);

                $source.foreach(($value : ClassMember) : void => {
                    if ($value.Member().childIndex < start) {
                        start = $value.Member().childIndex;
                    }
                    if ($value.Comment() && $value.Comment().childIndex < start) {
                        start = $value.Comment().childIndex;
                    }
                });
                return start;
            };

            const wuiPInfo : WuiProcessingInfo = <WuiProcessingInfo>this.getOnBefore().getResults($pinfo);

            const formatMembers = ($source : ArrayList<ClassMember>, $target : ArrayList<ClassMember>) : void => {
                if (!$target || !$source || $target.IsEmpty() || $source.IsEmpty()) {
                    return;
                }

                this.formatInsideVisibilitySpecTarget($target, (<number>$pinfo.Config().getPropertyValue("spaces")));

                let index : number = getFrom($source);
                $target.foreach(($value : ClassMember) : void => {
                    if ($value.Comment()) {
                        $value.Comment().childIndex = index;
                        $pinfo.Child().children[index] = $value.Comment();
                        $value.Member().childIndex = index + 1;
                        $pinfo.Child().children[index + 1] = $value.Member();
                        index += 2;
                    } else {
                        $value.Member().childIndex = index;
                        $pinfo.Child().children[index] = $value.Member();
                        index++;
                    }
                });
            };

            formatMembers(wuiPInfo.PublicMembers(), wuiPInfo.PublicTargetMembers());
            formatMembers(wuiPInfo.ProtectedMembers(), wuiPInfo.ProtectedTargetMembers());
            formatMembers(wuiPInfo.PrivateMembers(), wuiPInfo.PrivateTargetMembers());

            return true;
        }
    }
}
