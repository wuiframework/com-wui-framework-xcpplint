/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_REFERENCE_HPP_
#define NEWVISUALPROJECT_REFERENCE_HPP_

namespace NewVisualProject {    class VisualGraph;    class BaseContext;    class BaseGraph;    class Context0;    class Graph0;}

#endif  // NEWVISUALPROJECT_REFERENCE_HPP_
