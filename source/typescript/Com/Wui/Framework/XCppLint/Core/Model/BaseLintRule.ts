/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core.Model {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;
    import IBaseLintRule = Com.Wui.Framework.XCppLint.Interfaces.IBaseLintRule;
    import Priority = Com.Wui.Framework.XCppLint.Enums.Priority;
    import Severity = Com.Wui.Framework.XCppLint.Enums.Severity;
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ClassMember = Com.Wui.Framework.XCppLint.Structures.ClassMember;
    import BaseBefore = Com.Wui.Framework.XCppLint.Structures.BaseBefore;

    export abstract class BaseLintRule extends BaseObject implements IBaseLintRule {
        private message : string;
        private severity : Severity;
        private priority : Priority;
        private errorObject : IErrorReporterObject;
        private beforeInstance : BaseBefore;
        private sensitivity : ArrayList<TokenType>;

        public static isClass($body : string) : boolean {
            return (StringUtils.Contains($body, "class ") &&
                StringUtils.EndsWith($body.trim(), "{"));
        }

        public static isStruct($body : string) : boolean {
            return /\s*struct.*\s{/gm.test($body);
        }

        public static isFunction($body : string) : boolean {
            return (
                /\s*(\w(?:\w|::|<|>|\s)*)\s((\w|::|\*|\&|operator=|<<|>>)*)(\(.*)/gm.test($body) &&
                !this.isIf($body)
            );
        }

        public static isLambda($body : string) : boolean {
            return /\[.*\]\s*(\(.*\))?((\s*)|(\s*->\s*\w*\s*)){/gm.test($body);
        }

        public static isVisibilitySpec($body : string) : boolean {
            return /(public|protected|private)\s*:/gm.test($body);
        }

        public static isDo($body : string) : boolean {
            return /\s*do\s*{/gm.test($body);
        }

        public static getTokenClassName($body : string) : string {
            if (!BaseLintRule.isClass($body)) {
                return "";
            }
            const res : string = /\s*class\s*(([^:{ ]|[::])*)/gm.exec($body)[1];

            const name : string = StringUtils.Split(res, "::").reverse()[0];

            return name;
        }

        public static isCtor($body : string) : boolean {
            return /^(\s*)(((\w*)::\4\s*\()|(explicit\s+[a-z0-9_A-Z]*\()|([a-z0-9_A-Z]*\())/.test($body) &&
                !BaseLintRule.isCEFMacro($body);
        }

        public static isDtor($body : string) : boolean {
            return /^(\s*)(((\w*)::~\4\s*\()|(virtual\s*)?~([a-z0-9_A-Z]*\())/.test($body);
        }

        public static isIf($body : string) : boolean {
            return /^ *(if|else| else if)\s*(\(|{)/gm.test($body);
        }

        public static isFor($body : string) : boolean {
            return /^ *for\s*\(/gm.test($body);
        }

        public static isWhile($body : string) : boolean {
            return /^ *while\s*\(/gm.test($body);
        }

        public static isSwitch($body : string) : boolean {
            return /^ *switch\s*\(/gm.test($body);
        }

        public static isCase($body : string) : boolean {
            return /^ *case[\s\w]*:\s*{/gm.test($body);
        }

        public static isTry($body : string) : boolean {
            return /^\s*try\s*{/gm.test($body);
        }

        public static isCEFMacro($body : string) : boolean {
            return /^[A-Z_]+\(.*\)/gm.test($body.trim());
        }

        public static isTokenInClass($token : IToken) : boolean {
            if (!$token) {
                return false;
            }

            let next : IToken = $token.parent;

            while (next) {
                if (StringUtils.Contains(next.body, "class ")) {
                    return true;
                }
                next = next.parent;
            }

            return false;
        }

        public static isTokenInExtern($token : IToken) : boolean {
            if (!$token) {
                return false;
            }

            let next : IToken = $token.parent;

            while (next) {
                if (StringUtils.Contains(next.body, "extern ")) {
                    return true;
                }
                next = next.parent;
            }

            return false;
        }

        constructor() {
            super();
            this.message = "";
            this.severity = Severity.UNKNOWN;
            this.priority = Priority.MEDIUM;
            this.errorObject = null;
            this.beforeInstance = null;
            this.sensitivity = new ArrayList<TokenType>(TokenType.BLOCK);
        }

        /**
         * @param {BaseProcessingInfo} $processingInfo Processing info containing additional data about the pair child-parent,
         * necessary for correct error detection.
         * @param {function} $onError Callback that is called when a error is detected.
         */
        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            return;
        }

        /**
         * @param {Severity} $severity Set severity to this rule.
         * @return {Severity} Return severity of the rule.
         */
        public Severity($severity? : Severity) : Severity {
            return this.severity;
        }

        /**
         * @param {Priority} $priority Set priority to this rule.
         * @return {Priority} Return priority of the rule.
         */
        public Priority($priority? : Priority) : Priority {
            return this.priority;
        }

        /**
         * @param {IErrorReporterObject} $object Set error object which belong to the particular rule.
         */
        public setErrorObject($object : IErrorReporterObject) : void {
            this.errorObject = $object;
        }

        /**
         * @return {IErrorReporterObject} Return error object associated with the rule.
         */
        public getErrorObject() : IErrorReporterObject {
            return this.errorObject;
        }

        /**
         * @return {string} Return message of the rule.
         */
        public Message() : string {
            return this.message;
        }

        public ResetBefore() : void {
            if (this.beforeInstance !== null) {
                this.beforeInstance.Reset();
            }
        }

        /**
         * @param {IToken} $token Check if token fits the sensitivity of the rule.
         * @return {boolean} Return true if sensitivity fits, false otherwise.
         */
        public IsSensitiveTo($token : IToken) : boolean {
            return this.sensitivity.Contains($token.type);
        }

        /**
         * @param {BaseProcessingInfo} $pinfo Fix the error based on this additional information.
         * @return {boolean} Return true if fix successful, false otherwise.
         */
        public Fix($pinfo : BaseProcessingInfo) : boolean {
            return false;
        }

        /**
         * @param {Config} $config Load rule-specific information from the config.
         */
        public LoadConfig($config : Config) : void {
            return;
        }

        protected generateSpaces($numOfSpaces : number) : string {
            let res = "";
            for (let i = 0; i < $numOfSpaces; i++) {
                res += " ";
            }
            return res;
        }

        protected setOnBefore($onBefore : BaseBefore) : void {
            this.beforeInstance = $onBefore;
        }

        protected getOnBefore() : BaseBefore {
            return this.beforeInstance;
        }

        protected isCppCast($child : IToken) : boolean {
            return /(\w*_cast)\s*<[^>]*>/gm.test($child.body);
        }

        protected setSensitivity(...$sensitivities : TokenType[]) : void {
            this.sensitivity.Clear();
            $sensitivities.forEach(($sensitivity : TokenType) : void => {
                this.sensitivity.Add($sensitivity);
            });
        }

        protected requiresNewLine($body : string) : boolean {
            return (
                BaseLintRule.isFunction($body) ||
                BaseLintRule.isLambda($body) ||
                BaseLintRule.isIf($body) ||
                BaseLintRule.isFor($body) ||
                BaseLintRule.isWhile($body) ||
                BaseLintRule.isDo($body) ||
                this.isNamespace($body) ||
                BaseLintRule.isCtor($body) ||
                BaseLintRule.isDtor($body) ||
                BaseLintRule.isStruct($body) ||
                BaseLintRule.isClass($body)
            );
        }

        protected formatInsideVisibilitySpecTarget($target : ArrayList<ClassMember>, $expectedIndentSize : number) : void {
            for (let i = 0; i < $target.Length(); i++) {
                let first : IToken = null;
                if ($target.getItem(i).Comment()) {
                    first = $target.getItem(i).Comment();
                    if (StringUtils.EndsWith($target.getItem(i).Comment().body, "\n")) {
                        if (StringUtils.StartsWith($target.getItem(i).Member().body, "\n")) {
                            const lnl : string = CodeBlocksParser.getLeadingNewlines($target.getItem(i).Member().body);
                            $target.getItem(i).Member().body = StringUtils.Substring($target.getItem(i).Member().body, lnl.length);
                        }
                    } else {
                        if (!StringUtils.StartsWith($target.getItem(i).Member().body, "\n")) {
                            $target.getItem(i).Member().body = "\n" + $target.getItem(i).Member().body;
                        }
                    }
                } else {
                    first = $target.getItem(i).Member();
                }

                const expectedNl : string = (i > 0 ? "\n\n" : "\n");

                const lnl : string = CodeBlocksParser.getLeadingNewlines(first.body);

                if (lnl.length !== expectedNl.length) {
                    const body : string = StringUtils.Substring(first.body, lnl.length);
                    first.body = expectedNl + body;
                }
            }
        }

        protected getNextChild($parent : IToken, $childIndex : number) : any {
            if (!$parent) {
                return null;
            }

            if ($parent.children.length <= $childIndex + 1 || $childIndex < 0) {
                return null;
            }

            return $parent.children[$childIndex + 1];
        }

        private isNamespace($body : string) : boolean {
            return /(^ *)namespace/gm.test($body);
        }
    }
}
