/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Runtime.Explicit {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ExplicitProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.ExplicitProcessingInfo;
    import ExplicitBefore = Com.Wui.Framework.XCppLint.Rules.Before.ExplicitBefore;

    export class SingleParamCtor extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.RUNTIME.EXPLICIT.SINGLE_PARAM_CTOR);
            this.setOnBefore(ExplicitBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (!StringUtils.StartsWith($processingInfo.Parent().body.trim(), "class")) {
                return;
            }

            const pInfo : ExplicitProcessingInfo = (<ExplicitProcessingInfo>this.getOnBefore().getResults($processingInfo));

            if (!pInfo.IsMarkedExplicit() &&
                pInfo.OneArgCtor() &&
                !pInfo.InitializerListCtor() &&
                !pInfo.CopyCtor() &&
                pInfo.DefaultedArgs().length === 0) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
