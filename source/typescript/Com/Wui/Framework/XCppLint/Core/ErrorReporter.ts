/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Core {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import LintError = Com.Wui.Framework.XCppLint.Core.Model.LintError;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Severity = Com.Wui.Framework.XCppLint.Enums.Severity;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import Priority = Com.Wui.Framework.XCppLint.Enums.Priority;

    export const ERRORS = {
        BUILD          : {
            ENDIF_COMMENT     : {
                message : "Uncommented text after #endif is non-standard. Use a comment.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            EXPLICIT_MAKE_PAIR: {
                message : "For C++11-compatibility, omit template arguments from make_pair" +
                    " OR use pair directly OR if appropriate, construct a pair directly",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            INNER_FWD_DECL    : {
                message : "Inner-style forward declarations are invalid.  Remove this line.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            MAX_MIN_DEPRECATED: {
                message : ">? and <? (max and min) operators are non-standard and deprecated.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            NAMESPACES        : {
                UNNAMED_NAMESPACE: {
                    message : "Do not use unnamed namespaces in header files.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                USING_NAMESPACE  : {
                    message : "Do not use using namespace-directives. Use using-declarations instead.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            STORAGE_CLASS     : {
                message :
                    "Storage-class specifier (static, extern, typedef, etc) should be at the beginning of the declaration.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            UNDEFINED_ESCAPES : {
                message : "%, [, (, and { are undefined character escapes.  Unescape them.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            }
        },
        LEGAL_COPYRIGHT: {
            message : "No copyright message found. You should have a line: \"Copyright [year] <Copyright Owner>\"",
            priority: Priority.MEDIUM,
            severity: Severity.WARNING
        },
        OTHER          : {
            FILE_NEW_LINE   : {
                message : "File does not end in newline",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            INVALID_BRACKETS: {
                message : "Invalid brackets",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            LINE_LEN        : {
                message : "Line too long.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            TAB_FOUND       : {
                message : "Tab found, better to use spaces",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            }
        },
        READABILITY    : {
            BRACES     : {
                ELSE_BRACES_ON_BOTH_SIDES: {
                    message : "If an else has a brace on one side, it should have it on both",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                IF_ELSE_REQ_BRACES       : {
                    message : "If/else bodies require braces",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                NEW_LINE_IF              : {
                    message : "Did you mean \"else if\"? If not, start a new line for \"if\".",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                REDUNDANT_SEMICOLON      : {
                    message : "Redundant semicolon",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            CASTING    : {
                C_STYLE          : {
                    message : "Using C-style cast.  Use %s<%s>(...) instead",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                DEPRECATED_STYLE : {
                    message : "Using deprecated casting style. Use static_cast<%s>(...) instead",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                DEREFERENCED_CAST: {
                    message : "Are you taking an address of (something dereferenced from) a cast?",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            INHERITANCE: {
                OVERRIDE_REDUNDANT: {
                    message : "'override' is redundant since function is already declared as \"final\"",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                VIRT_REDUNDANT    : {
                    message : "'virtual' is redundant since function is already declared as \"%s\"",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            NUL        : {
                message : "Line contains NUL byte.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            UTF8       : {
                message : "Line contains invalid UTF-8 (or Unicode replacement character).",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            WUI        : {
                CLASS_ORDER            : {
                    message : "Invalid order of visibility specification in class.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                HAS_UNDERSCORE         : {
                    message : "argument contains '_'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                METHOD_NAMING          : {
                    message : "Incorrect name of method.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                METHOD_ORDER           : {
                    message : "Invalid order of methods within visibility specification.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_DOLLAR         : {
                    message : "missing prefix $.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                NULL_USED              : {
                    message : "Found NULL. Consider using nullptr instead.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                STRUCT_AS_PARAM        : {
                    message : "Do not use 'struct' as a parameter.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                VISIBILITY_SPEC_COMMENT: {
                    message : "Do not comment before 'public|protected|private'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            }
        },
        RUNTIME        : {
            CONST_STRING_REF  : {
                message : "const string& members are dangerous. It is much better to use alternatives, " +
                    "such as pointers or simple constants.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            EXPLICIT          : {
                CALLABLE_ONE_ARG_CTOR: {
                    message : "Constructors callable with one argument should be marked explicit.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                SINGLE_PARAM_CTOR    : {
                    message : "Single-parameter constructors should be marked explicit.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                ZERO_PARAM_CTOR      : {
                    message : "Zero-parameter constructors should not be marked explicit.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            INIT_ITSELF       : {
                message : "You seem to be initializing a member variable with itself.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            INT               : {
                PORTS    : {
                    message : "Use \"unsigned short\" for ports, not \"short\"",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                USE_INT64: {
                    message : "Use int16/int64/etc, rather than the C type %s.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            INVALID_INCREMENT : {
                message : "Changing pointer instead of value (or unused value of operator*).",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            MEMSET            : {
                message : "Did you mean \"memset(%s, 0, %s)\"?",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            NON_CONST_REF     : {
                message : "Is this a non-const reference? If so, make const or use a pointer.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            OPERATOR_AMPERSAND: {
                message : "Unary operator& is dangerous.  Do not use it.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            PRINTF            : {
                FORMAT_STRING_BUG: {
                    message : "Potential format string bug. Do %s(\"%%s\", %s) instead.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                SIZEOF_INSTEAD   : {
                    message : "If you can, use sizeof(%s) instead of %s as the 2nd arg to snprintf.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                SNPRINTF         : {
                    message : "Almost always, snprintf is better than %s",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                SPRINTF          : {
                    message : "Never use sprintf. Use snprintf instead: const char %s[].",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            PRINTF_FORMAT     : {
                N_FORMATS  : {
                    message : "%N$ formats are unconventional.  Try rewriting to avoid them.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                Q_IN_FORMAT: {
                    message : "%q in format strings is deprecated.  Use %ll instead.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            STRING            : {
                NOT_PERMITTED     : {
                    message : "Static/global string variables are not permitted.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                USE_C_STYLE_STRING: {
                    message : "For a static/global string constant, use a C style string instead.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            THREADSAFE_FN     : {
                message : "Consider using ... instead of ...",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            VAR_LEN_ARRAY     : {
                message : "Do not use variable-length arrays. " +
                    "Use an appropriately named ('k' followed by CamelCase) compile-time constant for the size.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            }
        },
        WHITESPACE     : {
            BLANK_LINE           : {
                AFTER_VISIBILITY_SPEC: {
                    message : "Do not leave a blank line after visibility specification",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                AT_END               : {
                    message : "redundant blank line at the end of a code block should be deleted",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                AT_START             : {
                    message : "redundant blank line at the start of a code block should be deleted",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                PRECEDED             : {
                    message : "(public|private|protected) should be preceded by a blank line",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            BRACES               : {
                BRACE_AT_PREV_LINE      : {
                    message : "{ should almost always be at the end of the previous line",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                EXTRA_SPACE_BEFORE_BRACE: {
                    message : "Extra space before [",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_CATCH    : {
                    message : "missing space before catch",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_ELSE     : {
                    message : "missing space before else",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_OPEN     : {
                    message : "missing space before {",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_WHILE    : {
                    message : "invalid space before while in do/while",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            COLON_INVALID_SPACING: {
                message : "Invalid spaces before ':'",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            COMMA_INVALID_SPACING: {
                message : "Invalid spacing around ','",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            COMMA_IN_FUNCTION    : {
                message : "Invalid spacing around ',' in function parameters",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            CPP_CASTING          : {
                message : "Invalid c++-style cast, should be '_cast<...>(...)'.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            EMPTY_BODY           : {
                BLOCK: {
                    message : "empty code block",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                FOR  : {
                    message : "empty for body",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                IF   : {
                    message : "empty if body",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                WHILE: {
                    message : "empty while body",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            FOR_COLON_MISSING    : {
                message : "Missing space around colon in range-based for loop",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            INDENT               : {
                INITIALIZER_LIST_INDENT: {
                    message : "Invalid indentation in initializer list.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                INVALID_INDENT         : {
                    message : "Invalid indentation.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                LINE_ENDS_WHITESPACE   : {
                    message : "Line ends with whitespace.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                LINE_START             : {
                    message : "Weird number of spaces at line start.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            NEWLINE              : {
                CATCH_NEWLINE                : {
                    message : "Newline before 'catch' should be removed.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                DO_WHILE_AFTER_BRACE         : {
                    message : "In do/while clause while should be after the closing }.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                DO_WHILE_SINGLE_LINE         : {
                    message : "do/while clauses should not be on a single line",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                ELSE_ON_SAME_LINE            : {
                    message : "An else should appear on the same line as the preceding }",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_AFTER_CLOSE_BRACE    : {
                    message : "Newline missing after '}'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_AFTER_COMMENT        : {
                    message : "Newline missing after comment.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_AFTER_OPEN_BRACE     : {
                    message : "Newline missing after '{'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_AFTER_TEMPLATE       : {
                    message : "Newline missing after template.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_AFTER_VISIBILITY_SPEC: {
                    message : "Newline missing after ':'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_CLOSE_BRACE   : {
                    message : "Newline missing before '}'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_COMMENT       : {
                    message : "Newline missing before comment.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_DEC_DEF       : {
                    message : "Newline missing before declaration or definition.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_DO            : {
                    message : "Newline missing before 'do {'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_BEFORE_INIT_LIST     : {
                    message : "Newline missing before ':' in constructor.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MULTIPLE_COMMANDS            : {
                    message : "multiple commands on the same line",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            OPERATORS            : {
                INVALID_BINARY_OPERATOR: {
                    message : "Invalid spacing around binary operator.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                INVALID_COMPARISON     : {
                    message : "Invalid spacing around comparison",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                INVALID_UNARY          : {
                    message : "Invalid spacing around unary operator",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                MISSING_SPACE_EQUAL    : {
                    message : "Missing space around =",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            PARENS               : {
                EXTRA_SPACE_AFTER : {
                    message : "Extra space after (",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                EXTRA_SPACE_BEFORE: {
                    message : "Extra space before (",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                INVALID_SPACING   : {
                    message : "Invalid spacing around ( or )",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                SHOULD_BE_MOVED_UP: {
                    message : "Closing ) should be moved to the previous line",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            POINTERS             : {
                message : "Extra space around pointer '->' or '.'.",
                priority: Priority.MEDIUM,
                severity: Severity.WARNING
            },
            SEMICOLON            : {
                DEFINING_EMPTY_STATEMENT: {
                    message : "Semicolon defining empty statement.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                EXTRA_SPACE_BEFORE      : {
                    message : "Extra space before last semicolon.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                INVALID_SPACING         : {
                    message : "Invalid spacing around ;",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                ONLY                    : {
                    message : "Line contains only semicolon.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            TEMPLATE             : {
                SPACE_AFTER_OPEN_BRACE  : {
                    message : "Extra space after '<'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                SPACE_AFTER_TEMPLATE    : {
                    message : "Extra space after 'template'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                },
                SPACE_BEFORE_CLOSE_BRACE: {
                    message : "Extra space before '>'.",
                    priority: Priority.MEDIUM,
                    severity: Severity.WARNING
                }
            },
            WUI                  : {
                TEMPLATED_VARS: {
                    INNER_SPACES  : {
                        message : "Do not leave a space before '>' or after '<'",
                        priority: Priority.MEDIUM,
                        severity: Severity.WARNING
                    },
                    SPACE_AT_END  : {
                        message : "Missing (or extra) space between '>' and variable name.",
                        priority: Priority.MEDIUM,
                        severity: Severity.WARNING
                    },
                    SPACE_AT_START: {
                        message : "Extra space between type and '<'.",
                        priority: Priority.MEDIUM,
                        severity: Severity.WARNING
                    }
                }
            }
        }
    };

    export class ErrorReporter extends BaseObject {
        private errorList : ArrayList<LintError>;
        private disabledLines : ArrayList<IDisabledLine>;
        private readonly camelCaseErrors : any[];
        private owner : string;

        constructor() {
            super();
            this.errorList = new ArrayList<LintError>();
            this.disabledLines = new ArrayList<IDisabledLine>();
            this.camelCaseErrors = [];
            this.owner = "";
            this.init();
        }

        /**
         * @param {string} $filePath File name to which all errors belong.
         */
        public setOwner($filePath : string) : void {
            this.owner = $filePath;
        }

        /**
         * @param {IErrorReporterObject} $message Object which holds message of the error.
         * @param {number} $line Line on which the error occurred.
         * @param {number} $column Column on which the error occurred.
         */
        public ReportError($message : IErrorReporterObject, $line : number, $column : number = -1) : void {
            if (!this.shouldPrintError($message, $line)) {
                return;
            }

            this.addError(
                new LintError($line, $message, $column)
            );

            this.errorList.getLast().setOwner(this.owner);
        }

        /**
         * Remove all lines globally disabling linter.
         */
        public CleanAllGlobalLines() : void {
            const all : IDisabledLine[] = this.disabledLines.getAll();
            const nonGlobals : IDisabledLine[] = all.filter(($elem : IDisabledLine) : boolean => {
                return !$elem.global;
            });
            this.disabledLines = ArrayList.ToArrayList(nonGlobals);
        }

        /**
         * Remove all lines disabling linter.
         */
        public CleanAllLines() : void {
            this.disabledLines = new ArrayList<IDisabledLine>();
        }

        /**
         * @param {string} $errorToIgnore Error name which should be disabled for linting.
         * @param {number} $line Line on which the disabling should take effect.
         * @param {boolean} $global Is this disabling only local or global (from $line on)?
         */
        public AddIgnoreErrorLine($errorToIgnore : string, $line : number, $global : boolean = false) : void {
            $errorToIgnore = StringUtils.ToLowerCase($errorToIgnore.trim());

            $errorToIgnore = StringUtils.Replace($errorToIgnore, ".", "-");

            if (!StringUtils.StartsWith($errorToIgnore, "errors-")) {
                $errorToIgnore = "errors-" + $errorToIgnore.trim();
            }

            $errorToIgnore = StringUtils.ToCamelCase($errorToIgnore);

            if (!this.isErrorDisabled($errorToIgnore, $line)) {
                this.disabledLines.Add(
                    <IDisabledLine>{
                        global : $global,
                        line   : $line,
                        message: $errorToIgnore
                    });
            }
        }

        /**
         * Clean all reported errors.
         */
        public ClearErrors() : void {
            this.errorList = new ArrayList<LintError>();
        }

        /**
         * @return {ArrayList<LintError>} Return array of reported errors.
         */
        public getAllErrors() : ArrayList<LintError> {
            return this.errorList;
        }

        /**
         * @param {IErrorReporterObject} $error Error to find.
         * @return {string} Return camelCase of error full name.
         */
        public FindCamelCase($error : IErrorReporterObject) : string {
            let err : string = "";

            for (const elem of this.camelCaseErrors) {
                if (elem[0].message === $error.message) {
                    err = elem[1];
                    break;
                }
            }

            return err;
        }

        private init() : void {
            const traverse = ($object : any, parent : string) : void => {
                const type = typeof $object;
                if (type === "object") {
                    for (const key of Object.keys($object)) {
                        if (key !== "message" && key !== "priority" && key !== "severity") {
                            traverse($object[key], parent + "-" + key);
                        } else {
                            this.camelCaseErrors.push(
                                [$object, StringUtils.ToCamelCase(StringUtils.ToLowerCase(parent))]
                            );
                            break;
                        }
                    }
                }
            };

            traverse(ERRORS, "ERRORS");
        }

        private addError($error : LintError) : void {
            this.errorList.Add($error);
        }

        private shouldPrintError($error : IErrorReporterObject, $line : number) : boolean {
            const err : string = this.FindCamelCase($error);

            if (StringUtils.IsEmpty(err)) {
                LogIt.Debug("Unknown error message: {0}", $error);
                return true;
            }

            return !this.isErrorDisabled(err, $line);
        }

        private isErrorDisabled($err : string, $line : number) : boolean {
            for (const disabled of this.disabledLines.getAll()) {
                if (disabled.message === $err) {
                    if (disabled.global) {
                        if ($line >= disabled.line) {
                            return true;
                        }
                    } else {
                        if ($line === disabled.line) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }

    interface IDisabledLine {
        message : string;
        line : number;
        global : boolean;
    }
}
