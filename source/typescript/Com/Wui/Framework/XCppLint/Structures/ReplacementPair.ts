/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Structures {
    "use strict";

    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ReplacementPair extends EventArgs {
        private original : string;
        private replacement : string;

        /**
         * @param {string} $original If set, set the original string value.
         * @return {string} Returns the original string value.
         */
        public Original($original? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($original)) {
                this.original = $original;
            }
            return this.original;
        }

        /**
         * @param {string} $replacement If set, set the replacement string value.
         * @return {string} Returns the replacement string value.
         */
        public Replacement($replacement? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($replacement)) {
                this.replacement = $replacement;
            }
            return this.replacement;
        }
    }
}
