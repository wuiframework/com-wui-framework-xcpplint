/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import TokenMapLinter = Com.Wui.Framework.XCppLint.Core.TokenMapLinter;
    import IToken = Com.Wui.Framework.XCppLint.Interfaces.IToken;

    export class MissingBeforeDecDef extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MISSING_BEFORE_DEC_DEF);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if ($processingInfo.PrevChild() &&
                (StringUtils.EndsWith($processingInfo.PrevChild().body, "{") ||
                    StringUtils.EndsWith($processingInfo.PrevChild().body, ";"))) {
                if ($processingInfo.Child().body.trim() === ";" ||
                    $processingInfo.Child().body.trim() === "}" ||
                    $processingInfo.Child().body.trim() === "};" ||
                    $processingInfo.Child().body.trim() === ");" ||
                    $processingInfo.Child().body.trim() === "{" ||
                    $processingInfo.Parent().body.trim() === ") {" ||
                    StringUtils.Remove($processingInfo.Child().body.trim(), " ", "\n").trim() === ",{") {
                    return;
                }

                if (StringUtils.StartsWith($processingInfo.Child().body.trim(), "else") ||
                    StringUtils.StartsWith($processingInfo.Child().body.trim(), "if")) {
                    return;
                }

                if (StringUtils.IsEmpty($processingInfo.Child().body.trim())) {
                    return;
                }

                if (StringUtils.StartsWith($processingInfo.Child().body.trim(), "using") ||
                    StringUtils.StartsWith($processingInfo.Child().body.trim(), "namespace") ||
                    StringUtils.StartsWith($processingInfo.Child().body.trim(), "typedef")) {
                    return;
                }

                if (StringUtils.StartsWith($processingInfo.Child().body.trim(), "const char ")) {
                    return;
                }

                if (StringUtils.StartsWith($processingInfo.Child().body.trim(), "public") ||
                    StringUtils.StartsWith($processingInfo.Child().body.trim(), "protected") ||
                    StringUtils.StartsWith($processingInfo.Child().body.trim(), "private") ||
                    StringUtils.StartsWith($processingInfo.Child().body.trim(), "using")) {
                    return;
                }

                if (BaseLintRule.isFunction($processingInfo.Parent().body) ||
                    BaseLintRule.isDo($processingInfo.Parent().body) ||
                    BaseLintRule.isIf($processingInfo.Parent().body) ||
                    BaseLintRule.isFor($processingInfo.Parent().body) ||
                    BaseLintRule.isCtor($processingInfo.Parent().body) ||
                    BaseLintRule.isWhile($processingInfo.Parent().body) ||
                    BaseLintRule.isSwitch($processingInfo.Parent().body) ||
                    BaseLintRule.isCase($processingInfo.Parent().body) ||
                    BaseLintRule.isTry($processingInfo.Parent().body) ||
                    BaseLintRule.isStruct($processingInfo.Parent().body) ||
                    BaseLintRule.isLambda($processingInfo.Parent().body)) {
                    return;
                }

                // handle CEF macros like IMPLEMENT_REFCOUNTING(ClientRenderDelegate) etc.
                if (BaseLintRule.isCEFMacro($processingInfo.Child().body)) {
                    return;
                }

                if (StringUtils.Contains($processingInfo.PrevToken().body, "regex")) {
                    return;
                }

                if (StringUtils.Contains($processingInfo.PrevChild().body, "json")) {
                    return;
                }

                const expectedNewlines : string = this.getExpectedNewlines($processingInfo);

                if (!StringUtils.StartsWith($processingInfo.Child().body, expectedNewlines)) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const matches : RegExpExecArray = /(\n*)( *.*)/gm.exec($pinfo.Child().body);
            const newlinesAtStart : string = matches[1];
            const expectedNewlines : string = this.getExpectedNewlines($pinfo);

            if (newlinesAtStart.length !== expectedNewlines.length) {
                const prevChild : IToken = TokenMapLinter.getPrevChild($pinfo.Parent(), $pinfo.Child().childIndex);
                if (prevChild && !(
                    StringUtils.StartsWith(prevChild.body.trim(), "public") ||
                    StringUtils.StartsWith(prevChild.body.trim(), "protected") ||
                    StringUtils.StartsWith(prevChild.body.trim(), "private")
                )) {
                    $pinfo.Child().body = expectedNewlines + StringUtils.Substring($pinfo.Child().body, newlinesAtStart.length);
                }
            }
            return true;
        }

        private getExpectedNewlines($processingInfo : BaseProcessingInfo) : string {
            if (BaseLintRule.isClass($processingInfo.Parent().body) &&
                !BaseLintRule.isFunction($processingInfo.Child().body) &&
                !BaseLintRule.isCtor($processingInfo.Child().body) &&
                !BaseLintRule.isDtor($processingInfo.Child().body)) {
                return "\n";
            } else {
                if (StringUtils.EndsWith($processingInfo.PrevChild().body, ");\n")) {
                    return "\n";
                }
                return "\n\n";
            }
        }
    }
}
