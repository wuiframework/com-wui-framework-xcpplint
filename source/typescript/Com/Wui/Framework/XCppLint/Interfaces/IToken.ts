/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Interfaces {
    "use strict";
    import TokenType = Com.Wui.Framework.XCppLint.Enums.TokenType;
    import SpecType = Com.Wui.Framework.XCppLint.Enums.SpecType;

    export interface IToken {
        body : string;
        childIndex : number;
        children : IToken[];
        type : TokenType;
        line : number;
        nesting : number;
        originalBody : string;
        specType : SpecType;
        parent : IToken;
    }
}
