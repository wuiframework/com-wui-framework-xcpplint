/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.XCppLint.Rules.Errors.Whitespace.Operators {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseLintRule = Com.Wui.Framework.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Com.Wui.Framework.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Com.Wui.Framework.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Com.Wui.Framework.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class InvalidBinaryOperator extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.OPERATORS.INVALID_BINARY_OPERATOR);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            let match : RegExpExecArray = /(.)\+(.)/gm.exec($processingInfo.Child().body);
            if (match) {
                if (!StringUtils.Contains($processingInfo.Child().body, "operator+") &&
                    !StringUtils.Contains($processingInfo.Child().body, "++") &&
                    !StringUtils.Contains($processingInfo.Child().body, "+=") &&
                    (match[1] !== " " || match[2] !== " ")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }

            match = /(.)-([^0-9])/gm.exec($processingInfo.Child().body);
            if (match) {
                if (!StringUtils.Contains($processingInfo.Child().body, "operator-") &&
                    !StringUtils.Contains($processingInfo.Child().body, "--") &&
                    !StringUtils.Contains($processingInfo.Child().body, "-=") &&
                    !StringUtils.Contains($processingInfo.Child().body, "->") &&
                    (match[1] !== " " || match[2] !== " ")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }

            match = /([^'])\/([^'])/gm.exec($processingInfo.Child().body);
            if (match) {
                if (!StringUtils.Contains($processingInfo.Child().body, "operator/") &&
                    !StringUtils.Contains($processingInfo.Child().body, "/=") &&
                    ((match[1] !== " " || match[2] !== " ") && match[2] !== "\n")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }

            match = /(.)%(.)/gm.exec($processingInfo.Child().body);
            if (match) {
                if (!StringUtils.Contains($processingInfo.Child().body, "operator%") &&
                    !StringUtils.Contains($processingInfo.Child().body, "%=") &&
                    (match[1] !== " " || match[2] !== " ")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
